package com.cornel.pims.storage.auth

import android.os.Bundle
import android.os.Handler
import com.cornel.pims.storage.R
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.main.StorageMainActivity
import kotlinx.android.synthetic.main.auth_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import javax.inject.Inject

interface StorageAuthView {

    val component: StorageAuthComponent

    fun setLogin(text: String)
    fun showError(e: Throwable, finishActivity: Boolean)
    fun finish()
    fun navigateToMainActivity()
}

class StorageAuthActivity : BaseView(), StorageAuthView {

    companion object {
        const val EXTRA_TOKEN_FLAG = "Token flag"
        const val TOKEN_TOAST_DELAY = 250L
    }

    override val component by lazy { app.component.plus(StorageAuthModule(this)) }

    @Inject
    override lateinit var presenter: StorageAuthPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.auth_activity)

        component.inject(this)

        if (intent.getBooleanExtra(EXTRA_TOKEN_FLAG, false)) {
            Handler().postDelayed({
                toast("Время сеанса закончено. Необходима авторизация.")
            }, TOKEN_TOAST_DELAY)
        }

        presenter.onCreate()

        addListeners()
        //TODO remove autologin
                // presenter.onLogin("admin@example.com", "password")
    }


    private fun addListeners() {
        enter.onClick {
            val userLogin = login.text.toString()
            val userPass = password.text.toString()
            presenter.onLogin(userLogin, userPass)
        }
    }

    override fun navigateToMainActivity() {
        startActivity(intentFor<StorageMainActivity>())
    }

    override fun setLogin(text: String) = login.setText(text)
}

