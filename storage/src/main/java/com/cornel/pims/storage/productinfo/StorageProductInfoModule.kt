package com.cornel.pims.storage.productinfo

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 15.02.18.
 */


@Module
class StorageProductInfoModule(private val view: StorageProductInfoView) {

    @StorageProductInfoScope
    @Provides
    fun providePresenter(): StorageProductInfoPresenter = StorageProductInfoPresenterImpl(view)
}