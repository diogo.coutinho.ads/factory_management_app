package com.cornel.pims.storage.common

data class ItemListUnit(
        val id: String,
        val name: String,
        val code: String,
        val quantityWithAbbreviation: String)

data class AcceptanceListItem(
        val id: String,
        val name: String,
        val sku: String,
        val workplace: String,
        val quantityWithAbbreviation: String)

data class AreaListUnit(val id: String, val name: String, val quantity: Int)

data class DocumentListUnit(
        val id: String,
        val date: String,
        val name: String,
        val code: String,
        val quantityWithAbbreviation: String
)

data class StorageListUnit(val id: String, val name: String)

data class HistoryUnit(
        val name: String,
        val date: String
)

data class OrderListUnit(
        val id: String,
        val number: Long,
        val partner: String
)

data class ShippingListUnit(
        val id: Int,
        val productId: String,
        val productName: String,
        val productDate: String,
        val orderQuantity: Double,
        val areas: List<ShippingAreaListUnit>
)

data class ShippingAreaListUnit(
        val id: String,
        val name: String,
        val itemQuantity: Double,
        var itemChosenQuantity: Double
)

data class ShippingAreaProductListUnit(
        val areaId: String,
        val itemId: String,
        val itemQuantity: Double
)
