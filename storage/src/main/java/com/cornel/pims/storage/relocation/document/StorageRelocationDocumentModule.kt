package com.cornel.pims.storage.relocation.document

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 27.02.2018.
 */

@Module
class StorageRelocationDocumentModule(private val activity: StorageRelocationDocumentActivity) {

    @StorageRelocationDocumentScope
    @Provides
    fun providePresenter(): StorageRelocationDocumentPresenter = StorageRelocationDocumentPresenterImpl(activity)

    @StorageRelocationDocumentScope
    @Provides
    fun provideActivity(): StorageRelocationDocumentView = activity
}