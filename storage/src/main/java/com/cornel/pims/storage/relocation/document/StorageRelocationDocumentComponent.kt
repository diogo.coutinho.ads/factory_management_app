package com.cornel.pims.storage.relocation.document

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageRelocationDocumentScope

@StorageRelocationDocumentScope
@Subcomponent(modules = arrayOf(StorageRelocationDocumentModule::class))
interface StorageRelocationDocumentComponent {
    fun inject(activity: StorageRelocationDocumentActivity)
    fun inject(presenter: StorageRelocationDocumentPresenterImpl)
}