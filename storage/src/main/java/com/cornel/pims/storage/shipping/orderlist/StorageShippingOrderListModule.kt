package com.cornel.pims.storage.shipping.orderlist

import dagger.Module
import dagger.Provides

/**
 * Created by KILLdon on 21.02.2018.
 */

@Module
class StorageShippingOrderListModule(private val activity: StorageShippingOrderListActivity) {

    @StorageShippingOrderListScope
    @Provides
    fun providePresenter(): StorageShippingOrderListPresenter = StorageShippingOrderListPresenterImpl(activity)

    @StorageShippingOrderListScope
    @Provides
    fun provideActivity(): StorageShippingOrderListView = activity
}