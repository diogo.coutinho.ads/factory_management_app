package com.cornel.pims.storage.main


import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 04.02.18.
 */

@Module
class StorageMainModule(private val activity: StorageMainActivity) {

    @StorageMainScope
    @Provides
    fun providePresenter(): StorageMainPresenter = StorageMainPresenterImpl(activity)
}
