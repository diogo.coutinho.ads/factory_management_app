package com.cornel.pims.storage.acceptance.fromfactory

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.cornel.pims.core.Barcode
import com.cornel.pims.core.BarcodeType
import com.cornel.pims.core.Formatter
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.common.HistoryUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.android.synthetic.main.acceptance_from_factory_activity.*
import kotlinx.android.synthetic.main.common_history_list_item.view.*
import kotlinx.android.synthetic.main.common_spinner_list_item.view.*
import kotlinx.android.synthetic.main.common_workshop_head_list_item.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 27.02.18.
 */

interface StorageAcceptanceFromFactoryView {
    val component: StorageAcceptanceFromFactoryComponent
    fun setTitle(text: String)
    fun setItemName(text: String)
    fun setProductCode(text: String)

    fun setCreationTimeDate(text: String)
    fun setWorkplaceName(text: String)

    fun setState(text: String)
    fun showOrHideDeletedAt(text: String)
    fun showWorkshopHeads(items: List<String>)
    fun showHistory(items: List<HistoryUnit>)
    fun setOrderId(text: String)
    fun setQuantity(count: Double, abbreviation: String)
    fun setStorage(text: String)
    fun setAreaSpinner(areas: List<AreaListUnit>)
    fun showOrHideAcceptFields(isAcceptable: Boolean)
    fun showError(e: Throwable)
    fun finish()
    fun isOk(ok: Boolean)
    fun getSelectedArea(): AreaListUnit
    fun showDialog(title: String, text: String,
                   actionBtnText: String, callback: () -> Unit)

    fun showConfirmation(text: String, actionBtnText: String, callback: () -> Unit)
}

class StorageAcceptanceFromFactoryActivity
    : BaseView(), StorageAcceptanceFromFactoryView {
    companion object {
        const val EXTRA_ITEM_ID = "item_id"
    }

    @Inject
    override lateinit var presenter: StorageAcceptanceFromFactoryPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy {
        app.component.plus(StorageAcceptanceFromFactoryModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acceptance_from_factory_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        val itemId = intent.getStringExtra(EXTRA_ITEM_ID)

        showOrHideAcceptFields(false)

        showPreloader()
        presenter.onCreate(itemId)
        addListeners()
    }

    override fun onBarcodeScan(barcode: Barcode) {
        if (barcode.type == BarcodeType.AREA) {
            if (!setSelectedArea(barcode.id)) {
                showMessage(getString(R.string.acceptance_from_factory_no_such_area))
            }
        } else {
            super.onBarcodeScan(barcode)
        }
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackBtnClick()
        }
        acceptBtn.onClick {
            presenter.onAcceptBtnClick()
        }
    }

    override fun setTitle(text: String) {
        toolbar.setTitle(text)
    }
    override fun setCreationTimeDate(text: String) = setOrHide(text, creationTimeDate, creationTimeDateContainer)

    override fun setWorkplaceName(text: String) = setOrHide(text, workplace, workplaceContainer)

    override fun setItemName(text: String) = setOrHide(text, productName, productNameContainer)

    override fun setProductCode(text: String) = setOrHide(text, productCode, productCodeContainer)

    override fun setOrderId(text: String) = setOrHide(text, orderId, orderIdContainer)

    override fun setQuantity(count: Double, abbreviation: String) = quantity.setText(getString(R.string.measurement_form, Formatter.format(count), abbreviation))

    override fun setStorage(text: String) = setOrHide(text, targetStorage, targetStorageContainer)

    override fun setState(text: String) = setOrHide(text, state, stateContainer)

    override fun showOrHideDeletedAt(text: String) = setOrHide(text, deleted, deletedContainer)

    override fun showWorkshopHeads(items: List<String>) {
        foremanContainer.removeAllViews()
        if (items.isEmpty()) {
            foremanTitle.visibility = View.GONE
        } else {
            foremanTitle.visibility = View.VISIBLE
            items.forEach {
                val view = layoutInflater
                        .inflate(R.layout.common_workshop_head_list_item, foremanContainer, false)
                view.itemHead.text = it
                foremanContainer.addView(view)
            }
        }
    }

    override fun showHistory(items: List<HistoryUnit>) {
        historyContainer.removeAllViews()
        if (items.isEmpty()) {
            historyTitle.visibility = View.GONE
        } else {
            historyTitle.visibility = View.VISIBLE
            items.forEach {
                val view = layoutInflater
                        .inflate(R.layout.common_history_list_item, historyContainer, false)
                view.itemMessage.text = it.name
                view.itemDate.text = it.date
                historyContainer.addView(view)
            }
        }
    }


    override fun showOrHideAcceptFields(isAcceptable: Boolean) {
        acceptBtn.visibility = if (isAcceptable) View.VISIBLE else View.GONE
        spinnerContainer.visibility = if (isAcceptable) View.VISIBLE else View.GONE

        stateContainer.visibility = if (!isAcceptable) View.VISIBLE else View.GONE
        historyTitle.visibility = if (!isAcceptable) View.VISIBLE else View.GONE
        historyContainer.visibility = if (!isAcceptable) View.VISIBLE else View.GONE
    }

    override fun setAreaSpinner(areas: List<AreaListUnit>) {
        targetAreaSpinner.adapter = AreaSpinnerAdapter(this,
                R.layout.common_area_list_item,
                R.id.itemName,
                areas)
    }

    override fun getSelectedArea(): AreaListUnit = targetAreaSpinner.selectedItem as AreaListUnit


    private fun setSelectedArea(id: String): Boolean {
        val list = (targetAreaSpinner.adapter as AreaSpinnerAdapter).values
        val area = list.find { it.id == id } ?: return false
        val position = list.indexOf(area)
        if (position != -1) {
            targetAreaSpinner.setSelection(position)
            return true
        }
        return false
    }

    class AreaSpinnerAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<AreaListUnit>
    ) : ArrayAdapter<AreaListUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item, parent, false)
            view.itemName.text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item_dropdown, parent, false)
            view.itemName.text = values[position].name
            return view
        }
    }
}