package com.cornel.pims.storage.relocation.document

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.common.StorageListUnit
import com.cornel.pims.storage.core.*
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 27.02.18.
 */

interface StorageRelocationDocumentPresenter : BasePresenter {
    fun onCreate(itemId: String)
    fun onRefresh()
    fun onRelocateClicked()
    fun onBackBtnClick()
    fun onStorageSelected()
}

class StorageRelocationDocumentPresenterImpl(
        private val view: StorageRelocationDocumentView)
    : BasePresenterImpl(view as BaseView), StorageRelocationDocumentPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    lateinit var itemId: String

    lateinit var storage: Storage
    lateinit var area: Area
    lateinit var product: Product

    override fun onCreate(itemId: String) {
        view.component.inject(this)
        this.itemId = itemId
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            // Launch concurrent data loading
            val itemDef = async { dataManager.getStorageItemById(itemId) }
            val storageListDef = async { dataManager.getStorageList() }

            // Await when all the data loaded
            val item = get(itemDef.await())
            val storageList = get(storageListDef.await())
            val storageItemList = storageList.map {
                StorageListUnit(it.id, it.name)
            }
            product = get(dataManager.getProductById(item.productId))
            area = get(dataManager.getAreaById(item.areaId))
            storage = storageList.find {
                it.id == area.storageId
            } ?: throw RuntimeException("Can't find storage with code ${area.storageId}")

            launch(UI) {
                view.apply {
                    setItemName(product.name)
                    setSku(product.code)
                    setQuantity(item.quantity.toString())
                    setMaxQuantity(item.quantity)
                    setSourceStorage(storage.name)
                    setSourceArea(area.name)
                    showStorageSpinner(storageItemList)
                }
            }
        }
    }

    override fun onStorageSelected() {
        info("Selected storage")
        smartLaunch {
            runBlocking {
                val areas = get(dataManager.getAreasByStorageId(view.getSelectedStorage().id))
                        .map {
                            AreaListUnit(it.id, it.name, it.storageItemsCount)
                        }.toMutableList()
                areas.add(0, AreaListUnit("", "None", -1))

                launch(UI) { view.showAreaSpinner(areas.toList()) }
            }
        }
    }

    override fun onRelocateClicked() {
        val quantity = view.getQuantity() ?: return
        val text = appContext.getString(
                R.string.relocation_document_confirm_text, product.name, Formatter.format(quantity))
        val btnText = appContext.getString(R.string.relocation_document_confirm_button)
        view.showConfirmation(text, btnText, this::submitDocument)
    }


    private fun submitDocument() {
        val date = Formats.dateFormat.format(Date())
        val quantity = view.getQuantity() ?: return

        val targetStorageId = view.getSelectedStorage().id

        val targetAreaId = view.getSelectedArea().id


        smartLaunch(finally = {
            super.finally()
            view.finish()
        }) {
            if (targetAreaId.isEmpty()) {
                runBlocking {
                    val element = RelocationShipmentElement(
                            product.id, area.id, quantity
                    )
                    val doc = RelocationShipmentDocument(
                            date, targetStorageId, listOf(element)
                    )

                    info("Submitted shipment document: $doc")
                    dataManager.submitRelocationShipmentDocument(doc)
                    view.isOk(true)
                }
            } else {
                runBlocking {
                    val shipmentElement = RelocationShipmentElement(
                            product.id, area.id, quantity
                    )
                    val shipmentDoc = RelocationShipmentDocument(
                            date, targetStorageId, listOf(shipmentElement)
                    )

                    info("Submitted shipment document: $shipmentDoc")
                    val shipmentId = dataManager.submitRelocationShipmentDocument(shipmentDoc)

                    val acceptanceElement = RelocationAcceptanceElement(
                            product.id, targetAreaId, quantity
                    )
                    val acceptanceDoc = RelocationAcceptanceDocument(
                            date, shipmentId, listOf(acceptanceElement)
                    )

                    info("Submitted acceptance document: $acceptanceDoc")
                    dataManager.submitRelocationAcceptanceDocument(acceptanceDoc)
                    view.isOk(true)
                }
            }
        }
    }

    override fun onBackBtnClick() {
        info("Back button pressed")
        view.finish()
    }

}