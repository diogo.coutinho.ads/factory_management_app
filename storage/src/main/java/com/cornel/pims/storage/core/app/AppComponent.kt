package com.cornel.pims.storage.core.app

import android.app.Application
import com.cornel.pims.storage.acceptance.fromfactory.StorageAcceptanceFromFactoryComponent
import com.cornel.pims.storage.acceptance.fromfactory.StorageAcceptanceFromFactoryModule
import com.cornel.pims.storage.acceptance.fromstorage.StorageAcceptanceFromStorageComponent
import com.cornel.pims.storage.acceptance.fromstorage.StorageAcceptanceFromStorageModule
import com.cornel.pims.storage.acceptance.list.StorageAcceptanceListComponent
import com.cornel.pims.storage.acceptance.list.StorageAcceptanceListModule
import com.cornel.pims.storage.auth.StorageAuthComponent
import com.cornel.pims.storage.auth.StorageAuthModule
import com.cornel.pims.storage.core.DataManagerImpl
import com.cornel.pims.storage.core.helper.APIHelperImpl
import com.cornel.pims.storage.core.helper.SharedPrefHelperImpl
import com.cornel.pims.storage.information.InformationComponent
import com.cornel.pims.storage.information.InformationModule
import com.cornel.pims.storage.main.StorageMainComponent
import com.cornel.pims.storage.main.StorageMainModule
import com.cornel.pims.storage.productinfo.StorageProductInfoComponent
import com.cornel.pims.storage.productinfo.StorageProductInfoModule
import com.cornel.pims.storage.relocation.arealist.StorageRelocationAreaListComponent
import com.cornel.pims.storage.relocation.arealist.StorageRelocationAreaListModule
import com.cornel.pims.storage.relocation.document.StorageRelocationDocumentComponent
import com.cornel.pims.storage.relocation.document.StorageRelocationDocumentModule
import com.cornel.pims.storage.relocation.productlist.StorageRelocationProductListComponent
import com.cornel.pims.storage.relocation.productlist.StorageRelocationProductListModule
import com.cornel.pims.storage.shipping.document.StorageShippingDocumentComponent
import com.cornel.pims.storage.shipping.document.StorageShippingDocumentModule
import com.cornel.pims.storage.shipping.orderlist.StorageShippingOrderListComponent
import com.cornel.pims.storage.shipping.orderlist.StorageShippingOrderListModule
import com.cornel.pims.storage.stocktaking.arealist.StorageStocktakingAreaListComponent
import com.cornel.pims.storage.stocktaking.arealist.StorageStocktakingAreaListModule
import com.cornel.pims.storage.stocktaking.document.StorageStocktakingDocumentComponent
import com.cornel.pims.storage.stocktaking.document.StorageStocktakingDocumentModule
import com.cornel.pims.storage.stocktaking.productlist.StorageStocktakingProductListComponent
import com.cornel.pims.storage.stocktaking.productlist.StorageStocktakingProductListModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by AlexGator on 03.02.18.
 */

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: Application)
    fun inject(dataManager: DataManagerImpl)
    fun inject(sharedPrefHelper: SharedPrefHelperImpl)
    fun inject(apiHelper: APIHelperImpl)

    fun plus(module: StorageAuthModule): StorageAuthComponent
    fun plus(module: StorageMainModule): StorageMainComponent
    fun plus(module: InformationModule): InformationComponent
    fun plus(module: StorageRelocationAreaListModule): StorageRelocationAreaListComponent
    fun plus(module: StorageRelocationProductListModule): StorageRelocationProductListComponent
    fun plus(module: StorageStocktakingAreaListModule): StorageStocktakingAreaListComponent
    fun plus(module: StorageStocktakingProductListModule): StorageStocktakingProductListComponent
    fun plus(module: StorageStocktakingDocumentModule): StorageStocktakingDocumentComponent
    fun plus(module: StorageShippingOrderListModule): StorageShippingOrderListComponent
    fun plus(module: StorageShippingDocumentModule): StorageShippingDocumentComponent
    fun plus(module: StorageAcceptanceListModule): StorageAcceptanceListComponent
    fun plus(module: StorageRelocationDocumentModule): StorageRelocationDocumentComponent
    fun plus(module: StorageAcceptanceFromFactoryModule): StorageAcceptanceFromFactoryComponent
    fun plus(module: StorageAcceptanceFromStorageModule): StorageAcceptanceFromStorageComponent
    fun plus(module: StorageProductInfoModule): StorageProductInfoComponent

}
