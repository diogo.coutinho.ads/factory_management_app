package com.cornel.pims.storage.stocktaking.arealist

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageStocktakingAreaListScope

@StorageStocktakingAreaListScope
@Subcomponent(modules = [StorageStocktakingAreaListModule::class])
interface StorageStocktakingAreaListComponent {
    fun inject(activity: StorageStocktakingAreaListActivity)
    fun inject(activity: StorageStocktakingAreaListPresenterImpl)
}