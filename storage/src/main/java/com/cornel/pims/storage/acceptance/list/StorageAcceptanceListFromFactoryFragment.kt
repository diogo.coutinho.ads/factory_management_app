package com.cornel.pims.storage.acceptance.list

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AcceptanceListItem
import com.cornel.pims.storage.common.ItemListUnit
import com.cornel.pims.storage.core.app.cast
import com.cornel.pims.storage.core.proto.HasComponent
import kotlinx.android.synthetic.main.common_acceptance_list_item.view.*
import org.w3c.dom.Text
import javax.inject.Inject

/**
 * Created by AlexGator on 02.04.18.
 */

interface StorageAcceptanceListFromFactoryView {
    fun setList(data: List<AcceptanceListItem>)
}

class StorageAcceptanceListFromFactoryFragment : Fragment(), StorageAcceptanceListFromFactoryView {

    @Inject
    lateinit var appContext: Context

    @Inject
    lateinit var presenter: StorageAcceptanceListPresenter

    private var adapter: ProductRecyclerAdapter? = null
    private var recycler: RecyclerView? = null

    override fun setList(data: List<AcceptanceListItem>) {
        adapter = ProductRecyclerAdapter(data, this::listener, appContext)
        recycler?.swapAdapter(adapter, false)
    }

    private fun listener(selected: AcceptanceListItem) {
        presenter.onProductSelected(selected)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        recycler = inflater
                .inflate(R.layout.common_recycler, container, false) as RecyclerView
        (recycler as RecyclerView).layoutManager = LinearLayoutManager(context)
        (recycler as RecyclerView).addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        val swipeRefresh = SwipeRefreshLayout(context)
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onProductRefresh()
        }
        swipeRefresh.addView(recycler)

        return swipeRefresh
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val componentActivity = cast<HasComponent<StorageAcceptanceListComponent>>(activity)
                ?: throw RuntimeException("Activity is not initialized or unsupported")
        componentActivity.component.inject(this)

    }

    override fun onResume() {
        super.onResume()
        presenter.onFilterSelected(StorageAcceptanceListView.Filter.FACTORY)
    }

    class ProductRecyclerAdapter(private val data: List<AcceptanceListItem>,
                                 private val listener: (AcceptanceListItem) -> Unit,
                                 private val context: Context)
        : RecyclerView.Adapter<ProductRecyclerAdapter.ViewHolder>() {

        init {
            setHasStableIds(true)
        }

        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view = v
            val itemName: TextView = v.itemName
            val itemCode: TextView = v.itemCode
            val workplace: TextView = v.workplace
            val itemCount: TextView = v.itemCount

            fun bind(item: AcceptanceListItem, listener: (AcceptanceListItem) -> Unit) {
                val skuText = context.getString(R.string.acceptance_list_sku, item.sku.substring(0 until 8))
                itemName.text = item.name
                itemCode.text = skuText
                workplace.text = item.workplace
                itemCount.text = item.quantityWithAbbreviation
                view.setOnClickListener { listener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_acceptance_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}
