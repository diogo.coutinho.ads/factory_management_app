package com.cornel.pims.storage.acceptance.fromstorage

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.cornel.pims.core.Barcode
import com.cornel.pims.core.BarcodeType
import com.cornel.pims.core.Formatter
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.android.synthetic.main.acceptance_from_storage_activity.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 27.02.18.
 */

interface StorageAcceptanceFromStorageView {
    val component: StorageAcceptanceFromStorageComponent
    fun setTitle(text: String)
    fun setSender(text: String)
    fun setSourceStorage(text: String)
    fun setSourceArea(text: String)
    fun setTargetAreaSpinner(areas: List<AreaListUnit>)
    fun setDocumentCode(text: String)
    fun setProductName(text: String)
    fun setProductCode(text: String)
    fun setMaxQuantity(quantity: Double, abbreviation: String)
    fun setQuantity(text: String)

    fun showError(e: Throwable)
    fun finish()
    fun getSelectedArea(): AreaListUnit
    fun getQuantity(): Double?
    fun isOk(ok: Boolean)
    fun showConfirmation(text: String, actionBtnText: String, callback: () -> Unit)
}

class StorageAcceptanceFromStorageActivity : BaseView(), StorageAcceptanceFromStorageView {
    companion object {
        const val EXTRA_DOC_ID = "document_id"
    }

    @Inject
    override lateinit var presenter: StorageAcceptanceFromStoragePresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    private var maxQuantity = 0.0

    override val component by lazy {
        app.component.plus(StorageAcceptanceFromStorageModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acceptance_from_storage_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        val docId = intent.getStringExtra(EXTRA_DOC_ID)

        presenter.onCreate(docId)
        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackPressed()
        }
        acceptBtn.onClick {
            presenter.onSubmit()
        }
        quantity.addTextChangedListener(object : TextWatcher {
            var skip: Boolean = false
            override fun afterTextChanged(e: Editable) {
                if (!skip) {
                    skip = true
                    val string = e.toString()
                    e.clear()
                    e.insert(0, string.replace(',', '.'))
                    skip = false
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })
    }

    override fun onBarcodeScan(barcode: Barcode) {
        if (barcode.type == BarcodeType.AREA) {
            if (!setSelectedArea(barcode.id)) {
                showMessage(getString(R.string.acceptance_from_storage_no_such_area))
            }
        } else {
            super.onBarcodeScan(barcode)
        }
    }

    override fun setTitle(text: String) {
        toolbar.setTitle(text)
    }

    override fun setDocumentCode(text: String) = orderId.setText(text)

    override fun setSender(text: String) = sender.setText(text)

    override fun setSourceStorage(text: String) = sourceStorage.setText(text)

    override fun setSourceArea(text: String) = sourceArea.setText(text)

    override fun setProductName(text: String) = itemName.setText(text)

    override fun setProductCode(text: String) = itemCode.setText(text)

    override fun setMaxQuantity(quantity: Double, abbreviation: String) {
        maxQuantity = quantity
        availableQuantity.text = getString(R.string.acceptance_from_storage_available_quantity, Formatter.format(quantity), abbreviation)
    }

    override fun setQuantity(text: String) = quantity.setText(text)

    override fun setTargetAreaSpinner(areas: List<AreaListUnit>) {
        targetAreaSpinner.adapter = AreaSpinnerAdapter(this,
                R.layout.common_area_list_item,
                R.id.itemName,
                areas)
    }

    override fun getSelectedArea(): AreaListUnit = targetAreaSpinner.selectedItem as AreaListUnit

    override fun getQuantity(): Double? {
        val illegalQuantityErr = getString(R.string.acceptance_from_storage_illegal_quantity)
        val mustBeGreaterZeroErr = getString(R.string.acceptance_from_storage_must_be_greater_zero)
        val tooMuchErr = getString(R.string.acceptance_from_storage_too_much_quantity)

        val quantityValue = try {
            quantity.text.toString().toDouble()
        } catch (e: Throwable) {
            quantity.error = illegalQuantityErr
            return null
        }

        return when {
            quantityValue <= 0.0 -> {
                quantity.error = mustBeGreaterZeroErr
                null
            }
            quantityValue > maxQuantity -> {
                quantity.error = tooMuchErr
                null
            }
            else -> quantityValue
        }
    }

    private fun setSelectedArea(id: String): Boolean {
        val list = (targetAreaSpinner.adapter as AreaSpinnerAdapter).values
        val area = list.find { it.id == id } ?: return false
        val position = list.indexOf(area)
        if (position != -1) {
            targetAreaSpinner.setSelection(position)
            return true
        }
        return false
    }

    class AreaSpinnerAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<AreaListUnit>
    ) : ArrayAdapter<AreaListUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item, parent, false)
            view.findViewById<TextView>(R.id.itemName).text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item_dropdown, parent, false)
            view.findViewById<TextView>(R.id.itemName).text = values[position].name
            return view
        }
    }
}