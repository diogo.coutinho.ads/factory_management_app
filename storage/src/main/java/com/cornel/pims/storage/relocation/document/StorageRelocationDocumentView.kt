package com.cornel.pims.storage.relocation.document

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.common.StorageListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.android.synthetic.main.common_spinner_list_item.view.*
import kotlinx.android.synthetic.main.relocation_document_activity.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by AlexGator on 27.02.18.
 */

interface StorageRelocationDocumentView {
    val component: StorageRelocationDocumentComponent
    fun setItemName(name: String)
    fun setSku(sku: String)
    fun setQuantity(quantity: String)
    fun setSourceStorage(name: String)
    fun setSourceArea(name: String)
    fun getQuantity(): Double?
    fun isOk(ok: Boolean)
    fun getSelectedStorage(): StorageListUnit
    fun showStorageSpinner(storageList: List<StorageListUnit>)
    fun showError(e: Throwable)
    fun finish()
    fun showAreaSpinner(areaList: List<AreaListUnit>)
    fun getSelectedArea(): AreaListUnit
    fun showConfirmation(text: String, actionBtnText: String, callback: () -> Unit)
    fun setMaxQuantity(quantity: Double)
}

class StorageRelocationDocumentActivity : BaseView(), StorageRelocationDocumentView {

    companion object {
        const val EXTRA_ITEM_ID = "item_id"
    }

    @Inject
    override lateinit var presenter: StorageRelocationDocumentPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    private var maxQuantity: Double = 0.0

    override val component by lazy {
        app.component.plus(StorageRelocationDocumentModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.relocation_document_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        val itemId = intent.getStringExtra(EXTRA_ITEM_ID)

        presenter.onCreate(itemId)

        addListeners()
    }

    fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackBtnClick()
        }
        relocateBtn.onClick {
            presenter.onRelocateClicked()
        }
        itemQuantity.addTextChangedListener(object : TextWatcher {
            var skip: Boolean = false
            override fun afterTextChanged(e: Editable) {
                if (!skip) {
                    skip = true
                    val string = e.toString()
                    e.clear()
                    e.insert(0, string.replace(',', '.'))
                    skip = false
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })
    }

    override fun setItemName(name: String) = itemName.setText(name)
    override fun setSku(sku: String) = itemCode.setText(sku)
    override fun setQuantity(quantity: String) = itemQuantity.setText(quantity)
    override fun setSourceStorage(name: String) = sourceStorage.setText(name)
    override fun setSourceArea(name: String) = sourceZone.setText(name)
    override fun showStorageSpinner(storageList: List<StorageListUnit>) {
        targetStorageSpinner.adapter = StorageSpinnerAdapter(this,
                R.layout.common_spinner_list_item,
                R.id.itemName,
                storageList)
        targetStorageSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                presenter.onStorageSelected()
            }
        }
    }

    override fun setMaxQuantity(quantity: Double) {
        this.maxQuantity = quantity
    }

    override fun showAreaSpinner(areaList: List<AreaListUnit>) {
        targetAreaSpinner.adapter = AreaSpinnerAdapter(this,
                R.layout.common_spinner_list_item,
                R.id.itemName,
                areaList)
    }

    override fun getSelectedStorage() = targetStorageSpinner.selectedItem as StorageListUnit

    override fun getSelectedArea() = targetAreaSpinner.selectedItem as AreaListUnit


    override fun getQuantity(): Double? {
        val q = itemQuantity.text.toString()
        return try {
            val quantity = q.toDouble()
            if (quantity <= 0.0 || quantity > maxQuantity)
                throw IllegalArgumentException(getString(R.string.relocation_document_illegal_quantity_exception))
            quantity
        } catch (e: Throwable) {
            itemQuantity.error = e.message
            null
        }
    }

    class StorageSpinnerAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<StorageListUnit>
    ) : ArrayAdapter<StorageListUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item, parent, false)
            view.itemName.text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item_dropdown, parent, false)
            view.itemName.text = values[position].name
            return view
        }
    }

    class AreaSpinnerAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<AreaListUnit>
    ) : ArrayAdapter<AreaListUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item, parent, false)
            view.itemName.text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item_dropdown, parent, false)
            view.itemName.text = values[position].name
            return view
        }
    }
}