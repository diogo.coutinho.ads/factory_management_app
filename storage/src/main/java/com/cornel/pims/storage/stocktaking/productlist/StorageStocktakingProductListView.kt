package com.cornel.pims.storage.stocktaking.productlist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.ItemListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.stocktaking.document.StorageStocktakingDocumentActivity
import kotlinx.android.synthetic.main.common_item_list_item.view.*
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

/**
 * Created by KILLdon on 21.02.2018.
 */

interface StorageStocktakingProductListView {
    val component: StorageStocktakingProductListComponent
    fun showItems(data: List<ItemListUnit>)
    fun navigateToStocktakingDocument(itemId: String)
    fun showError(e: Throwable)
    fun showPreloader()
    fun hidePreloader()
    fun setTitle(title: String)
    fun finish()
}

/*
 * whe levaecall on pause
  * in on resume get info
 */
class StorageStocktakingProductListActivity : BaseView(), StorageStocktakingProductListView {

    companion object {
        const val EXTRA_ZONE_ID = "zone_id"
        const val REQUEST_CODE_DOCUMENT = 1
    }

    @Inject
    override lateinit var presenter: StorageStocktakingProductListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageStocktakingProductListModule(this)) }

    lateinit var areaId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        setSupportActionBar(toolbar)

        component.inject(this)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        areaId = intent.getStringExtra(EXTRA_ZONE_ID)

        presenter.onCreate(areaId)

        addListeners()
    }

    fun onRecyclerItemClick(item: ItemListUnit) {
        presenter.onItemSelected(item)
    }

    override fun showItems(data: List<ItemListUnit>) {
        recycler.swapAdapter(RecyclerAdapter(data, this::onRecyclerItemClick, this),
                false)
    }

    override fun setTitle(title: String) {
        toolbar.title = title
    }

    override fun navigateToStocktakingDocument(itemId: String) {
        startActivityForResult(intentFor<StorageStocktakingDocumentActivity>(
                StorageStocktakingDocumentActivity.EXTRA_ITEM_ID to itemId
        ), 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                showMessage(getString(R.string.stocktaking_product_list_success_send))
            }
            Activity.RESULT_CANCELED -> {
                showMessage(getString(R.string.stocktaking_product_list_failed_send))
            }
        }
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.setOnClickListener {
            presenter.onBackButton()
        }
    }

    class RecyclerAdapter(val data: List<ItemListUnit>,
                          private val listener: (ItemListUnit) -> Unit,
                          private val context: Context)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view = v
            val itemName: TextView = v.itemName
            val sku: TextView = v.itemCode
            val itemCount: TextView = v.itemCount

            fun bind(item: ItemListUnit, listener: (ItemListUnit) -> Unit) {

                val skuText = context.getString(R.string.stocktaking_product_list_code,
                        item.code)
                itemName.text = item.name
                sku.text = skuText
                itemCount.text = item.quantityWithAbbreviation
                view.setOnClickListener { listener(item) }
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_item_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}