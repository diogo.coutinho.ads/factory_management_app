package com.cornel.pims.storage.shipping.document

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageShippingDocumentScope

@StorageShippingDocumentScope
@Subcomponent(modules = arrayOf(StorageShippingDocumentModule::class))
interface StorageShippingDocumentComponent {
    fun inject(activity: StorageShippingDocumentActivity)
    fun inject(activity: StorageShippingDocumentPresenterImpl)
}