package com.cornel.pims.storage.relocation.arealist

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageRelocationAreaListScope

@StorageRelocationAreaListScope
@Subcomponent(modules = [StorageRelocationAreaListModule::class])
interface StorageRelocationAreaListComponent {
    fun inject(activity: StorageRelocationAreaListActivity)
    fun inject(activity: StorageRelocationAreaListPresenterImpl)
}