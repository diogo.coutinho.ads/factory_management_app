package com.cornel.pims.storage.stocktaking.productlist

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageStocktakingProductListScope

@StorageStocktakingProductListScope
@Subcomponent(modules = [StorageStocktakingProductListModule::class])
interface StorageStocktakingProductListComponent {
    fun inject(activity: StorageStocktakingProductListActivity)
    fun inject(activity: StorageStocktakingProductListPresenterImpl)
}