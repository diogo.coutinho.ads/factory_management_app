package com.cornel.pims.storage.stocktaking.document

import dagger.Module
import dagger.Provides

/**
 * Created by KILLdon on 25.02.2018.
 */

@Module
class StorageStocktakingDocumentModule(private val activity: StorageStocktakingDocumentActivity) {

    @StorageStocktakingDocumentScope
    @Provides
    fun providePresenter(): StorageStocktakingDocumentPresenter = StorageStocktakingDocumentPresenterImpl(activity)

    @StorageStocktakingDocumentScope
    @Provides
    fun provideActivity(): StorageStocktakingDocumentView = activity
}