package com.cornel.pims.storage.relocation.arealist

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.relocation.productlist.StorageRelocationProductListActivity
import kotlinx.android.synthetic.main.common_area_list_item.*
import kotlinx.android.synthetic.main.common_area_list_item.view.*
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by KILLdon on 21.02.2018.
 */

interface StorageRelocationAreaListView {
    val component: StorageRelocationAreaListComponent
    fun showItems(data: List<AreaListUnit>)
    fun navigateToProductView(zoneId: String)
    fun showPreloader()
    fun hidePreloader()
    fun showError(e: Throwable)
    fun setQuantity(q: String)
    fun finish()
}

class StorageRelocationAreaListActivity : BaseView(), StorageRelocationAreaListView {

    @Inject
    override lateinit var presenter: StorageRelocationAreaListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageRelocationAreaListModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        setSupportActionBar(toolbar)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        component.inject(this)

        presenter.onCreate()

        addListeners()
    }

    fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackBtnClick()
        }
    }

    fun onRecyclerItemClick(item: AreaListUnit) {
        presenter.onItemSelected(item)
    }

    override fun showItems(data: List<AreaListUnit>) {
        recycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick),
                false)
    }

    override fun navigateToProductView(zoneId: String) {
        startActivity(intentFor<StorageRelocationProductListActivity>(
                StorageRelocationProductListActivity.EXTRA_AREA_ID to zoneId))
    }


    class RecyclerAdapter(private val data: List<AreaListUnit>,
                          private val listener: (AreaListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {


        class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view = v
            val areaName: TextView = v.itemName
            val areaCount: TextView = v.itemCount

            fun bind(item: AreaListUnit, listener: (AreaListUnit) -> Unit) {
                areaName.text = item.name
                areaCount.text = item.quantity.toString()
                view.setOnClickListener { listener(item) }
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun getItemId(position: Int): Long = data[position].id.toLong()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_area_list_item, parent, false)
            return ViewHolder(v)
        }

    }

    override fun setQuantity(q: String) = itemCount.setText(q)
}

