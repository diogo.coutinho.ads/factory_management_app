package com.cornel.pims.storage.stocktaking.document

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import com.cornel.pims.storage.R
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.android.synthetic.main.stocktaking_document_activity.*
import org.jetbrains.anko.sdk25.coroutines.textChangedListener
import javax.inject.Inject

/**
 * Created by KILLdon on 25.02.18.
 */

interface StorageStocktakingDocumentView {
    val component: StorageStocktakingDocumentComponent

    fun setName(text: String)
    fun setSKU(text: String)
    fun setSystemQuantity(text: String)
    fun setActualQuantity(text: String)
    fun showPreloader()
    fun hidePreloader()
    fun isOk(ok: Boolean)
    fun showError(e: Throwable)
    fun finish()
    fun getQuantitySafe(): Double?
    fun showConfirmation(text: String, actionBtnText: String, callback: () -> Unit)
    fun getQuantity(): Double?
}

class StorageStocktakingDocumentActivity : BaseView(), StorageStocktakingDocumentView {

    companion object {
        val EXTRA_ITEM_ID = "item_id"
    }

    @Inject
    override lateinit var presenter: StorageStocktakingDocumentPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageStocktakingDocumentModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.stocktaking_document_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        val itemId = intent.getStringExtra(EXTRA_ITEM_ID)

        presenter.onCreate(itemId)

        addListeners()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    private fun addListeners() {
        confirmBtn.setOnClickListener {
            presenter.onStocktakingBtnClick()
        }
        goBackBtn.setOnClickListener {
            presenter.onBackButton()
        }
        actualQuantity.addTextChangedListener(object : TextWatcher{
            var skip: Boolean = false
            override fun afterTextChanged(e: Editable) {
                if (!skip) {
                    skip = true
                    val string = e.toString()
                    e.clear()
                    e.insert(0, string.replace(',', '.'))
                    skip = false
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })
    }

    override fun setName(text: String) = itemName.setText(text)
    override fun setSKU(text: String) = itemCode.setText(text)
    override fun setSystemQuantity(text: String) = systemQuantity.setText(text)
    override fun setActualQuantity(text: String) = actualQuantity.setText(text)

    override fun getQuantitySafe(): Double? {
        return try {
            val quantity = actualQuantity.text.toString().toDouble()
            if (quantity <= 0L)
                throw IllegalArgumentException()
            quantity
        } catch (e: Throwable) {
            showMessage(getString(R.string.stocktaking_document_illegal_quantity_exception))
            actualQuantity.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(actualQuantity, InputMethodManager.SHOW_IMPLICIT)
            null
        }
    }

    override fun getQuantity(): Double? {
        return systemQuantity.text.toString().toDouble()
    }
}