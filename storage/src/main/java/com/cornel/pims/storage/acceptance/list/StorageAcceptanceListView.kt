package com.cornel.pims.storage.acceptance.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.acceptance.fromfactory.StorageAcceptanceFromFactoryActivity
import com.cornel.pims.storage.acceptance.fromstorage.StorageAcceptanceFromStorageActivity
import com.cornel.pims.storage.common.AcceptanceListItem
import com.cornel.pims.storage.common.DocumentListUnit
import com.cornel.pims.storage.common.ItemListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.core.proto.HasComponent
import kotlinx.android.synthetic.main.acceptance_list_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by AlexGator on 24.02.18.
 */

interface StorageAcceptanceListView {
    enum class Filter { FACTORY, STORAGE }

    val component: StorageAcceptanceListComponent
    fun showProducts(data: List<AcceptanceListItem>)
    fun showDocuments(data: List<DocumentListUnit>)
    fun navigateToAcceptanceDocument(itemId: String)
    fun navigateToAcceptRelocationDocument(itemId: String)
    fun hidePreloader()
    fun showError(e: Throwable)
    fun finish()
}

class StorageAcceptanceListActivity
    : BaseView(), StorageAcceptanceListView, HasComponent<StorageAcceptanceListComponent> {

    companion object {
        const val REQUEST_CODE_FROM_FACTORY = 1
        const val REQUEST_CODE_FROM_STORAGE = 2
    }

    @Inject
    override lateinit var presenter: StorageAcceptanceListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    @Inject
    lateinit var fromFactory: StorageAcceptanceListFromFactoryView

    @Inject
    lateinit var fromStorage: StorageAcceptanceListFromStorageView

    override val component by lazy { app.component.plus(StorageAcceptanceListModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acceptance_list_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        pager.adapter = PagerAdapter(supportFragmentManager)

        presenter.onCreate()

        addListeners()
    }

    private fun addListeners() {
        goBackBtn.onClick {
            presenter.onBackBtnClick()
        }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab) {
                pager.setCurrentItem(tab.position, true)
            }
        })

        pager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                tabLayout.getTabAt(position)!!.select()
            }
        })
    }

    override fun showProducts(data: List<AcceptanceListItem>) {
        fromFactory.setList(data)
    }

    override fun showDocuments(data: List<DocumentListUnit>) {
        fromStorage.setList(data)
    }

    override fun navigateToAcceptanceDocument(itemId: String) {
        startActivityForResult(intentFor<StorageAcceptanceFromFactoryActivity>(
                StorageAcceptanceFromFactoryActivity.EXTRA_ITEM_ID to itemId
        ), REQUEST_CODE_FROM_FACTORY)
    }

    override fun navigateToAcceptRelocationDocument(itemId: String) {
        startActivityForResult(intentFor<StorageAcceptanceFromStorageActivity>(
                StorageAcceptanceFromStorageActivity.EXTRA_DOC_ID to itemId
        ), REQUEST_CODE_FROM_STORAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_FROM_STORAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    showMessage(getString(R.string.acceptance_list_success_accept))
                } else {
                    showMessage(getString(R.string.acceptance_list_fail_accept))
                }
            }
            REQUEST_CODE_FROM_FACTORY -> {
                if (resultCode == Activity.RESULT_OK) {
                    showMessage(getString(R.string.acceptance_list_success_accept))
                } else {
                    showMessage(getString(R.string.acceptance_list_fail_accept))
                }
            }
        }
    }

    inner class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment = when (position) {
            0 -> fromFactory as Fragment
            1 -> fromStorage as Fragment
            else -> throw RuntimeException("Unknown position: $position")
        }

        override fun getCount() = 2
    }
}
