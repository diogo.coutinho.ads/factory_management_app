package com.cornel.pims.storage.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.text.SimpleDateFormat
import java.util.*

object Formats {
    val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale("ru"))
}


enum class QError {
        AUTH,
        TOKEN,
        SERVER
}

data class QResponse <T> (
        val data: T?,
        val error: QError?
)

@Serializable
data class OAuthJsonResponse(
        @SerialName("access_token")
        val accessToken: String,
        @SerialName("token_type")
        val tokenType: String,
        @SerialName("expire_in")
        val expireIn: String,
        @SerialName("refresh_token")
        val refreshToken: String,
        val scope: String,
        @SerialName("created_at")
        val createdAt: String)

data class User(
        val id: String,
        val name: String
)

data class Storage(
        val id: String,
        val name: String,
        val code: String)

data class Area(
        val id: String,
        val name: String,
        val code: String,
        val storageId: String,
        val storageItemsCount: Int
)

data class StorageItem(
        val id: String,
        val quantity: Double,
        val productId: String,
        val areaId: String,
        val partnerName: String,
        val orderId: String,
        val abbreviation: String
)

data class ProductionItem(
        val id: String,
        val partnerName: String,
        val shippingDate: String,
        val user: User,
        val common: String
)

data class Product(
        val id: String,
        val name: String,
        val code: String,
        val abbreviation: String
)

data class AcceptanceDocument(
        val date: String,
        val areaId: String,
        val partnerId: String?,
        val elements: List<AcceptanceElement>
)

data class AcceptanceElement(
        val productId: String,
        val productAssignmentId: String,
        val quantity: Double
)

data class RelocationDocument(
        val code: String,
        val targetStorageId: String,
        val targetStorageName: String,
        val sourceStorageId: String,
        val sourceStorageName: String,
        val sourceAreaId: String,
        val sourceAreaName: String,
        val userFullname: String,
        val product: Product,
        val quantity: Double
)

data class RelocationDocumentInfo(
        val id: String,
        val date: String,
        val elements: List<RelocationDocumentElement>
)

data class RelocationDocumentElement(
        val productName: String,
        val productCode: String,
        val abbreviation: String,
        val quantity: Double
)

data class RelocationAcceptanceDocument(
        val date: String,
        val parentId: String,
        val elements: List<RelocationAcceptanceElement>
)

data class RelocationAcceptanceElement(
        val productId: String,
        val targetAreaId: String,
        val quantity: Double
)

data class RelocationShipmentDocument(
        val date: String,
        val storageId: String,
        val elements: List<RelocationShipmentElement>
)

data class RelocationShipmentElement(
        val productId: String,
        val sourceAreaId: String,
        val quantity: Double
)

data class StocktakingDocument(
        val date: String,
        val senderId: String,
        val areaId: String,
        val elements: List<StocktakingElement>
)

data class StocktakingElement(
        val productId: String,
        val quantity: Double
)

data class ProductionDocument(
        val id: String,
        val partner: String,
        val shippingDate: String,
        val manager: String,
        val comment: String
)

data class ShipmentDocument(
        val date: String,
        val partnerId: String?,
        val elements: List<ShipmentElement>
)

data class ShipmentElement(
        val productId: String,
        val areaId: String,
        val quantity: Double)

data class AreaWithQuantity(
        val areaId: String,
        val areaName: String,
        val quantity: Double
)

data class Order(
        val orderId: String,
        val orderNumber: Long,
        val partnerId: String,
        val partnerName: String,
        val elements: List<OrderElement> = emptyList()
)

data class OrderElement(
        val productId: String,
        val productName: String,
        val productDate: String,
        val abbreviation: String,
        val quantity: Double
)

data class AcceptanceAssignment(
        val id: String,
        val code: String,
        val state: String,
        val deletedAt: String?,
        val quantity: Double,
        val productId: String,
        val abbreviation: String,
        val creationDate: String,
        val workplaceName: String,
        val workshopHeads: List<String>,
        val history: List<HistoryUnit>
)

data class HistoryUnit(
        val message: String,
        val date: String
)

enum class AssignmentState(val value: String) {
        ARRIVED("arrived_at_stage"),
        IN_PROGRESS("work_in_progress"),
        PAUSED("paused_work"),
        WAIT_QC("waiting_for_technical_control"),
        QC_IN_PROGRESS("technical_control_in_progress"),
        REJECTED("rejected"),
        WAIT_REV("waiting_for_revision"),
        REV_IN_PROGRESS("revision_in_progress"),
        WAIT_ACCEPT("waiting_for_acceptance"),
        ACCEPT_STORAGE("accepted_to_storage"),
        UNKNOWN("")
}

data class ChangedStorageItem(
        val id: String,
        val productId: String,
        val areaId: String,
        val storageId: String,
        val quantity: Long
)

