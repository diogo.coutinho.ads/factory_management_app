package com.cornel.pims.storage.stocktaking.arealist

import dagger.Module
import dagger.Provides

/**
 * Created by KILLdon on 21.02.2018.
 */

@Module
class StorageStocktakingAreaListModule(private val activity: StorageStocktakingAreaListActivity) {

    @StorageStocktakingAreaListScope
    @Provides
    fun providePresenter(): StorageStocktakingAreaListPresenter = StorageStocktakingAreaListPresenterImpl(activity)

    @StorageStocktakingAreaListScope
    @Provides
    fun provideActivity(): StorageStocktakingAreaListView = activity
}