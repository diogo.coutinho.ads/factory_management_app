package com.cornel.pims.storage.acceptance.fromfactory

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 28.02.18.
 */

@Module
class StorageAcceptanceFromFactoryModule(private val activity: StorageAcceptanceFromFactoryActivity) {

    @StorageAcceptanceFromFactoryScope
    @Provides
    fun providePresenter(): StorageAcceptanceFromFactoryPresenter = StorageAcceptanceFromFactoryPresenterImpl(activity)

    @StorageAcceptanceFromFactoryScope
    @Provides
    fun provideActivity(): StorageAcceptanceFromFactoryView = activity
}
