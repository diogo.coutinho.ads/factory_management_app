package com.cornel.pims.storage.acceptance.list

import dagger.Module
import dagger.Provides

/**
 * Created by user on 24.02.18.
 */

@Module
class StorageAcceptanceListModule(private val activity: StorageAcceptanceListActivity) {
    private lateinit var presenter: StorageAcceptanceListPresenter
    @StorageAcceptanceListScope
    @Provides
    fun providePresenter(): StorageAcceptanceListPresenter {
        if (!this::presenter.isInitialized) {
            presenter = StorageAcceptanceListPresenterImpl(activity)
        }
        return presenter
    }
}

@Module
class StorageAcceptanceListFromFactoryModule {
    @StorageAcceptanceListScope
    @Provides
    fun provideFragment(): StorageAcceptanceListFromFactoryView = StorageAcceptanceListFromFactoryFragment()
}

@Module
class StorageAcceptanceListFromStorageModule {
    @StorageAcceptanceListScope
    @Provides
    fun provideFragment(): StorageAcceptanceListFromStorageView = StorageAcceptanceListFromStorageFragment()
}