package com.cornel.pims.storage.relocation.productlist

import dagger.Module
import dagger.Provides

/**
 * Created by KILLdon on 21.02.2018.
 */

@Module
class StorageRelocationProductListModule(private val activity: StorageRelocationProductListActivity) {

    @StorageRelocationProductListScope
    @Provides
    fun providePresenter(): StorageRelocationProductListPresenter = StorageRelocationProductListPresenterImpl(activity)

    @StorageRelocationProductListScope
    @Provides
    fun provideActivity(): StorageRelocationProductListView = activity
}