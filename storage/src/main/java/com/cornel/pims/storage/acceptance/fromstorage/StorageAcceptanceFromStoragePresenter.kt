package com.cornel.pims.storage.acceptance.fromstorage

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.core.*
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 28.02.18.
 */

interface StorageAcceptanceFromStoragePresenter : BasePresenter {
    fun onCreate(docId: String)
    fun onRefresh()
    fun onBackPressed()
    fun onSubmit()
}

class StorageAcceptanceFromStoragePresenterImpl(private val view: StorageAcceptanceFromStorageView)
    : BasePresenterImpl(view as BaseView), StorageAcceptanceFromStoragePresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var docId: String

    private lateinit var document: RelocationDocument

    override fun onCreate(docId: String) {
        view.component.inject(this)
        this.docId = docId
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        // TODO solve measurement issue
        info("Requesting document with code=$docId")
        runBlocking {
            document = get(dataManager.getRelocationDocumentById(docId))
            val sourceAreaDef = async { dataManager.getAreaById(document.sourceAreaId) }
            val storageListDef = async {
                dataManager.getStorageList()
            }
            val storageList = get(storageListDef.await())
            val sourceStorageDef = async {
                storageList
                        .find { it.id == document.sourceStorageId }
            }
            val targetStorageDef = async {
                storageList
                        .find { it.id == document.targetStorageId }
            }
            val areaListDef = async {
                get(dataManager.getAreasByStorageId(dataManager.getCurrentStorageId()))
                        .map {
                            AreaListUnit(it.id, it.name, it.storageItemsCount)
                        }
            }
            val sourceStorage = sourceStorageDef.await()
                    ?: throw RuntimeException("Не удалось найти склад с указанным идентификатором:" +
                            " ${document.sourceStorageId}")
            val sourceArea = get(sourceAreaDef.await())
            val targetStorage = targetStorageDef.await()
                    ?: throw RuntimeException("Не удалось найти склад с указанным идентификатором:" +
                            " ${document.targetStorageId}")
            val areaList = areaListDef.await()
            launch(UI) {
                view.apply {
                    setTitle(appContext.getString(R.string.acceptance_from_storage_title, targetStorage.name))
                    setTargetAreaSpinner(areaList)
                    setSourceArea(sourceArea.name)
                    setSourceStorage(sourceStorage.name)
                    setDocumentCode(document.code)
                    setSender(document.userFullname)
                    setProductName(document.product.name)
                    setProductCode(document.product.code)
                    setMaxQuantity(document.quantity, document.product.abbreviation)
                    setQuantity(document.quantity.toString())
                }
            }

        }
    }

    override fun onBackPressed() {
        info("Back button pressed")
        view.finish()
    }

    override fun onSubmit() {
        val quantity = view.getQuantity() ?: return

        val text = appContext.getString(
                R.string.acceptance_from_storage_confirm_text, document.product.name, Formatter.format(quantity))
        val buttonText = appContext.getString(R.string.acceptance_from_storage_confirm_button)
        view.showConfirmation(text, buttonText, this::submitDocument)
    }

    private fun submitDocument() {
        val date = Formats.dateFormat.format(Date())
        val areaId = view.getSelectedArea().id
        val quantity = view.getQuantity() ?: return

        smartLaunch(onException = {
            launch(UI) {
                view.isOk(false)
            }
            super.onException(it)
        }, finally = {
            super.finally()
        }) {
            runBlocking {
                val element = RelocationAcceptanceElement(
                        document.product.id, areaId, quantity
                )
                val doc = RelocationAcceptanceDocument(
                        date, docId, listOf(element)
                )
                info("Submitted document: $doc")
                dataManager.submitRelocationAcceptanceDocument(doc)
                view.isOk(true)
                view.finish()
            }
        }
    }
}

