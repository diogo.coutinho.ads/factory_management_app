package com.cornel.pims.storage.stocktaking.productlist

import dagger.Module
import dagger.Provides

/**
 * Created by KILLdon on 21.02.2018.
 */

@Module
class StorageStocktakingProductListModule(private val activity: StorageStocktakingProductListActivity) {

    @StorageStocktakingProductListScope
    @Provides
    fun providePresenter(): StorageStocktakingProductListPresenter = StorageStocktakingProductListPresenterImpl(activity)

    @StorageStocktakingProductListScope
    @Provides
    fun provideActivity(): StorageStocktakingProductListView = activity
}