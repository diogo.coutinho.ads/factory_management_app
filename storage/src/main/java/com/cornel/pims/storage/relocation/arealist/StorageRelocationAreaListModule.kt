package com.cornel.pims.storage.relocation.arealist

import dagger.Module
import dagger.Provides

/**
 * Created by KILLdon on 21.02.2018.
 */

@Module
class StorageRelocationAreaListModule(private val activity: StorageRelocationAreaListActivity) {

    @StorageRelocationAreaListScope
    @Provides
    fun providePresenter(): StorageRelocationAreaListPresenter =
            StorageRelocationAreaListPresenterImpl(activity)

    @StorageRelocationAreaListScope
    @Provides
    fun provideActivity(): StorageRelocationAreaListView = activity
}