package com.cornel.pims.storage.relocation.productlist

import android.content.Context
import android.util.Log
import com.cornel.pims.core.Formatter
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.ItemListUnit
import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by KILLdon on 21.02.2018.
 */

interface StorageRelocationProductListPresenter : BasePresenter {
    fun onCreate(areaId: String)
    fun onRefresh()
    fun onItemSelected(item: ItemListUnit)
    fun onBackButtonClick()
}

class StorageRelocationProductListPresenterImpl(val view: StorageRelocationProductListView)
    : BasePresenterImpl(view as BaseView), StorageRelocationProductListPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var areaId: String

    override fun onCreate(areaId: String) {
        view.component.inject(this)
        this.areaId = areaId
    }

    override fun doOnResumeAsync() {
        updateDate()
    }

    override fun onRefresh() {
        smartLaunch {
            updateDate()
        }
    }

    private fun updateDate() {
        runBlocking {
            val area = get(dataManager.getAreaById(areaId))
            launch(UI) {
                view.setTitle(area.name)
            }
            val data = get(dataManager.getStorageItemsByAreaId(areaId)).map {
                val product = get(dataManager.getProductById(it.productId))
                ItemListUnit(it.id,
                        product.name,
                        product.code,
                        appContext.getString(R.string.measurement_form, Formatter.format(it.quantity), it.abbreviation))
            }
//            Log.i("Abbreviation_test", data[0].abbreviation)
            launch(UI) { view.showItems(data) }
        }
    }

    override fun onItemSelected(item: ItemListUnit) {
        info("Item selected: $item")
        view.navigateToTransferDocument(item.id)
    }

    override fun onBackButtonClick() {
        info("Back button pressed")
        view.finish()
    }

}