package com.cornel.pims.storage.productinfo

import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by AlexGator on 14.02.18.
 */

interface StorageProductInfoPresenter : BasePresenter {
    fun onCreate(itemId: String)
    fun onRefresh()
    fun onRelocationBtnClick()
    fun onStocktakingBtnClick()
    fun onGoBackBtnClick()
}

class StorageProductInfoPresenterImpl(private val view: StorageProductInfoView)
    : BasePresenterImpl(view as BaseView), StorageProductInfoPresenter {

    @Inject
    lateinit var dataManager: DataManager

    private lateinit var itemId: String

    override fun onCreate(itemId: String) {
        view.component.inject(this)
        this.itemId = itemId
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val item = get(dataManager.getStorageItemById(itemId))
            val product = get(dataManager.getProductById(item.productId))
            val area = get(dataManager.getAreaById(item.areaId))
            val storage = get(dataManager.getStorageList()).find { it.id == area.storageId }
            info(product.toString())
            launch(UI) {
                view.apply {
                    setName(product.name)
                    setCode(product.code)
                    setQuantity(item.quantity.toString())
                    setStorage(storage?.name ?: "")
                    setArea(area.name)
                    setPartner(item.partnerName)
                    setOrderId(item.orderId)
                }
            }
        }
    }

    override fun onRelocationBtnClick() {
        view.navigateToTransferDocument(itemId)
    }

    override fun onStocktakingBtnClick() {
        view.navigateToStocktakingDocument(itemId)
    }

    override fun onGoBackBtnClick() {
        view.finish()
    }
}