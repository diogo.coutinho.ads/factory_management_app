package com.cornel.pims.storage.productinfo

import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by AlexGator on 15.02.18.
 */

@Scope
annotation class StorageProductInfoScope

@StorageProductInfoScope
@Subcomponent(modules = [StorageProductInfoModule::class])
interface StorageProductInfoComponent {
    fun inject(activity: StorageProductInfoActivity)
    fun inject(presenter: StorageProductInfoPresenterImpl)
}