package com.cornel.pims.storage.stocktaking.productlist

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.ItemListUnit
import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by KILLdon on 21.02.2018.
 */

interface StorageStocktakingProductListPresenter : BasePresenter {
    fun onCreate(areaId: String)
    fun onRefresh()
    fun onItemSelected(item: ItemListUnit)
    fun onBackButton()
}

class StorageStocktakingProductListPresenterImpl(private val view: StorageStocktakingProductListView)
    : BasePresenterImpl(view as BaseView), StorageStocktakingProductListPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var areaId: String

    override fun onCreate(areaId: String) {
        view.component.inject(this)
        this.areaId = areaId
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val area = get(dataManager.getAreaById(areaId))
            launch(UI) {
                view.setTitle(area.name)
            }
            val data = get(dataManager.getStorageItemsByAreaId(areaId)).map {
                val product = get(dataManager.getProductById(it.productId))
                ItemListUnit(it.id,
                        product.name,
                        product.code,
                        appContext.getString(R.string.measurement_form, Formatter.format(it.quantity), it.abbreviation))
            }
            launch(UI) { view.showItems(data) }
        }
    }

    override fun onItemSelected(item: ItemListUnit) {
        info("Item selected: $item")
        view.navigateToStocktakingDocument(item.id)
    }

    override fun onBackButton() {
        info("Back button pressed")
        view.finish()
    }
}