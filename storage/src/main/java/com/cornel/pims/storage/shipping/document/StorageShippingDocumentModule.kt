package com.cornel.pims.storage.shipping.document

import dagger.Module
import dagger.Provides

/**
 * Created by KILLdon on 25.02.2018.
 */

@Module
class StorageShippingDocumentModule(private val activity: StorageShippingDocumentActivity) {

    @StorageShippingDocumentScope
    @Provides
    fun providePresenter(): StorageShippingDocumentPresenter = StorageShippingDocumentPresenterImpl(activity)

    @StorageShippingDocumentScope
    @Provides
    fun provideActivity(): StorageShippingDocumentView = activity
}