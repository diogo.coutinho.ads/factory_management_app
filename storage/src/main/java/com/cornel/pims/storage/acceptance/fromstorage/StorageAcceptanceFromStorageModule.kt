package com.cornel.pims.storage.acceptance.fromstorage

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 28.02.18.
 */

@Module
class StorageAcceptanceFromStorageModule(private val activity: StorageAcceptanceFromStorageActivity) {

    @StorageAcceptanceFromStorageScope
    @Provides
    fun providePresenter(): StorageAcceptanceFromStoragePresenter = StorageAcceptanceFromStoragePresenterImpl(activity)

    @StorageAcceptanceFromStorageScope
    @Provides
    fun provideActivity(): StorageAcceptanceFromStorageView = activity
}
