package com.cornel.pims.storage.core.proto

/**
 * Created by AlexGator on 02.04.18.
 */

interface HasComponent<out Component> {
    val component: Component
}