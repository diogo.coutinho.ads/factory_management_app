package com.cornel.pims.storage.acceptance.list

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.DocumentListUnit
import com.cornel.pims.storage.core.app.cast
import com.cornel.pims.storage.core.proto.HasComponent
import kotlinx.android.synthetic.main.common_item_list_item.view.*
import javax.inject.Inject

/**
 * Created by AlexGator on 02.04.18.
 */

interface StorageAcceptanceListFromStorageView {
    fun setList(data: List<DocumentListUnit>)
}

class StorageAcceptanceListFromStorageFragment : Fragment(), StorageAcceptanceListFromStorageView {

    @Inject
    lateinit var appContext: Context

    @Inject
    lateinit var presenter: StorageAcceptanceListPresenter

    private var adapter: DocumentRecyclerAdapter? = null
    private var recycler: RecyclerView? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        val componentActivity = cast<HasComponent<StorageAcceptanceListComponent>>(activity)
                ?: throw RuntimeException("Activity is not initialized or unsupported")
        componentActivity.component.inject(this)
    }

    override fun setList(data: List<DocumentListUnit>) {
        adapter = DocumentRecyclerAdapter(data, this::listener, appContext)
        recycler?.swapAdapter(adapter, false)
    }

    private fun listener(selected: DocumentListUnit) {
        presenter.onDocumentSelected(selected)
    }

    override fun onResume() {
        super.onResume()
        presenter.onFilterSelected(StorageAcceptanceListView.Filter.STORAGE)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        recycler = inflater
                .inflate(R.layout.common_recycler, container, false) as RecyclerView
        (recycler as RecyclerView).layoutManager = LinearLayoutManager(context)
        (recycler as RecyclerView).addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        val swipeRefresh = SwipeRefreshLayout(context)
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onDocumentRefresh()
        }
        swipeRefresh.addView(recycler)

        return swipeRefresh
    }

    class DocumentRecyclerAdapter(private val data: List<DocumentListUnit>,
                                  private val listener: (DocumentListUnit) -> Unit,
                                  private val context: Context)
        : RecyclerView.Adapter<DocumentRecyclerAdapter.ViewHolder>() {

        init {
            setHasStableIds(true)
        }

        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view = v
            val name: TextView = v.itemName
            val code: TextView = v.itemCode
            val count: TextView = v.itemCount

            fun bind(item: DocumentListUnit, listener: (DocumentListUnit) -> Unit) {
                val documentName = context.getString(
                        R.string.acceptance_from_storage_document,
                        item.id.substring(0 until 8))
//                val documentName = item.name
                val documentCode = item.code
                val documentQuantity = item.quantityWithAbbreviation
                name.text = documentName
                code.text = documentCode
                count.text = documentQuantity

                view.setOnClickListener { listener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_item_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}