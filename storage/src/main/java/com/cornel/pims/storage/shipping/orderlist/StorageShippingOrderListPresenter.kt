package com.cornel.pims.storage.shipping.orderlist

import android.content.Context
import com.cornel.pims.storage.common.OrderListUnit
import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by KILLdon on 25.02.18.
 */

interface StorageShippingOrderListPresenter : BasePresenter {
    fun onCreate()
    fun onRefresh()
    fun onItemSelected(id: String)
    fun onBackBtnClick()
}

class StorageShippingOrderListPresenterImpl(private val view: StorageShippingOrderListView)
    : BasePresenterImpl(view as BaseView), StorageShippingOrderListPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    override fun onCreate() {
        view.component.inject(this)
    }

    override fun doOnResumeAsync() {
        udapteDate()
    }

    override fun onRefresh() {
        smartLaunch {
            udapteDate()
        }
    }

    private fun udapteDate() {
        runBlocking {
            val orders = get(dataManager.getOrders()).map {
                OrderListUnit(it.orderId, it.orderNumber, it.partnerName)
            }
            launch(UI) { view.showItems(orders) }
        }
    }

    override fun onItemSelected(id: String) {
        view.navigateToShippingDocument(id)
    }

    override fun onBackBtnClick() {
        view.finish()
    }

}