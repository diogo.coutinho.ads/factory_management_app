package com.cornel.pims.storage.stocktaking.arealist

import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by KILLdon on 21.02.2018.
 */

interface StorageStocktakingAreaListPresenter : BasePresenter {
    fun onCreate()
    fun onRefresh()
    fun onItemSelected(item: AreaListUnit)
    fun onBackButton()
}

class StorageStocktakingAreaListPresenterImpl(val view: StorageStocktakingAreaListView)
    : BasePresenterImpl(view as BaseView), StorageStocktakingAreaListPresenter {

    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        view.component.inject(this)
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val storageId = dataManager.getCurrentStorageId()
            val data = get(dataManager.getAreasByStorageId(storageId)).map {
                AreaListUnit(it.id, it.name, it.storageItemsCount)
            }
            launch(UI) { view.showItems(data) }
        }
    }

    override fun onItemSelected(item: AreaListUnit) {
        info("Item selected: $item")
        view.navigateToProductView(item.id)
    }

    override fun onBackButton() {
        info("Back button pressed")
        view.finish()
    }

}