package com.cornel.pims.storage.stocktaking.document

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.storage.R
import com.cornel.pims.storage.core.*
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by KILLdon on 25.02.2018.
 */

interface StorageStocktakingDocumentPresenter : BasePresenter {
    fun onCreate(itemId: String)
    fun onStocktakingBtnClick()
    fun onBackButton()
}

class StorageStocktakingDocumentPresenterImpl(val view: StorageStocktakingDocumentView)
    : BasePresenterImpl(view as BaseView), StorageStocktakingDocumentPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    lateinit var item: StorageItem
    lateinit var user: User
    lateinit var product: Product

    lateinit var itemId: String

    var disposable: Disposable? = null

    override fun onCreate(itemId: String) {
        view.component.inject(this)
        this.itemId = itemId
    }

    override fun doOnResumeAsync() {
        launch {
            val itemDef = async { dataManager.getStorageItemById(itemId) }

            user = get(dataManager.getUser())
            item = get(itemDef.await())
            product = get(dataManager.getProductById(item.productId))

            launch(UI) {
                view.apply {
                    setName(product.name)
                    setSKU(product.code)
                    setSystemQuantity(Formatter.format(item.quantity))
                    setActualQuantity(Formatter.format(item.quantity))
                }
            }
        }
        subscribeToListChanges()
    }

    override fun doOnPause() {
        disposable?.dispose()
        dataManager.stopStorageItemSubscription()
    }



    override fun onStocktakingBtnClick() {
        val quantity = view.getQuantitySafe() ?: return
        val text = appContext.getString(
                R.string.stocktaking_document_confirm_text, Formatter.format(quantity))
        val btnText = appContext.getString(R.string.stocktaking_document_confirm_button)
        view.showConfirmation(text, btnText, this::submitDocument)
    }

    private fun submitDocument() {
        val date = SimpleDateFormat("dd-MM-yyyy", Locale("ru"))
                .format(Date())

        val quantity = view.getQuantitySafe() ?: return

        smartLaunch(finally = {
            super.finally()
            view.finish()
        }, onException = {
            view.isOk(false)
            super.onException(it)
        }) {
            runBlocking {
                val element = StocktakingElement(product.id, quantity)

                val doc = StocktakingDocument(
                        date,
                        user.id,
                        item.areaId,
                        listOf(element))
                info("Submitted document: $doc")
                dataManager.submitStocktakingDocument(doc)
                view.isOk(true)
            }
        }
    }

    override fun onBackButton() {
        info("Back button pressed")
        view.finish()
    }

    private suspend fun refreshStorageItem() {

                val itemDef = async { dataManager.getStorageItemById(itemId) }

                item = get(itemDef.await())
                product = get(dataManager.getProductById(item.productId))

                launch(UI) {
                    view.apply {
                        if (view.getQuantity() != item.quantity) {
                            setSystemQuantity(item.quantity.toString())
                            appContext.toast("Данные обновились")
                        }
                    }
                }

    }

    private fun subscribeToListChanges() {
        dataManager.subscribeOnChangedStorageItem{
           launch { refreshStorageItem() }
        }
    }
}
