package com.cornel.pims.storage.acceptance.fromstorage

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageAcceptanceFromStorageScope

@StorageAcceptanceFromStorageScope
@Subcomponent(modules = [StorageAcceptanceFromStorageModule::class])
interface StorageAcceptanceFromStorageComponent {
    fun inject(activity: StorageAcceptanceFromStorageActivity)
    fun inject(presenter: StorageAcceptanceFromStoragePresenterImpl)
}