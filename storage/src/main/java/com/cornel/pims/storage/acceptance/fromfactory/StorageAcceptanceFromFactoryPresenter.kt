package com.cornel.pims.storage.acceptance.fromfactory

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.common.HistoryUnit
import com.cornel.pims.storage.core.*
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 28.02.18.
 */

interface StorageAcceptanceFromFactoryPresenter : BasePresenter {
    fun onCreate(assignmentId: String)
    fun onRefresh()
    fun onBackBtnClick()
    fun onAcceptBtnClick()
}

class StorageAcceptanceFromFactoryPresenterImpl(private val view: StorageAcceptanceFromFactoryView)
    : BasePresenterImpl(view as BaseView), StorageAcceptanceFromFactoryPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var assignment: AcceptanceAssignment
    private lateinit var product: Product

    private lateinit var assignmentId: String
    private var quantity: Double = 0.0
    private lateinit var abbreviation: String

    override fun onCreate(assignmentId: String) {
        view.component.inject(this)
        this.assignmentId = assignmentId
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val assignmentNull = get(dataManager.getItemAwaitAcceptanceById(assignmentId))
            if (assignmentNull == null) {
                launch(UI) {
                    view.showError(RuntimeException("Данное производственное задание было удалено."))
                }
            } else {
                assignment = assignmentNull
                product = get(dataManager.getProductById(assignment.productId))
                quantity = assignment.quantity
                abbreviation = product.abbreviation

                val storage = get(dataManager.getCurrentStorage())
                val areas = get(dataManager.getAreasByStorageId(storage.id)).map {
                    // We don't need quantity in the spinner, so we don't waste time to receive it
                    AreaListUnit(it.id, it.name, it.storageItemsCount)
                }

                launch(UI) {
                    view.apply {
                        setTitle(appContext.getString(R.string.acceptance_from_factory_title, storage.name))
                        setItemName(product.name)

                        setCreationTimeDate(assignment.creationDate)
                        setProductCode(product.code)
                        setWorkplaceName(assignment.workplaceName)
                        showWorkshopHeads(assignment.workshopHeads)
                        setQuantity(quantity, abbreviation)
                        setOrderId(assignment.code)
                        setStorage(storage.name)
                        setAreaSpinner(areas)
                        val stateStr: String
                        val deleteStr: String
                        val deleted = assignment.deletedAt
                        if (deleted != null) {
                            stateStr = "Отменено"
                            deleteStr = deleted
                        } else {
                            val stateRes = stateTextMap[assignment.state]
                            stateStr = if (stateRes != null) appContext.getString(stateRes) else assignment.state
                            deleteStr = ""
                        }
                        setState(stateStr)
                        showOrHideDeletedAt(deleteStr)
                        showHistory(assignment.history.map { HistoryUnit(it.message, it.date) })
                        showOrHideAcceptFields(deleteStr.isEmpty() && assignment.state == AssignmentState.WAIT_ACCEPT.value)
                    }
                }
            }
        }
    }

    override fun onBackBtnClick() {
        info("Back button pressed")
        view.finish()
    }

    override fun onAcceptBtnClick() {
        val text = appContext.getString(
                R.string.acceptance_from_factory_confirm_text, product.name, Formatter.format(quantity), abbreviation)
        val buttonText = appContext.getString(R.string.acceptance_from_storage_confirm_button)
        view.showConfirmation(text, buttonText, this::submitDocument)
    }

    private fun submitDocument() {
        val date = Formats.dateFormat.format(Date())
        val areaId = view.getSelectedArea().id

        smartLaunch(onException = {
            view.isOk(false)
            super.onException(it)
        }, finally = {
            super.finally()
        }
        ) {
            runBlocking {
                val element = AcceptanceElement(
                        product.id, assignmentId, quantity
                )
                val doc = AcceptanceDocument(
                        date, areaId, null, listOf(element)
                )
                info("Submitted document: $doc")
                dataManager.submitAcceptanceDocument(doc)
                view.isOk(true)
                view.finish()
            }
        }
    }

    companion object {
        val stateTextMap = mapOf(
                AssignmentState.ARRIVED.value to R.string.acceptance_from_factory_state_arrived,
                AssignmentState.IN_PROGRESS.value to R.string.acceptance_from_factory_state_in_progress,
                AssignmentState.PAUSED.value to R.string.acceptance_from_factory_state_paused,
                AssignmentState.WAIT_REV.value to R.string.acceptance_from_factory_state_wait_revision,
                AssignmentState.REV_IN_PROGRESS.value to R.string.acceptance_from_factory_state_revision_in_progress,
                AssignmentState.WAIT_QC.value to R.string.acceptance_from_factory_state_wait_qc,
                AssignmentState.ACCEPT_STORAGE.value to R.string.acceptance_from_factory_state_accept_storage,
                AssignmentState.UNKNOWN.value to R.string.acceptance_from_factory_state_unknown
        )
    }
}

