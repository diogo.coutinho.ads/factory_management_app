package com.cornel.pims.storage.relocation.productlist

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageRelocationProductListScope

@StorageRelocationProductListScope
@Subcomponent(modules = [StorageRelocationProductListModule::class])
interface StorageRelocationProductListComponent {
    fun inject(activity: StorageRelocationProductListActivity)
    fun inject(activity: StorageRelocationProductListPresenterImpl)
}