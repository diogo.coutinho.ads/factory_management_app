package com.cornel.pims.storage.shipping.orderlist

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageShippingOrderListScope

@StorageShippingOrderListScope
@Subcomponent(modules = [StorageShippingOrderListModule::class])
interface StorageShippingOrderListComponent {
    fun inject(activity: StorageShippingOrderListActivity)
    fun inject(activity: StorageShippingOrderListPresenterImpl)
}