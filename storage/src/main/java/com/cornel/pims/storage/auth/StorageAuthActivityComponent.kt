package com.cornel.pims.storage.auth

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageAuthScope

@StorageAuthScope
@Subcomponent(modules = [StorageAuthModule::class])
interface StorageAuthComponent {
    fun inject(activity: StorageAuthActivity)
    fun inject(presenter: StorageAuthPresenterImpl)
}