package com.cornel.pims.storage.core.proto

import android.util.Log
import com.cornel.pims.core.api.TokenException
import com.cornel.pims.storage.core.QError
import com.cornel.pims.storage.core.QResponse
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

/**
 * Created by AlexGator on 19.03.18.
 */

interface BasePresenter {
    fun onResume()
    fun onPause()
}

abstract class BasePresenterImpl(private val view: BaseView) : BasePresenter {

    protected var tag: String? = null

    protected val jobs = mutableSetOf<Job>()

    fun info(text: String) {
        val logTag = tag ?: this::class.simpleName
        Log.i(logTag, text)
    }

    fun debug(text: String) {
        val logTag = tag ?: this::class.simpleName
        Log.d(logTag, text)
    }

    // Load data and show it in view
    protected abstract fun doOnResumeAsync()

    override fun onResume() {
        smartLaunch(action = this::doOnResumeAsync)
    }

    // Unsubscribe
    protected open fun doOnPause() {}

    override fun onPause() {
        debug("BasePresenter is paused. Stopping all pending requests")
        smartLaunch(action = this::doOnPause)
        jobs.forEach {
            if (it.isActive) {
                debug("Job $it is still active and will be cancelled")
                it.cancel()
            }
        }
        jobs.clear()
        view.hidePreloader()
    }

    fun smartLaunch(
            before: () -> Unit = this::before,
            onException: (e: Exception) -> Unit = this::onException,
            finally: () -> Unit = this::finally,
            action: () -> Unit
    ) {
        val job = launch {
            try {
                before()
                action()
            } catch (e: Exception) {
                onException(e)
            } finally {
                finally()
            }
        }
        debug("Launching new async request: $job")
        jobs.add(job)
    }

    protected fun before() {
        launch(UI) { view.showPreloader() }
    }

    protected fun onException(e: Exception) {
        launch(UI) { view.showError(e) }
    }

    protected fun finally() {
        launch(UI) { view.hidePreloader() }
    }

    protected fun <T> get(response: QResponse<T>): T {
        if (response.data == null) {
            when (response.error) {
                QError.SERVER -> throw RuntimeException("Ошибка сервера. Обратитесь к начальнику производства.")
                QError.AUTH -> throw RuntimeException("У Вас нет прав для работы с данным функционалом. Обратитесь к начальнику производства.")
                QError.TOKEN -> throw TokenException()
                null -> throw RuntimeException("Неизвестная ошибка сервера. Обратитесь к начальнику производства.")
            }
        } else {
            return response.data
        }
    }
}