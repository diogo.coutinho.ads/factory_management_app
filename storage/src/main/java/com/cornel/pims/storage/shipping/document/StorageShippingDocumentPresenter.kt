package com.cornel.pims.storage.shipping.document

import android.content.Context
import android.util.Log
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.common.ShippingAreaListUnit
import com.cornel.pims.storage.common.ShippingListUnit
import com.cornel.pims.storage.core.*
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.util.*
import javax.inject.Inject
import kotlin.math.min

/**
 * Created by KILLdon on 25.02.18.
 */

interface StorageShippingDocumentPresenter : BasePresenter {
    fun onCreate(orderId: String)
    fun onRefresh()
    fun onBackButtonClick()
    fun onShippingBtnClick()
}

class StorageShippingDocumentPresenterImpl(private val view: StorageShippingDocumentView)
    : BasePresenterImpl(view as BaseView), StorageShippingDocumentPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var orderId: String
    private lateinit var order: Order

    override fun onCreate(orderId: String) {
        view.component.inject(this)
        this.orderId = orderId
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {

            val storage = get(dataManager.getCurrentStorage())

            order = get(dataManager.getOrderById(orderId))
            var counter = 0
            val shipmentItems = order.elements.map {

                val shipment = get(dataManager.getAreasByProductIdAndStorageId(it.productId, storage.id)).map {
                    ShippingAreaListUnit(it.areaId, it.areaName, it.quantity, 0.0)
                }

                ShippingListUnit(counter++, it.productId, it.productName, it.productDate, it.quantity, shipment)
            }

            launch(UI) {
                view.setTitle(appContext.getString(R.string.shipping_document_title, order.orderNumber))
                view.showItems(shipmentItems)
            }
        }
    }

    override fun onBackButtonClick() {
        view.finish()
    }

    override fun onShippingBtnClick() {
        val text = appContext
                .getString(R.string.shipping_document_confirm_text)
        val btnText = appContext.getString(R.string.shipping_document_confirm_button)
        view.showConfirmation(text, btnText, this::submitDocument)
    }

    private fun submitDocument() {
        val items = view.getSelectedItems()
        if (items == null || items.isEmpty()) {
            launch(UI) {
                (view as BaseView)
                        .showMessage(appContext.getString(R.string.shipping_error_list_is_empty))
            }
            return
        }

        val elements = items.map {
            it.value.map {
                ShipmentElement(it.itemId, it.areaId, it.itemQuantity)
            }
        }.flatten()

        val date = Formats.dateFormat.format(Date())
        smartLaunch(
                onException = {
                    view.isOk(false)
                    super.onException(it)
                },
                finally = {
                    super.finally()
                    view.finish()
                }
        ) {
            runBlocking {
                val doc = ShipmentDocument(date, orderId, elements)
                info("Document sent: $doc")
                dataManager.submitShipmentDocument(doc)
                view.isOk(true)
            }
        }
    }
}