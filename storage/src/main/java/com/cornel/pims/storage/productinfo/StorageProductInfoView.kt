package com.cornel.pims.storage.productinfo

import android.os.Bundle
import com.cornel.pims.storage.R
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.relocation.document.StorageRelocationDocumentActivity
import com.cornel.pims.storage.stocktaking.document.StorageStocktakingDocumentActivity
import kotlinx.android.synthetic.main.product_info_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.noHistory
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by AlexGator on 14.02.18.
 */

interface StorageProductInfoView {
    val component: StorageProductInfoComponent
    fun setName(text: String)
    fun setCode(text: String)
    fun setQuantity(text: String)
    fun setStorage(text: String)
    fun setArea(text: String)
    fun setPartner(text: String)
    fun setOrderId(text: String)
    fun showError(e: Throwable)
    fun finish()
    fun navigateToTransferDocument(itemId: String)
    fun navigateToStocktakingDocument(itemId: String)
}

class StorageProductInfoActivity : BaseView(), StorageProductInfoView {

    companion object {
        const val EXTRA_ITEM_ID = "item_id"
    }

    @Inject
    override lateinit var presenter: StorageProductInfoPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageProductInfoModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_info_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        val itemId = intent.getStringExtra(EXTRA_ITEM_ID)

        presenter.onCreate(itemId)

        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onGoBackBtnClick()
        }
        relocateBtn.onClick {
            presenter.onRelocationBtnClick()
        }
        stocktakingBtn.onClick {
            presenter.onStocktakingBtnClick()
        }
    }


    override fun setName(text: String) {
        supportActionBar?.title = text
    }

    override fun setCode(text: String) = itemCode.setText(text)
    override fun setQuantity(text: String) = quantity.setText(text)
    override fun setStorage(text: String) = setOrHide(text, storage, storageContainer)
    override fun setArea(text: String) = setOrHide(text, area, areaContainer)
    override fun setPartner(text: String) = setOrHide(text, partner, partnerContainer)
    override fun setOrderId(text: String) = setOrHide(text, orderId, orderIdContainer)

    override fun navigateToTransferDocument(itemId: String) {
        startActivity(intentFor<StorageRelocationDocumentActivity>(
                StorageRelocationDocumentActivity.EXTRA_ITEM_ID to itemId
        ).noHistory())
    }

    override fun navigateToStocktakingDocument(itemId: String) {
        startActivity(intentFor<StorageStocktakingDocumentActivity>(
                StorageStocktakingDocumentActivity.EXTRA_ITEM_ID to itemId
        ).noHistory())
    }
}