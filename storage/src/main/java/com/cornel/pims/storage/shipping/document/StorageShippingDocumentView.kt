package com.cornel.pims.storage.shipping.document

import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.cornel.pims.core.Formatter
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.ShippingAreaProductListUnit
import com.cornel.pims.storage.common.ShippingListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.android.synthetic.main.common_shipping_area_item.view.*
import kotlinx.android.synthetic.main.common_shipping_document_item.view.*
import kotlinx.android.synthetic.main.shipping_document_activity.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

/**
 * Created by KILLdon on 25.02.18.
 */

interface StorageShippingDocumentView {
    val component: StorageShippingDocumentComponent

    fun setTitle(text: String)
    fun showItems(data: List<ShippingListUnit>)
    fun showError(e: Throwable)
    fun finish()
    fun getSelectedItems(): Map<Int, List<ShippingAreaProductListUnit>>?
    fun isOk(ok: Boolean): Any
    fun showConfirmation(text: String, actionBtnText: String, callback: () -> Unit)
}

class StorageShippingDocumentActivity : BaseView(), StorageShippingDocumentView {

    companion object {
        const val EXTRA_ORDER_ID = "item_id"
    }

    @Inject
    override lateinit var presenter: StorageShippingDocumentPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageShippingDocumentModule(this)) }

    private var adapter: RecyclerAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.shipping_document_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        val orderId = intent.getStringExtra(EXTRA_ORDER_ID)

        presenter.onCreate(orderId)

        addListeners()
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        adapter?.toggleCheckForAll()
//        return true
//    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.shipping_appbar_menu, menu)
//        return true
//    }

    override fun showItems(data: List<ShippingListUnit>) {
        adapter = RecyclerAdapter(data, this)
        recycler.swapAdapter(adapter, true)
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.setOnClickListener {
            presenter.onBackButtonClick()
        }
        goShipBtn.setOnClickListener {
            presenter.onShippingBtnClick()
        }
    }

    override fun setTitle(text: String) {
        toolbar.setTitle(text)
    }

    override fun getSelectedItems(): Map<Int, List<ShippingAreaProductListUnit>>? = adapter?.getSelectedItems()

    class RecyclerAdapter(val data: List<ShippingListUnit>,
                          private val appContext: Context)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        companion object {
            const val QUANTITY_MIN_VALUE = 0.0
        }

        fun getSelectedItems(): Map<Int, List<ShippingAreaProductListUnit>> {
            val result = mutableMapOf<Int, List<ShippingAreaProductListUnit>>()
            for (item in data) {
                val areaMap = item.areas.filter { it.itemChosenQuantity > 0 }
                if (!areaMap.isEmpty())
                    result.put(item.id, areaMap.map { ShippingAreaProductListUnit(it.id, item.productId, it.itemChosenQuantity) })
            }
            return result.toMap()
        }

        inner class ViewHolder(v: View,
                               private val appContext: Context)
            : RecyclerView.ViewHolder(v) {

            private val view = v
            private val name: TextView = view.itemName
            private val date: TextView = view.itemDate
            private val orderQuantity: TextView = view.orderQuantity
            val areaContainer: LinearLayout = view.areaContainer

            val cleaningMap = mutableMapOf<String, () -> Unit>()

            fun bind(item: ShippingListUnit) {

                name.text = item.productName
                date.text = item.productDate
                orderQuantity.text = appContext.getString(R.string.shipping_document_separator_args, Formatter.format(item.areas.map { it.itemChosenQuantity }.sum()), Formatter.format(item.orderQuantity))

                item.areas.map { it.itemChosenQuantity }.sum()

                if (item.areas.isEmpty()) {
                    view.areaQuantityInfo.setText(R.string.shipping_not_available)
                } else {
                    view.areaQuantityInfo.setText(R.string.shipping_quantity_and_available)

                    for (area in item.areas) {

                        val v = LayoutInflater.from(view.context)
                                .inflate(R.layout.common_shipping_area_item, areaContainer, false)

                        Log.i("Order", "$area in $v")

                        val areaName = v.areaName
                        val increment = v.incrementBtn
                        val decrement = v.decrementBtn
                        val maxQuantity = v.itemMaxQuantity
                        val itemQuantity = v.itemQuantity

                        areaName.text = area.name
                        maxQuantity.text = Formatter.format(area.itemQuantity)
                        itemQuantity.setText(Formatter.format(area.itemChosenQuantity))

                        val textWatcher = object : TextWatcher {

                            var skip = false
                            var result: Double = 0.0
                            val mItem = item
                            val mArea = area

                            override fun afterTextChanged(e: Editable) {
                                if (!skip) {
                                    skip = true
                                    val string = e.toString()
                                    e.clear()
                                    e.insert(0, string.replace(',','.'))
                                    skip = false

                                    if (e.toString().endsWith("."))
                                        return

                                    val value: Double = try {
                                        e.toString().toDouble()
                                    } catch (e: Throwable) {
                                        null
                                    } ?: QUANTITY_MIN_VALUE

                                    val areaMap = mItem.areas

                                    val maxValue = min(mItem.orderQuantity - (areaMap.map { it.itemChosenQuantity }.sum() - mArea.itemChosenQuantity), mArea.itemQuantity)
                                    val minValue = QUANTITY_MIN_VALUE

                                    result = max(minValue, min(value, maxValue))
                                    skip = true
                                    e.clear()
                                    e.insert(0, Formatter.format(result))
                                    skip = false

                                    mArea.itemChosenQuantity = result

                                    orderQuantity.text = appContext.getString(R.string.shipping_document_separator_args, Formatter.format(areaMap.map { it.itemChosenQuantity }.sum()), Formatter.format(mItem.orderQuantity))
                                }
                            }

                            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                        }

                        if (!cleaningMap.contains(area.id)) {
                            itemQuantity.addTextChangedListener(textWatcher)
                            cleaningMap[area.id] = { itemQuantity.removeTextChangedListener(textWatcher) }
                        }

                        increment.onClick {
                            try {
                                itemQuantity.setText((min(area.itemQuantity, area.itemChosenQuantity + 1)).toString())
                            } catch (e: Throwable) {
                            }
                        }

                        decrement.onClick {
                            try {
                                itemQuantity.setText((max(QUANTITY_MIN_VALUE, area.itemChosenQuantity - 1)).toString())
                            } catch (e: Throwable) {
                            }
                        }

                        areaContainer.addView(v)
                    }
                }
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_shipping_document_item, parent, false)
            return ViewHolder(v, appContext)
        }

        override fun onViewRecycled(holder: ViewHolder?) {
            for (cleanUp in holder!!.cleaningMap.values)
                cleanUp()

            holder.cleaningMap.clear()
            holder.areaContainer.removeAllViews()
        }

    }
}