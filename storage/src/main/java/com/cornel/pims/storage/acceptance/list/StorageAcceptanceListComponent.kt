package com.cornel.pims.storage.acceptance.list

import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by user on 24.02.18.
 */

@Scope
annotation class StorageAcceptanceListScope

@StorageAcceptanceListScope
@Subcomponent(modules = [
    StorageAcceptanceListModule::class,
    StorageAcceptanceListFromFactoryModule::class,
    StorageAcceptanceListFromStorageModule::class])
interface StorageAcceptanceListComponent {
    fun inject(activity: StorageAcceptanceListActivity)
    fun inject(presenter: StorageAcceptanceListPresenterImpl)
    fun inject(fragment: StorageAcceptanceListFromFactoryFragment)
    fun inject(fragment: StorageAcceptanceListFromStorageFragment)
}