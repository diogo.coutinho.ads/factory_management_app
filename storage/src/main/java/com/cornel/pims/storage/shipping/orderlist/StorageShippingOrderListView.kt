package com.cornel.pims.storage.shipping.orderlist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.OrderListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.shipping.document.StorageShippingDocumentActivity
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import kotlinx.android.synthetic.main.common_order_list_item.view.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

/**
 * Created by KILLdon on 25.02.18.
 */

interface StorageShippingOrderListView {
    val component: StorageShippingOrderListComponent

    fun showItems(data: List<OrderListUnit>)

    fun showError(e: Throwable)
    fun finish()
    fun navigateToShippingDocument(id: String)
}

class StorageShippingOrderListActivity : BaseView(), StorageShippingOrderListView {

    companion object {
        const val REQUEST_CODE_SHIPPING_DOC = 1
    }

    @Inject
    override lateinit var presenter: StorageShippingOrderListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageShippingOrderListModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        setSupportActionBar(toolbar)

        component.inject(this)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        presenter.onCreate()
        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.setOnClickListener {
            presenter.onBackBtnClick()
        }
    }

    fun onRecyclerItemClick(item: OrderListUnit) {
        presenter.onItemSelected(item.id)
    }

    override fun showItems(data: List<OrderListUnit>) {
        recycler.swapAdapter(RecyclerAdapter(data, this, this::onRecyclerItemClick), false)
    }

    override fun navigateToShippingDocument(id: String) {
        startActivityForResult(intentFor<StorageShippingDocumentActivity>(
                StorageShippingDocumentActivity.EXTRA_ORDER_ID to id
        ), REQUEST_CODE_SHIPPING_DOC)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_SHIPPING_DOC) {
            when (resultCode) {
                Activity.RESULT_OK -> showMessage(getString(R.string.shipping_order_list_success_accept))
                Activity.RESULT_CANCELED -> showMessage(getString(R.string.shipping_order_list_failed_accept))
            }
        }
    }

    class RecyclerAdapter(val data: List<OrderListUnit>,
                          val context: Context,
                          private val listener: (OrderListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            private var view = v
            private val itemId = v.itemCode
            private val orderPartner = v.orderPartner

            fun bind(item: OrderListUnit, listener: (OrderListUnit) -> Unit) {
                val orderName = context.getString(R.string.shipping_order_list_order,
                        item.number)
                itemId.text = orderName
                orderPartner.text = item.partner
                view.setOnClickListener { listener(item) }
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_order_list_item, parent, false)
            return ViewHolder(v)
        }

    }
}