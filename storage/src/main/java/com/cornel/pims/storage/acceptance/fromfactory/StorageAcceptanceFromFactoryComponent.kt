package com.cornel.pims.storage.acceptance.fromfactory

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageAcceptanceFromFactoryScope

@StorageAcceptanceFromFactoryScope
@Subcomponent(modules = [StorageAcceptanceFromFactoryModule::class])
interface StorageAcceptanceFromFactoryComponent {
    fun inject(activity: StorageAcceptanceFromFactoryActivity)
    fun inject(presenter: StorageAcceptanceFromFactoryPresenterImpl)
}