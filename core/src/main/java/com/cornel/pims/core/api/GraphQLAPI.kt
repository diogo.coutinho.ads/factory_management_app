package com.cornel.pims.core.api

import android.util.Log
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.subscription.OperationClientMessage
import com.apollographql.apollo.subscription.SubscriptionTransport
import com.apollographql.apollo.subscription.WebSocketSubscriptionTransport
import com.cornel.pims.core.OAuthJsonResponse
import com.google.gson.Gson
import kotlinx.serialization.json.JSON
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import java.io.IOException
import java.net.ConnectException

class AuthenticationException(message: String = "") : IOException(message)

class TokenException(message: String = "") : IOException(message)

class GraphQLAPI(
        private val apiURL: HttpUrl,
        private val authURL: HttpUrl,
        private val wsURL: String,
        private var email: String,
        private var password: String) {

    //TODO decide if we sew client_id into app or make it settable
    companion object {
        val LOG_TAG = "GQL_API"
        val GRANT_TYPE = "password"
        val CLIENT_ID = "55oo55o555o55od555o5oo5o555d5oo5o55ooo555o5o5555o5555555oooo5o5"
        val gson = Gson()
    }

    val client: ApolloClient by lazy { buildClient() }
    val client2: ApolloClient by lazy { buildClient2() }

    private fun buildClient(): ApolloClient {
        val token = getToken().access_token
        Log.i(LOG_TAG, "Got a new token: ${token.substring(0..10)}...")

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        val httpClient = OkHttpClient()
                .newBuilder()
//                .addInterceptor(logging)
                .addNetworkInterceptor(HeadersInterceptor(token))
                .build()

        return ApolloClient.builder()
                .serverUrl(apiURL)
                .okHttpClient(httpClient)
                .subscriptionTransportFactory(WebSocketSubscriptionTransport.Factory(wsURL, httpClient))
                .build()
    }

    private fun buildClient2(): ApolloClient {
        val token = getToken().access_token
        Log.i(LOG_TAG, token)
        Log.i(LOG_TAG, "Got a new token: ${token.substring(0..10)}...")

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        val httpClient = OkHttpClient()
                .newBuilder()
////                .addInterceptor(logging)
//                .addNetworkInterceptor(HeadersInterceptor(token))
                .build()

        return ApolloClient.builder()
                .serverUrl(apiURL)
                .okHttpClient(httpClient)
                .subscriptionTransportFactory(WebSocketSubscriptionTransport.Factory(wsURL, httpClient))
                .build()
    }

    private fun getToken(): OAuthJsonResponse {
        val client = OkHttpClient()

        val body = RequestBody
                .create(MediaType.parse("text"),
                        "grant_type=$GRANT_TYPE&email=$email" +
                                "&password=$password&client_id=$CLIENT_ID")

        val request = Request.Builder()
                .url(authURL)
                .post(body)
                .build()

        val response = try {
            client
                    .newCall(request)
                    .execute()
        } catch (e: Exception) {
            Log.e(LOG_TAG, "", e)
            throw ConnectException("Couldn't connect to the server $authURL")
        } ?: throw RuntimeException("Response is null")

        if (!response.isSuccessful) { // Connected, but get errors
            when (response.code()) {
                401 -> throw AuthenticationException(response.toString())
                else -> throw ConnectException("Unsuccessful response: $response")
            }
        }

        val responseBody = response.body() ?: throw ConnectException("Empty response body")
        val responseText = String(responseBody.bytes())
        return gson.fromJson(responseText, OAuthJsonResponse::class.java)
    }

    //TODO Handle exception when the token is invalid
    class HeadersInterceptor(private val token: String) : okhttp3.Interceptor {
        override fun intercept(chain: Interceptor.Chain?): okhttp3.Response {
            if (chain == null) throw IllegalArgumentException("Interceptor: chain is null")
            val request = chain.request()
                    .newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Authorization", "Bearer $token")
                    .build()
            return chain.proceed(request)
        }
    }
}
