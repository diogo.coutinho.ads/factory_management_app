package com.cornel.pims.core.helper

import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Mutation
import com.apollographql.apollo.api.Operation
import com.apollographql.apollo.api.Query
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.exception.ApolloNetworkException
import com.cornel.pims.core.api.GraphQLAPI
import com.cornel.pims.core.api.GraphQLAPI.Companion.LOG_TAG
import kotlinx.coroutines.experimental.CompletableDeferred
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.delay
import okhttp3.HttpUrl
import java.net.ConnectException


abstract class BaseApiHelper {
    companion object {
        const val RETRY_COUNT = 5
        const val RETRY_DELAY = 3000
    }

    //TODO receive from DM
    lateinit var email: String
    lateinit var password: String

    private val authURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1337)
            .addPathSegment("oauth")
            .addPathSegment("token")
            .build()!!

    private val apiURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1337)
            .addPathSegment("graphql")
            .build()!!

    private val wsURL = "ws://11.11.11.11:1338/cable"

    private lateinit var graphQLAPI: GraphQLAPI

    /**
     * Special function to rebuild private [GraphQLAPI] class.
     */
    private fun rebuildClient() {
        var i = 0
        // Retrying only on ConnectException
        while (i < RETRY_COUNT) {
            try {
                graphQLAPI = GraphQLAPI(apiURL, authURL, wsURL, email, password)
                Log.d(LOG_TAG, "Client rebuild")
                return
            } catch (e: ConnectException) {
                Log.e(LOG_TAG, "API client build failed", e)
                if (++i == RETRY_COUNT) throw e
            }
        }
    }

    protected fun error(message: String, e: Throwable? = null) {
        Log.e(this::class.simpleName, message, e)
    }

    protected fun info(message: String) {
        Log.i(this::class.simpleName, message)
    }

    /* Authentication */
    public fun connect(email: String, password: String) {
        this.email = email
        this.password = password
        rebuildClient()
    }

    /**
     * Generic for Apollo Query
     *
     * @param T The query itself
     * @param D simply provide here T.Data
     * @param R Deferred return type
     * */
    protected fun<T, D, V, R> queryProto(
            query: T,
            onSuccess: (resp: Response<D>, defer: CompletableDeferred<R>) -> Unit,
            onError: (e: ApolloException) -> Unit = {}): Deferred<R>
            where T : Query<D, D, V>, D : Operation.Data, V : Operation.Variables {
        val defer = CompletableDeferred<R>()

        graphQLAPI.client.query(query).enqueue(object : ApolloCall.Callback<D>() {
            override fun onResponse(response: Response<D>) {
                info("${query::class.simpleName} success.")
                try {
                    onSuccess(response, defer)
                } catch (e: Throwable) {
                    defer.cancel(e)
                    error("Response received successfully, but onSuccess call failed: ", e)
                }
            }

            override fun onFailure(e: ApolloException) {
                error("${query::class.simpleName} failed: ", e)
                defer.cancel(e)
                onError(e)
            }
        })
        return defer
    }
    /**
     * Generic for Apollo Mutation
     *
     * @param T The query itself
     * @param D simply provide here T.Data
     * @param R Deferred return type
     * */

    protected fun<T, D, V, R> mutationProto(
            mutation: T,
            onSuccess: (resp: Response<D>, defer: CompletableDeferred<R>) -> Unit,
            onError: (e: ApolloException) -> Unit = {}): Deferred<R>
            where T : Mutation<D, D, V>, D: Operation.Data, V: Operation.Variables {
        val defer = CompletableDeferred<R>()

        graphQLAPI.client.mutate(mutation).enqueue(object : ApolloCall.Callback<D>() {
            override fun onResponse(response: Response<D>) {
                info("${mutation::class.simpleName} success.")
                try {
                    onSuccess(response, defer)
                } catch (e: Throwable) {
                    defer.cancel(e)
                    error("Response received successfully, but onSuccess call failed: ", e)
                }
            }

            override fun onFailure(e: ApolloException) {
                error("${mutation::class.simpleName} failed: ", e)
                defer.cancel(e)
                onError(e)
            }
        })
        return defer
    }


    /**
     * Special generic function which makes [RETRY_COUNT] retries
     * if error takes place while connecting.
     */
    protected suspend fun <T> proceedOperation(requestFun: () -> Deferred<T>): T {
        var exception = Throwable()
        for (i in 0..RETRY_COUNT) {
            try {
                if (!this::graphQLAPI.isInitialized) {
                    rebuildClient()
                }
                return requestFun().await()
            } catch (e: ConnectException) {
                exception = e
                Log.e(LOG_TAG, "Request to Auth server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            } catch (e: ApolloNetworkException) {
                // Handles every exception during API server call, and invalid token too
                exception = e
                Log.e(LOG_TAG, "Request to API server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            }
        }

        throw exception
    }

}
