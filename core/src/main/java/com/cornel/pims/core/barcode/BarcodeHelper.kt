package com.cornel.pims.core.barcode

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import com.cornel.pims.core.Barcode
import com.cornel.pims.core.BarcodeRaw
import com.cornel.pims.core.BarcodeType
import com.cornel.pims.core.R
import com.google.gson.Gson

/**
 * Handles hardware barcode scan button.
 * */

class BarcodeException(message: String): RuntimeException(message)

interface BarcodeSubscriber {
    fun onBarcodeScan(barcode: Barcode)
    fun onBarcodeError(e: Throwable)
}

interface BarcodeHelper {
    fun subscribe(subscriber: BarcodeSubscriber)
    fun unsubscribe(subscriber: BarcodeSubscriber): Boolean
    fun register(context: Context)
    fun unregister(context: Context)
}

object BarcodeHelperImpl: BroadcastReceiver(), BarcodeHelper {
    private const val SCAN_ACTION = "urovo.rcv.message"
    private const val BARCODE_EXTRA = "barocode" // SDK developer's mistake
    private const val LENGTH_EXTRA = "length"

    private val subscribers = mutableListOf<BarcodeSubscriber>()

    private var isRegistered = false

    private val gson = Gson()

    fun isRegistered() = isRegistered

    override fun onReceive(context: Context, intent: Intent) {
        try {
            val bytecode = intent.getByteArrayExtra(BARCODE_EXTRA)
            val length = intent.getIntExtra(LENGTH_EXTRA, 0)

            // Decoding from bytes to string
            val decoded = String(bytecode, 0, length)

            val barcodeRaw = try {
                gson.fromJson(decoded, BarcodeRaw::class.java)
            } catch (e: Throwable) {
                Log.e(this::class.toString(), "Error:", e)
                throw BarcodeException(
                        context.getString(R.string.error_barcode_unknown_format)
                )
            }

            val type = BarcodeType.values().associateBy { it.value }[barcodeRaw.t]
                    ?: throw BarcodeException(
                            context.getString(R.string.error_barcode_unknown_type, barcodeRaw.t))

            val barcode = Barcode(barcodeRaw.v, type, barcodeRaw.id)

            subscribers.forEach { it.onBarcodeScan(barcode) }
        } catch (e: Throwable) {
            subscribers.forEach { it.onBarcodeError(e) }
        }
    }

    override fun subscribe(subscriber: BarcodeSubscriber) {
        subscribers.add(subscriber)
    }

    override fun unsubscribe(subscriber: BarcodeSubscriber): Boolean {
        return subscribers.remove(subscriber)
    }

    override fun register(context: Context) {
        if (!isRegistered) {
            context.registerReceiver(this, IntentFilter(SCAN_ACTION))
            isRegistered = true
        }
    }

    override fun unregister(context: Context) {
        if (isRegistered) {
            context.unregisterReceiver(this)
            isRegistered = false
        }
    }
}
