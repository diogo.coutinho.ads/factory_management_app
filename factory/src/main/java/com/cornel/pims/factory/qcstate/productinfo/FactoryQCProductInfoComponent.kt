package com.cornel.pims.factory.qcstate.productinfo

import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by AlexGator on 13.03.18.
 */
@Scope annotation class FactoryQCProductInfoScope

@FactoryQCProductInfoScope
@Subcomponent(modules = [FactoryQCProductInfoModule::class])
interface FactoryQCProductInfoComponent {
    fun inject(activity: FactoryQCProductInfoActivity)
    fun inject(presenter: FactoryQCProductInfoPresenterImpl)
}