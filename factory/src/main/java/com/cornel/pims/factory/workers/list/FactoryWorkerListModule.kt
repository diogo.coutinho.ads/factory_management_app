package com.cornel.pims.factory.workers.list

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 08.03.18.
 */

@Module
class FactoryWorkerListModule(
        private val activity: FactoryWorkerListActivity) {
    @FactoryWorkerListScope
    @Provides
    fun providePresenter(): FactoryWorkerListPresenter =
            FactoryWorkerListPresenterImpl(activity)
}
