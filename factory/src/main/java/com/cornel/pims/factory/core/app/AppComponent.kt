package com.cornel.pims.factory.core.app

import android.app.Application
import com.cornel.pims.factory.auth.FactoryAuthComponent
import com.cornel.pims.factory.auth.FactoryAuthModule
import com.cornel.pims.factory.core.DataManagerImpl
import com.cornel.pims.factory.core.helper.APIHelperImpl
import com.cornel.pims.factory.core.helper.SharedPrefHelperImpl
import com.cornel.pims.factory.information.InformationComponent
import com.cornel.pims.factory.information.InformationModule
import com.cornel.pims.factory.main.FactoryMainActivityComponent
import com.cornel.pims.factory.main.FactoryMainActivityModule
import com.cornel.pims.factory.qcstate.list.FactoryQCStatusComponent
import com.cornel.pims.factory.qcstate.list.FactoryQCStatusModule
import com.cornel.pims.factory.qcstate.productinfo.FactoryQCProductInfoComponent
import com.cornel.pims.factory.qcstate.productinfo.FactoryQCProductInfoModule
import com.cornel.pims.factory.status.productioninfo.FactoryProductInfoComponent
import com.cornel.pims.factory.status.productioninfo.FactoryProductInfoModule
import com.cornel.pims.factory.status.productionlist.FactoryProductionListComponent
import com.cornel.pims.factory.status.productionlist.FactoryProductionListModule
import com.cornel.pims.factory.status.workplacelist.FactoryProdWorkplaceListComponent
import com.cornel.pims.factory.status.workplacelist.FactoryProdWorkplaceListModule
import com.cornel.pims.factory.workers.add.FactoryWorkerAddComponent
import com.cornel.pims.factory.workers.add.FactoryWorkerAddModule
import com.cornel.pims.factory.workers.info.FactoryWorkerInfoComponent
import com.cornel.pims.factory.workers.info.FactoryWorkerInfoModule
import com.cornel.pims.factory.workers.list.FactoryWorkerListComponent
import com.cornel.pims.factory.workers.list.FactoryWorkerListModule
import com.cornel.pims.factory.workers.workplace.FactoryWorkersWorkplaceListComponent
import com.cornel.pims.factory.workers.workplace.FactoryWorkersWorkplaceListModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by AlexGator on 03.02.18.
 */

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: Application)
    fun inject(dataManager: DataManagerImpl)
    fun inject(sharedPrefHelper: SharedPrefHelperImpl)
    fun inject(apiHelper: APIHelperImpl)

    fun plus(module: InformationModule): InformationComponent
    fun plus(module: FactoryAuthModule): FactoryAuthComponent
    fun plus(module: FactoryMainActivityModule): FactoryMainActivityComponent
    fun plus(module: FactoryWorkersWorkplaceListModule): FactoryWorkersWorkplaceListComponent
    fun plus(module: FactoryWorkerListModule): FactoryWorkerListComponent
    fun plus(module: FactoryWorkerAddModule): FactoryWorkerAddComponent
    fun plus(module: FactoryWorkerInfoModule): FactoryWorkerInfoComponent
    fun plus(module: FactoryQCStatusModule): FactoryQCStatusComponent
    fun plus(module: FactoryProdWorkplaceListModule): FactoryProdWorkplaceListComponent
    fun plus(module: FactoryProductionListModule): FactoryProductionListComponent
    fun plus(module: FactoryProductInfoModule): FactoryProductInfoComponent
    fun plus(module: FactoryQCProductInfoModule): FactoryQCProductInfoComponent
}
