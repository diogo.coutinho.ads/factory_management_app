package com.cornel.pims.factory.core

import java.text.SimpleDateFormat
import java.util.*



object Formats {
    val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale("ru"))
}

enum class QError {
        AUTH,
        TOKEN,
        SERVER
}

data class QResponse <T> (
        val data: T?,
        val error: QError?
)

data class WorkersCounts (
        val max: Long,
        val current: Long
)

data class User(
        val id: String,
        val name: String
)

data class Product(
        val id: String,
        val name: String,
        val code: String
)

data class Workshop(val id: String,
                    val code: String,
                    val name: String)

data class Workplace(val id: String,
                     val code: String,
                     val name: String,
                     val producedQuantity: Int,
                     val currentJobsQuantity: Int,
                     val maxJobsQuantity: Int,
                     val assignedWorkersQuantity: Int,
                     val assignedWorkersIds: List<String>)

data class Worker(val id: String,
                  val fullName: String,
                  val lastName: String,
                  val patronymicName: String,
                  val assignedWorkplacesIds: List<String>)

enum class AssignmentState(val value: String) {
        ARRIVED("arrived_at_stage"),
        IN_PROGRESS("work_in_progress"),
        PAUSED("paused_work"),
        WAIT_QC("waiting_for_technical_control"),
        QC_IN_PROGRESS("technical_control_in_progress"),
        REJECTED("rejected"),
        WAIT_REV("waiting_for_revision"),
        REV_IN_PROGRESS("revision_in_progress"),
        WAIT_ACCEPT("waiting_for_acceptance"),
        ACCEPT_STORAGE("accepted_to_storage"),
        UNKNOWN("")
}

data class HistoryUnit (
        val message: String,
        val date: String
)

class ProductionAssignment(
        val id: String,
        val code: String,
        val defaultWorkplaceName: String,
        val availableWorkplaces: List<ShortWorkplace>,
        val productId: String,
        val productSku: String,
        val quantity: Double,
        val abbreviation: String,
        val history: List<HistoryUnit>,
        val currentOperation: FactoryOperation?,
        val stateStr: String,
        val deletedAt: String?,
        val resources: Map<String, Double>? = null,
        val sideProducts: Map<String, Double>? = null
) {
        companion object {
                private val stateMap = AssignmentState.values().associateBy { it.value }
        }

        var state: AssignmentState? = null

        init {
                this.state = stateMap[stateStr] ?: AssignmentState.UNKNOWN
        }

        data class ShortWorkplace(
                val id: String,
                val name: String
        )
}

data class FactoryOperation(
        val id: String,
        val name: String,
        val stageNumber: Int,
        val productionAssignments: List<ProductionAssignment>)

data class ProductionUnit(val id: String,
                          val name: String,
                          val agent: String,
                          val shippingDate: String,
                          val manager: String,
                          val extraInfo: String,
                          val qcStatus: String,
                          val qcSpecialist: String,
                          val workplace: Workplace,
                          val declineReason: String)

class QCProductionAssignment(
        val id: String,
        val code: String,
        val quantity: Double,
        val productId: String,
        stateStr: String,
        val manager: String,
        val additionalInfo: String,
        val comment: String,
        val abbreviation: String,
        val history: List<HistoryUnit>
) {
        companion object {
                private val stateMap = AssignmentState.values().associateBy { it.value }
        }

        var state: AssignmentState? = null

        init {
                this.state = stateMap[stateStr] ?: AssignmentState.UNKNOWN
        }
}
