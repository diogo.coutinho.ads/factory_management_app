package com.cornel.pims.factory.auth

import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject



interface FactoryAuthPresenter : BasePresenter {
    fun onCreate()
    fun onLogin(userLogin: String, userPass: String)
}

class FactoryAuthPresenterImpl(private val view: FactoryAuthView)
    : BasePresenterImpl(view as BaseView), FactoryAuthPresenter {
    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        view.component.inject(this)
        smartLaunch {
            val login = dataManager.getLogin()
            launch(UI) {
                view.setLogin(login)
            }
        }
    }

    override fun doOnResumeAsync() {}

    override fun onLogin(userLogin: String, userPass: String) {
        smartLaunch {
            runBlocking {
                dataManager.setLogin(userLogin)
                dataManager.connect(userLogin, userPass)
                dataManager.getUser()
                launch(UI) {
                    val check = dataManager.getAppPermission()
                    launch(UI) {
                        if (check) {
                            view.navigateToMainActivity()
                        } else {
                            view.showError(RuntimeException("Вы не имеете права работать в данном приложении. Обратитесь к начальнику производства"), false)
                        }
                    }
                }
            }
        }
    }

}
