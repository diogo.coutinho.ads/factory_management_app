package com.cornel.pims.factory.status.productioninfo

import android.content.Context
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.HistoryUnit
import com.cornel.pims.factory.common.ItemListUnit
import com.cornel.pims.factory.common.WorkplaceUnit
import com.cornel.pims.factory.core.AssignmentState
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.ProductionAssignment
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */
interface FactoryProductInfoPresenter : BasePresenter {
    fun onCreate(id: String)
    fun onRefresh()
    fun onBackButtonPressed()
    fun onActionBtnClick()
    fun onCancelBtnClick()
    fun onPauseBtnClick()
}

class FactoryProductInfoPresenterImpl(
        private val view: FactoryProductInfoView)
    : BasePresenterImpl(view as BaseView), FactoryProductInfoPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var itemId: String
    private lateinit var assignment: ProductionAssignment
    private var state: AssignmentState? = null

    override fun onCreate(id: String) {
        view.component.inject(this)
        itemId = id
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val assignmentNull = get(dataManager.getProductionAssignmentById(itemId))
            if (assignmentNull == null) {
                launch(UI) {
                    view.showError(RuntimeException("Данное производственное задание было удалено."))
                }
            } else {
                assignment = assignmentNull
                state = assignment.state
                val resultProduct = get(dataManager.getProductById(assignment.productId))!!

                val stateText: String
                val deletedAtText: String

                val deletedAt = assignment.deletedAt
                if (deletedAt != null) {
                    stateText = "Отменено"
                    deletedAtText = deletedAt
                } else {
                    val stateTextResId = stateStrTextMap[assignment.stateStr]
                    stateText = if (stateTextResId != null) {
                        appContext.getString(stateTextResId)
                    } else assignment.stateStr
                    deletedAtText = ""
                }

                val actionBtnResId = actionButtonTextMap[assignment.state]
                val actionBtnText = if (actionBtnResId != null) {
                    appContext.getString(actionBtnResId)
                } else ""
                val resources = assignment.resources?.map {
                    val product = get(dataManager.getProductById(it.key))!!
                    ItemListUnit(product.id, product.name, product.code, it.value)
                } ?: emptyList()

                val sideProducts = assignment.sideProducts?.map {
                    val product = get(dataManager.getProductById(it.key))!!
                    ItemListUnit(product.id, product.name, product.code, it.value)
                } ?: emptyList()

                launch(UI) {
                    view.apply {
                        setProductName(resultProduct.name)
                        setProductCode(resultProduct.code)
                        if (deletedAtText.isEmpty()) {
                            if (state == AssignmentState.ARRIVED)
                                showWorkplaces(assignment.availableWorkplaces.map {
                                    WorkplaceUnit(
                                            it.id,
                                            it.name
                                    )
                                })
                            else
                                setWorkplace(assignment.defaultWorkplaceName)
                        } else {
                            hideWorkplace()
                        }
                        setOperationType(assignment.currentOperation?.name!!)
                        setAssignmentCode(assignment.code)
                        setMaxQuantity(assignment.quantity)
                        setQuantity(assignment.quantity)
                        setState(stateText)
                        showOrHideDeleteDate(deletedAtText)
                        setActionBtnText(if (deletedAtText.isEmpty()) actionBtnText else "")
                        showOrHidePauseBtn(deletedAtText.isEmpty() && (state == AssignmentState.IN_PROGRESS || state == AssignmentState.REV_IN_PROGRESS))
                        showOrHideRejectBtn(deletedAtText.isEmpty() && state != AssignmentState.WAIT_ACCEPT)
                        showResources(resources)
                        showSideProducts(sideProducts)
                        showHistory(assignment.history.map {
                            HistoryUnit(it.message, it.date)
                        })
                    }
                }
            }
        }
    }

    override fun onActionBtnClick() {
        when (state) {
            AssignmentState.ARRIVED -> startProductionAssignment()
            AssignmentState.IN_PROGRESS -> finishProductionAssignment()
            AssignmentState.PAUSED -> resumeProductionAssignment()
            AssignmentState.WAIT_REV -> startRevision()
            AssignmentState.REV_IN_PROGRESS -> finishRevision()
            else -> {}
        }
    }

    override fun onCancelBtnClick() {
        cancelProductionAssignment()
    }

    override fun onPauseBtnClick() {
        suspendProductionAssignment()
    }

    private fun suspendProductionAssignment() {
        val quantity = view.getQuantity()?: return
        val confirmText = appContext
                .getString(R.string.factory_status_production_info_confirm_suspend)
        val confirmBtnText = appContext
                .getString(R.string.factory_status_production_info_confirm_suspend_btn)
        (view as BaseView).showConfirmation(confirmText, confirmBtnText) {
            smartLaunch(finally = {
                super.finally()
                view.finish()
            }) {
                runBlocking {
                    // TODO set up splitting_quantity usage
                    dataManager.suspendProductionAssignment(itemId, quantity)
                }
            }
        }
    }

    private fun startProductionAssignment() {
        val quantity = view.getQuantity()?: return
        runBlocking {
            val workplace = get(dataManager.getWorkplaceById(view.getSelectedWorkplace().id))!!
            if (workplace.maxJobsQuantity == workplace.currentJobsQuantity) {
                (view as BaseView).showMessage(appContext.getString(R.string.factory_status_production_info_unable_to_start))
            } else {
                val confirmText = appContext
                        .getString(R.string.factory_status_production_info_confirm_start)
                val confirmBtnText = appContext
                        .getString(R.string.factory_status_production_info_confirm_start_btn)
                (view as BaseView).showConfirmation(confirmText, confirmBtnText) {
                    smartLaunch(finally = {
                        super.finally()
                        view.finish()
                    }) {
                        runBlocking {
                            // TODO set up splitting_quantity usage
                            dataManager.startProductionAssignment(itemId, workplace.id, quantity)
                        }
                    }
                }
            }
        }
    }

    private fun finishProductionAssignment() {
        val quantity = view.getQuantity()?: return
        val confirmText = appContext
                .getString(R.string.factory_status_production_info_confirm_finish)
        val confirmBtnText = appContext
                .getString(R.string.factory_status_production_info_confirm_finish_btn)
        (view as BaseView).showConfirmation(confirmText, confirmBtnText) {
            smartLaunch(finally = {
                super.finally()
                view.finish()
            }) {
                runBlocking {
                    // TODO set up splitting_quantity usage
                    dataManager.finishProductionAssignment(itemId, quantity)
                }
            }
        }
    }

    private fun resumeProductionAssignment() {
        val quantity = view.getQuantity()?: return
        val confirmText = appContext
                .getString(R.string.factory_status_production_info_confirm_resume)
        val confirmBtnText = appContext
                .getString(R.string.factory_status_production_info_confirm_resume_btn)
        (view as BaseView).showConfirmation(confirmText, confirmBtnText) {
            smartLaunch(finally = {
                super.finally()
                view.finish()
            }) {
                runBlocking {
                    // TODO set up splitting_quantity usage
                    dataManager.resumeProductionAssignment(itemId, quantity)
                }
            }
        }
    }

    private fun startRevision() {
        val quantity = view.getQuantity()?: return
        val confirmText = appContext
                .getString(R.string.factory_status_production_info_confirm_start_revision)
        val confirmBtnText = appContext
                .getString(R.string.factory_status_production_info_confirm_start_revision_btn)
        (view as BaseView).showConfirmation(confirmText, confirmBtnText) {
            smartLaunch(finally = {
                super.finally()
                view.finish()
            }) {
                runBlocking {
                    // TODO set up splitting_quantity usage
                    dataManager.startRevision(itemId, quantity)
                }
            }
        }
    }

    private fun finishRevision() {
        val quantity = view.getQuantity()?: return
        val confirmText = appContext
                .getString(R.string.factory_status_production_info_confirm_finish_revision)
        val confirmBtnText = appContext
                .getString(R.string.factory_status_production_info_confirm_finish_revision_btn)
        (view as BaseView).showConfirmation(confirmText, confirmBtnText) {
            smartLaunch(finally = {
                super.finally()
                view.finish()
            }) {
                runBlocking {
                    // TODO set up splitting_quantity usage
                    dataManager.finishRevision(itemId, quantity)
                }
            }
        }
    }

    private fun cancelProductionAssignment() {
        val quantity = view.getQuantity()?: return
        view.showCancelDialog { resourceAction, cause ->
            smartLaunch(finally = {
                super.finally()
                view.finish()
            }) {
                runBlocking {
                    // TODO set up splitting_quantity usage
                    dataManager.cancelProductionAssignment(itemId, quantity)
                }
            }
        }
    }

    override fun onBackButtonPressed() = view.finish()

    companion object {
        val stateStrTextMap = mapOf (
                AssignmentState.ARRIVED.value to R.string.factory_status_production_info_state_arrived,
                AssignmentState.IN_PROGRESS.value to R.string.factory_status_production_info_state_in_progress,
                AssignmentState.PAUSED.value to R.string.factory_status_production_info_state_paused,
                AssignmentState.WAIT_REV.value to R.string.factory_status_production_info_state_wait_revision,
                AssignmentState.REV_IN_PROGRESS.value to R.string.factory_status_production_info_state_revision_in_progress,
                AssignmentState.WAIT_QC.value to R.string.factory_status_production_info_state_wait_qc,
                AssignmentState.WAIT_ACCEPT.value to R.string.factory_status_production_info_wait_accept,
                AssignmentState.ACCEPT_STORAGE.value to R.string.factory_status_production_info_accept_storage,
                AssignmentState.UNKNOWN.value to R.string.factory_status_production_info_state_unknown
        )

        val stateTextMap = mapOf (
                AssignmentState.ARRIVED to R.string.factory_status_production_info_state_arrived,
                AssignmentState.IN_PROGRESS to R.string.factory_status_production_info_state_in_progress,
                AssignmentState.PAUSED to R.string.factory_status_production_info_state_paused,
                AssignmentState.WAIT_REV to R.string.factory_status_production_info_state_wait_revision,
                AssignmentState.REV_IN_PROGRESS to R.string.factory_status_production_info_state_revision_in_progress,
                AssignmentState.WAIT_QC to R.string.factory_status_production_info_state_wait_qc,
                AssignmentState.WAIT_ACCEPT to R.string.factory_status_production_info_wait_accept,
                AssignmentState.ACCEPT_STORAGE to R.string.factory_status_production_info_accept_storage,
                AssignmentState.UNKNOWN to R.string.factory_status_production_info_state_unknown
        )

        val actionButtonTextMap = mapOf(
                AssignmentState.ARRIVED to R.string.factory_status_production_info_btn_start,
                AssignmentState.IN_PROGRESS to R.string.factory_status_production_info_btn_stop,
                AssignmentState.PAUSED to R.string.factory_status_production_info_btn_start,
                AssignmentState.WAIT_REV to R.string.factory_status_production_info_btn_start,
                AssignmentState.REV_IN_PROGRESS to R.string.factory_status_production_info_btn_stop,
                AssignmentState.WAIT_QC to null
        )
    }
}