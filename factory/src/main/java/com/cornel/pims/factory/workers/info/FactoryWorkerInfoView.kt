package com.cornel.pims.factory.workers.info

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkersWorkplaceListUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.android.synthetic.main.common_workers_workplace_list_item.view.*
import kotlinx.android.synthetic.main.factory_worker_info_activity.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */

interface FactoryWorkerInfoView {
    val component: FactoryWorkerInfoComponent

    fun showWorkerName(text: String)
    fun showDateAndWorkingShift(text: String)
    fun showItems(data: List<WorkersWorkplaceListUnit>)
    fun showError(e: Throwable)
    fun finish()
}

class FactoryWorkerInfoActivity: BaseView(), FactoryWorkerInfoView {

    companion object {
        const val EXTRA_WORKER_ID = "worker_id"
    }

    @Inject
    override lateinit var presenter: FactoryWorkerInfoPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy {
        app.component.plus(FactoryWorkerInfoModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.factory_worker_info_activity)
        setSupportActionBar(toolbar)

        recycler.layoutManager = LinearLayoutManager(this)

        component.inject(this)

        val workerId = intent.getStringExtra(EXTRA_WORKER_ID)

        presenter.onCreate(workerId)

        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackButtonPressed()
        }
    }

    override fun showWorkerName(text: String) = workerName.setText(text)

    override fun showDateAndWorkingShift(text: String) = dateAndWorkingShiftText.setText(text)

    override fun showItems(data: List<WorkersWorkplaceListUnit>) {
        workplaceTitle.visibility = if (data.isEmpty()) View.VISIBLE else View.GONE
        recycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick, this::onItemRemoveBtnClick),
                false)
    }

    private fun onRecyclerItemClick(item: WorkersWorkplaceListUnit) = presenter.onItemSelected(item)
    private fun onItemRemoveBtnClick(item: WorkersWorkplaceListUnit) = presenter.onItemRemove(item)

    class RecyclerAdapter(private val data: List<WorkersWorkplaceListUnit>,
                          private val listener: (WorkersWorkplaceListUnit) -> Unit,
                          private val itemRemoveListener: (WorkersWorkplaceListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init { setHasStableIds(true) }

        class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view               = v
            val itemName: TextView = v.itemName
            val productionCount: TextView = v.productionCount

            fun bind(item: WorkersWorkplaceListUnit, listener: (WorkersWorkplaceListUnit) -> Unit,
                     itemRemoveListener: (WorkersWorkplaceListUnit) -> Unit) {
                val produced = view.context.getString(R.string.produced)
                itemName.text   = item.name
                val producedDuringWorkingShift = "${item.productionQuantity} $produced"
                productionCount.text = producedDuringWorkingShift
                view.setOnClickListener { listener(item) }
                view.removeBtn.setOnClickListener { itemRemoveListener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener, itemRemoveListener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_workers_workplace_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}