package com.cornel.pims.factory.status.workplacelist

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryProdWorkplaceListScope

@FactoryProdWorkplaceListScope
@Subcomponent(modules = [FactoryProdWorkplaceListModule::class])
interface FactoryProdWorkplaceListComponent {
    fun inject(activity: FactoryProdWorkplaceListActivity)
    fun inject(presenter: FactoryProdWorkplaceListPresenterImpl)
}

