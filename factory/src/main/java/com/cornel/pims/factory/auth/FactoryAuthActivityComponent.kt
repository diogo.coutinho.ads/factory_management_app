package com.cornel.pims.factory.auth

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class FactoryAuthScope
@FactoryAuthScope
@Subcomponent(modules = [FactoryAuthModule::class])
interface FactoryAuthComponent {
    fun inject(activity: FactoryAuthActivity)
    fun inject(presenter: FactoryAuthPresenterImpl)
}