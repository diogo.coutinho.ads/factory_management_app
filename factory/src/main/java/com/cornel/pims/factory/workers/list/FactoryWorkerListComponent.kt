package com.cornel.pims.factory.workers.list

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryWorkerListScope

@FactoryWorkerListScope
@Subcomponent(modules = [FactoryWorkerListModule::class])
interface FactoryWorkerListComponent {
    fun inject(activity: FactoryWorkerListActivity)
    fun inject(presenter: FactoryWorkerListPresenterImpl)
}

