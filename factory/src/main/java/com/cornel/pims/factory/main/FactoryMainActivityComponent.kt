package com.cornel.pims.factory.main

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryMainActivityScope

@FactoryMainActivityScope
@Subcomponent(modules = [FactoryMainActivityModule::class])
interface FactoryMainActivityComponent {
    fun inject(activity: FactoryMainActivity)
    fun inject(presenter: FactoryMainPresenterImpl)
}

