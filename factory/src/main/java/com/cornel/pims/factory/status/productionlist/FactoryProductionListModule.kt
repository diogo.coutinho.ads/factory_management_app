package com.cornel.pims.factory.status.productionlist

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 08.03.18.
 */

@Module
class FactoryProductionListModule(
        private val activity: FactoryProductionListActivity) {
    @FactoryProductionListScope
    @Provides
    fun providePresenter(): FactoryProductionListPresenter =
            FactoryProductionListPresenterImpl(activity)
}
