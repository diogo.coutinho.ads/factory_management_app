package com.cornel.pims.factory.workers.info

import android.content.Context
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkersWorkplaceListUnit
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 08.03.18.
 */
interface FactoryWorkerInfoPresenter : BasePresenter {
    fun onCreate(id: String)
    fun onRefresh()
    fun onItemSelected(item: WorkersWorkplaceListUnit)
    fun onBackButtonPressed()
    fun onItemRemove(item: WorkersWorkplaceListUnit)
}

class FactoryWorkerInfoPresenterImpl(
        private val view: FactoryWorkerInfoView)
    : BasePresenterImpl(view as BaseView), FactoryWorkerInfoPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var workerId: String

    override fun onCreate(id: String) {
        view.component.inject(this)
        workerId = id
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val date = SimpleDateFormat("d MMM", Locale("ru")).format(Date())

            val worker = get(dataManager.getWorkerById(workerId))!!
            val workshopId = dataManager.getCurrentWorkshopId()!!
            val workplacesDef = async { dataManager.getWorkplacesByWorkshopId(workshopId) }

            val workingShift = get(dataManager.getWorkingShift(workshopId))!!
            val workingShiftText = if (workingShift != -1)
                appContext.getString(R.string.working_shift, workingShift)
            else
                appContext.getString(R.string.working_shift_off)

            val dateAndWorkingShift = "$workingShiftText, $date"

            val workerName = "${worker.fullName} ${worker.patronymicName} " +
                    worker.lastName


            val workplaces = get(workplacesDef.await())!!
                    .filter {
                        worker.assignedWorkplacesIds.contains(it.id)
                    }
                    .map {
                        WorkersWorkplaceListUnit(it.id, it.name, it.producedQuantity)
                    }

            launch(UI) {
                view.showWorkerName(workerName)
                view.showDateAndWorkingShift(dateAndWorkingShift)
                view.showItems(workplaces)
            }
        }
    }

    override fun onItemSelected(item: WorkersWorkplaceListUnit) {}

    override fun onBackButtonPressed() {
        view.finish()
    }

    override fun onItemRemove(item: WorkersWorkplaceListUnit) {
        smartLaunch {


            runBlocking {
                // TODO check if ok
                val worker = get(dataManager.getWorkerById(workerId))!!

//                val text = appContext.getString(R.string.factory_workers_unregister_confirm, item.name, area.name)
                val text = "Вы уверены, что хотите удалить работника ${worker.fullName} с рабочей зоны ${item.name}?"

//                val btnText = appContext.getString(R.string.factory_workers_register_button)
                val btnText = "Удалить"

                launch(UI) {
                    (view as BaseView).showConfirmation(text,
                            btnText,
                            {unregisterWorker(item)})

//                    doOnResumeAsync()
                }

            }
        }
    }


    private fun unregisterWorker(item: WorkersWorkplaceListUnit) {
        smartLaunch {
            runBlocking {
                dataManager.unregisterWorker(workerId, item.id)
                doOnResumeAsync()
            }
        }
    }

}