package com.cornel.pims.factory.core.helper

import android.content.Context
import android.content.SharedPreferences
import com.cornel.pims.factory.core.app.App
import javax.inject.Inject

interface SharedPrefHelper {

    fun getAuthUrl(): String
    fun getApiUrl(): String
    fun getWorkshopId() : String
    fun getLogin(): String
    fun getSecret(): String

    fun setAuthUrl(url: String)
    fun setApiUrl(url: String)
    fun setWorkshopId(id: String)
    fun setLogin(login: String)
    fun setSecret(secret: String)

}

class SharedPrefHelperImpl(app: App) : SharedPrefHelper {

    @Inject
    lateinit var context: Context

    companion object {
        private const val APP_PREFERENCES = "pims_technical_stuff"
        private const val PREFERENCES_AUTH_URL = "auth_url"
        private const val PREFERENCES_API_URL = "api_url"
        private const val PREFERENCES_STORAGE = "default_storage"
        private const val PREFERENCES_LOGIN = "default_login"
        private const val PREFERENCES_SECRET = "default_secret"
        private const val PREFERENCE_WORKSHOP = "default_workshop"
    }

    private val sharedPref: SharedPreferences

    init {
        app.component.inject(this)

        sharedPref = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
    }


    override fun getWorkshopId(): String  = getString(PREFERENCE_WORKSHOP)

    override fun getLogin(): String = getString(PREFERENCES_LOGIN)

    override fun getSecret(): String = getString(PREFERENCES_SECRET)

    override fun setWorkshopId(id: String) = setString(PREFERENCE_WORKSHOP, id)

    override fun setLogin(login: String) = setString(PREFERENCES_LOGIN, login)

    override fun setSecret(secret: String) = setString(PREFERENCES_SECRET, secret)

    private fun getString(key: String) = sharedPref.getString(key, "")

    private fun getInt(key: String): Int = sharedPref.getInt(key, 0)

    private fun setString(key: String, value: String) {
        val prefEditor = sharedPref.edit()
        prefEditor.putString(key, value)
        prefEditor.apply()
    }

    override fun getAuthUrl(): String = sharedPref.getString(PREFERENCES_AUTH_URL, "")

    override fun getApiUrl(): String = sharedPref.getString(PREFERENCES_API_URL, "")


    override fun setAuthUrl(url: String) = setString(PREFERENCES_AUTH_URL, url)

    override fun setApiUrl(url: String) = setString(PREFERENCES_API_URL, url)

}
