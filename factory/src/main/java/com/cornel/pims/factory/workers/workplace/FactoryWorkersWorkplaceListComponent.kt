package com.cornel.pims.factory.workers.workplace

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryWorkersWorkplaceListScope

@FactoryWorkersWorkplaceListScope
@Subcomponent(modules = [FactoryWorkersWorkplaceListModule::class])
interface FactoryWorkersWorkplaceListComponent {
    fun inject(activity: FactoryWorkersWorkplaceListActivity)
    fun inject(presenter: FactoryWorkersWorkplaceListPresenterImpl)
}

