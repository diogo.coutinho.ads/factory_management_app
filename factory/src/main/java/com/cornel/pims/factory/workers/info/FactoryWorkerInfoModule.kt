package com.cornel.pims.factory.workers.info

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 08.03.18.
 */

@Module
class FactoryWorkerInfoModule(
        private val activity: FactoryWorkerInfoActivity) {
    @FactoryWorkerInfoScope
    @Provides
    fun providePresenter(): FactoryWorkerInfoPresenter =
            FactoryWorkerInfoPresenterImpl(activity)
}
