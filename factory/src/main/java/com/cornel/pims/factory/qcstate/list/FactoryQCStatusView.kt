package com.cornel.pims.factory.qcstate.list

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.QCListUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import com.cornel.pims.factory.core.proto.HasComponent
import com.cornel.pims.factory.qcstate.productinfo.FactoryQCProductInfoActivity
import kotlinx.android.synthetic.main.factory_qc_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by AlexGator on 08.03.18.
 */

interface FactoryQCStatusView {
    enum class Filter { ALL, AWAIT, REJECTED }
    val component: FactoryQCStatusComponent
    fun showItems(list: List<QCListUnit>)
    fun showError(e: Throwable)
    fun finish()
    fun navigateToQCInfo(id: String)
}

class FactoryQCStatusActivity
    : BaseView(), FactoryQCStatusView, HasComponent<FactoryQCStatusComponent> {

    @Inject
    override lateinit var presenter: FactoryQCStatusPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(FactoryQCStatusModule(this)) }

    private val fragments = listOf(
            FactoryQCStatusListFragment().setFilter(FactoryQCStatusView.Filter.AWAIT),
            FactoryQCStatusListFragment().setFilter(FactoryQCStatusView.Filter.ALL),
            FactoryQCStatusListFragment().setFilter(FactoryQCStatusView.Filter.REJECTED)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.factory_qc_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        pager.adapter = PagerAdapter(supportFragmentManager)
        pager.offscreenPageLimit = 2
        tabLayout.getTabAt(1)?.select() // By default select middle tab - "All"
        pager.currentItem = 1


        presenter.onCreate()

        addListeners()
    }

    private fun addListeners() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab) {
                pager.currentItem = tab.position
            }
        })

        pager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                tabLayout.getTabAt(position)!!.select()
            }
        })

        goBackBtn.onClick {
            presenter.onBackBtnPressed()
        }
    }

    override fun showItems(list: List<QCListUnit>) {
        fragments.forEach { it.setList(list) }
    }

    override fun navigateToQCInfo(id: String) {
        startActivity(intentFor<FactoryQCProductInfoActivity>(
                FactoryQCProductInfoActivity.EXTRA_PRODUCT_ID to id
        ))
    }

    inner class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount() = fragments.size
    }
}