package com.cornel.pims.factory.status.productioninfo

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 08.03.18.
 */

@Module
class FactoryProductInfoModule(
        private val activity: FactoryProductInfoActivity) {
    @FactoryProductInfoActivityScope
    @Provides
    fun providePresenter(): FactoryProductInfoPresenter =
            FactoryProductInfoPresenterImpl(activity)
}
