package com.cornel.pims.factory.qcstate.productinfo

import android.os.Bundle
import android.view.View
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.HistoryUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.android.synthetic.main.common_history_list_item.view.*
import kotlinx.android.synthetic.main.factory_qc_product_info.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by AlexGator on 14.02.18.
 */

interface FactoryQCProductInfoView {
    val component: FactoryQCProductInfoComponent

    fun setProductName(text: String)
    fun setAssignmentCode(text: String)
    fun setProductCode(text: String)
//    fun setPartner(text: String)
    fun setQuantity(text: String)
    fun showHistory(items: List<HistoryUnit>)
//    fun setManager(text: String)
//    fun setAdditionalInfo(text: String)
//    fun setComment(text: String)
    fun setState(text: String)
}

class FactoryQCProductInfoActivity : BaseView(), FactoryQCProductInfoView {

    companion object {
        const val EXTRA_PRODUCT_ID = "product_id"
    }

    @Inject
    override lateinit var presenter: FactoryQCProductInfoPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy {app.component.plus(FactoryQCProductInfoModule(this))}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.factory_qc_product_info)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        component.inject(this)

        val id = intent.getStringExtra(EXTRA_PRODUCT_ID)

        presenter.onCreate(id)

        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        product_info_btn_back.onClick {
            presenter.onBackBtnClick()
        }
    }

    override fun setProductName(text: String) {
        supportActionBar?.setTitle(text)
    }

    override fun setQuantity(text: String) = product_info_quantity.setText(text)

    override fun setProductCode(text: String) = sku.setText(text)

    override fun setAssignmentCode(text: String) = assignment_code.setText(text)

    override fun setState(text: String) = product_info_state.setText(text)

    override fun showHistory(items: List<HistoryUnit>) {
        historyContainer.removeAllViews()
        if (items.isEmpty()) {
            historyTitle.visibility = View.GONE
        } else {
            historyTitle.visibility = View.VISIBLE
            items.forEach {
                val view = layoutInflater
                        .inflate(R.layout.common_history_list_item, historyContainer, false)
                view.itemMessage.text = it.name
                view.itemDate.text = it.date
                historyContainer.addView(view)
            }
        }
    }

//    override fun setPartner(text: String) = product_info_partner.setText(text)
//
//    override fun setShippingDate(text: String) = product_info_shipping_date.setText(text)
//
//    override fun setManager(text: String) = product_info_manager.setText(text)
//
//    override fun setAdditionalInfo(text: String) = product_info_additional_info.setText(text)
//
//    override fun setComment(text: String) = product_info_comment.setText(text)
}