package com.cornel.pims.factory.core.proto

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.apollographql.apollo.exception.ApolloNetworkException
import com.cornel.pims.core.Barcode
import com.cornel.pims.core.BarcodeType
import com.cornel.pims.core.api.AuthenticationException
import com.cornel.pims.core.api.TokenException
import com.cornel.pims.core.barcode.BarcodeException
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.core.barcode.BarcodeSubscriber
import com.cornel.pims.factory.R
import com.cornel.pims.factory.auth.FactoryAuthActivity
import com.cornel.pims.factory.main.FactoryMainActivity
import com.cornel.pims.factory.status.productioninfo.FactoryProductInfoActivity
import com.cornel.pims.factory.status.productionlist.FactoryProductionListActivity
import com.cornel.pims.factory.workers.info.FactoryWorkerInfoActivity
import com.cornel.pims.factory.workers.list.FactoryWorkerListActivity
import com.cornel.pims.factory.workers.workplace.FactoryWorkersWorkplaceListActivity
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import java.net.ConnectException


/**
 * Created by AlexGator on 02.03.18.
 */


abstract class BaseView : AppCompatActivity(), BarcodeSubscriber {

    companion object {
        const val MESSAGE_DIALOG_LENGTH = 3000
    }

    abstract val presenter: BasePresenter
    abstract val barcodeHelper: BarcodeHelper

    private var barcodeSubscribeId: Int = -1

    private var preloaderDialog: DialogFragment? = null
    private var messageDialog: AlertDialog? = null

    protected var tag: String? = null

    override fun onResume() {
        super.onResume()
        presenter.onResume()
        barcodeHelper.subscribe(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
        barcodeHelper.unsubscribe(this)
    }

    fun showMessage(message: String) {
        val dialog = AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(true)
                .create()
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.show()
        messageDialog = dialog
        launch {
            delay(MESSAGE_DIALOG_LENGTH)
            messageDialog?.dismiss()
            messageDialog = null
        }
    }

    fun showPreloader() {
        if (preloaderDialog == null) {
            preloaderDialog = PreloaderDialog()
            preloaderDialog?.show(supportFragmentManager, "preloader")
        }
    }

    fun hidePreloader() {
        try {
            preloaderDialog?.dismiss()
        } catch (e: Exception) {
            error("Error while dismissing preloaderDialog: ", e)
        } finally {
            preloaderDialog = null
        }
    }

    fun isOk(ok: Boolean) {
        setResult(if (ok) Activity.RESULT_OK else Activity.RESULT_CANCELED)
    }

    fun showError(e: Throwable) {
        showError(e, true)
    }



    fun showError(e: Throwable, finishActivity: Boolean) {
        if (e is AuthenticationException){
            toast(R.string.error_authentication)
        } else if (e is TokenException) {
            val view = this
            launch(UI) {
                val intent = Intent(view, FactoryAuthActivity::class.java).apply {
                    putExtra(FactoryAuthActivity.EXTRA_TOKEN_FLAG, true)
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                }
                view.startActivity(intent)
                view.finish()
            }
        } else {
            val dialog = ErrorDialog()
            dialog.finishActivity = finishActivity
            dialog.text = when (e) {
                is BarcodeException -> getString(R.string.barcode_error, e.message)
                is ConnectException -> getString(R.string.error_connection)
                is ApolloNetworkException -> getString(R.string.error_apollo_network)
                else -> e.message ?: getString(R.string.error_unknown, e)
            }
            dialog.show(supportFragmentManager, "errorDialog")
            Log.e("ACTIVITY", "Error caught:", e)
        }
    }

    fun showLogoutDialog() {
        showDialog(getString(R.string.common_logout_title),
                getString(R.string.common_logout_text),
                getString(R.string.common_logout_exit), this::finishAndRemoveTask)
    }

    fun setOrHide(text: String, target: TextView, container: View? = null) {
        if (text.isEmpty()) {
            target.visibility = View.GONE
            container!!.visibility = View.GONE
        } else {
            container!!.visibility = View.VISIBLE
            target.visibility = View.VISIBLE
            target.text = text
        }
    }

    fun showConfirmation(text: String, actionBtnText: String, callback: () -> Unit) {
        val title = getString(R.string.common_confirm_action)
        showDialog(title, text, actionBtnText, callback)
    }

    fun showDialog(title: String, text: String,
                   actionBtnText: String, callback: () -> Unit) {
        val dialog = InfoDialog()
        val negativeBtnText = getString(R.string.common_dialog_cancel)

        dialog.apply {
            this.title = title
            this.text = text
            positiveBtnText = actionBtnText
            this.negativeBtnText = negativeBtnText
            this.callback = callback
        }
        dialog.show(supportFragmentManager, "infoDialog")
    }

    override fun onBarcodeError(e: Throwable) {
        showError(e, false)
    }

    override fun onBarcodeScan(barcode: Barcode) {
        info("Barcode scanned: $barcode")
        when (barcode.type) {
            BarcodeType.WORKPLACE -> {
                    AlertDialog.Builder(this)
                            .setTitle("Выберите действие")
                            .setCancelable(true)
                            .setPositiveButton("Работники") { _, _ ->
                                startActivity(intentFor<FactoryWorkerListActivity>(
                                        FactoryWorkerListActivity.EXTRA_WORKPLACE_ID to barcode.id
                                ))
                            }
                            .setNegativeButton("Изделия") { _, _ ->
                                startActivity(intentFor<FactoryProductionListActivity>(
                                        FactoryProductionListActivity.EXTRA_WORKPLACE_ID to barcode.id
                                ))
                            }
                            .create()
                            .show()
            }
            BarcodeType.WORKER -> {
                startActivity(intentFor<FactoryWorkerInfoActivity>(
                        FactoryWorkerInfoActivity.EXTRA_WORKER_ID to barcode.id
                ))
            }
            BarcodeType.ASSIGNMENT -> {
                startActivity(intentFor<FactoryProductInfoActivity>(
                        FactoryProductInfoActivity.EXTRA_PRODUCT_ID to barcode.id
                ))
            }
            BarcodeType.WORKSHOP -> {
                if (this is FactoryMainActivity) {
                    setSelectedWorkshop(barcode.id)
                } else {
                    startActivity(intentFor<FactoryMainActivity>(
                            FactoryMainActivity.EXTRA_WORKSHOP_ID to barcode.id
                    ))
                }
            }
            else -> showError(BarcodeException(
                    getString(R.string.barcode_error_type_not_supported)), false)
        }
    }

    class InfoDialog : DialogFragment() {
        lateinit var title: String
        lateinit var text: String
        lateinit var positiveBtnText: String
        lateinit var negativeBtnText: String
        lateinit var callback: () -> Unit
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            return AlertDialog.Builder(activity)
                    .setTitle(title)
                    .setMessage(text)
                    .setCancelable(false)
                    .setPositiveButton(positiveBtnText) { _, _ ->
                        callback()
                    }
                    .setNegativeButton(negativeBtnText) { _, _ -> }
                    .create()
        }
    }

    class ErrorDialog : DialogFragment() {
        lateinit var text: String
        var finishActivity: Boolean = true

        override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            isCancelable = false
            return super.onCreateView(inflater, container, savedInstanceState)
        }

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            return AlertDialog.Builder(activity)
                    .setTitle(R.string.error_title)
                    .setMessage(text)
                    .setCancelable(false)
                    .setPositiveButton(R.string.error_close) { _, _ ->
                        if (finishActivity) {
                            activity.finish()
                        }
                    }
                    .create()
        }
    }

    class PreloaderDialog : DialogFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.common_preloader, container, false)
            dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
            isCancelable = false
            return view
        }
    }

    fun info(text: String) {
        val logTag = tag ?: this::class.simpleName
        Log.i(logTag, text)
    }

    fun error(message: String, e: Throwable? = null) {
        Log.e(this::class.simpleName, message, e)
    }
}




