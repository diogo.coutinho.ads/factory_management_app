package com.cornel.pims.factory.core.app

import android.content.Context
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.core.barcode.BarcodeHelperImpl
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.DataManagerImpl
import com.cornel.pims.factory.core.helper.APIHelper
import com.cornel.pims.factory.core.helper.APIHelperImpl
import com.cornel.pims.factory.core.helper.SharedPrefHelper
import com.cornel.pims.factory.core.helper.SharedPrefHelperImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by AlexGator on 03.02.18.
 */

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    fun provideBarcodeHelper(): BarcodeHelper = BarcodeHelperImpl

    @Provides
    @Singleton
    fun provideDataManager(): DataManager = DataManagerImpl(app)

    @Provides
    @Singleton
    fun provideSharedPrefHelper(): SharedPrefHelper = SharedPrefHelperImpl(app)

    @Provides
    @Singleton
    fun provideAPIHelper(): APIHelper = APIHelperImpl(app)

    // Platform call, may produce NPE
    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

}
