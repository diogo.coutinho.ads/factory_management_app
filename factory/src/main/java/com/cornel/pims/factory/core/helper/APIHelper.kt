package com.cornel.pims.factory.core.helper

import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.exception.ApolloNetworkException
import com.cornel.pims.core.api.GraphQLAPI
import com.cornel.pims.factory.core.*
import com.cornel.pims.factory.core.app.App
import com.cornel.pims.factory.mutation.*
import com.cornel.pims.factory.query.*
import kotlinx.coroutines.experimental.CompletableDeferred
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.delay
import okhttp3.HttpUrl
import java.net.ConnectException
import javax.inject.Inject

interface APIHelper {
    fun connect(email: String, password: String)

    /* Queries */
    suspend fun getCurrentUser(): QResponse<User>
    suspend fun getAppPermission(): Boolean
    suspend fun getCurrentShift(workshopId: String): QResponse<Int>

    suspend fun getProductList(): QResponse<List<Product>>
    suspend fun getWorkshopsList(): QResponse<List<Workshop>>
    suspend fun getWorkplacesByWorkshopId(id: String): QResponse<List<Workplace>>
    suspend fun getWorkplaceById(id: String): QResponse<Workplace>
    suspend fun getWorkersByWorkplaceId(id: String): QResponse<List<Worker>>
    suspend fun getUnassignedWorkersByWorkplaceId(id: String): QResponse<List<Worker>>
    suspend fun getWorkerCountsByWorkplaceId(id: String): QResponse<WorkersCounts>
    suspend fun getWorkerById(id: String): QResponse<Worker>
    suspend fun getProductionAssignmentById(id: String): QResponse<ProductionAssignment?>
    suspend fun getProductionAssignmentsByWorkplaceId(id: String): QResponse<List<ProductionAssignment>>
    suspend fun getQCAssignmentsByWorkshopId(id: String): QResponse<List<QCProductionAssignment>>
    suspend fun getQCAssignmentById(id: String): QResponse<QCProductionAssignment>
    suspend fun getAssignedWorkersQuantityByWorkplaceId(id: String): QResponse<Int>
    suspend fun getCurrentJobsQuantityByWorkplaceId(id: String): QResponse<Int>
    suspend fun getFinishedJobsQuantityByWorkplaceId(id: String): QResponse<Int>

    /* Mutations */
    suspend fun assignWorkerToWorkplace(workerId: String, workplaceId: String): String
    suspend fun removeWorkerFromWorkplace(workerId: String, workplaceId: String): String
    suspend fun startProductionAssignment(id: String, workplaceId: String, splittingQuantity: Double): String
    suspend fun cancelProductionAssignment(id: String, splittingQuantity: Double): String
    suspend fun finishProductionAssignment(id: String, splittingQuantity: Double): String
    suspend fun suspendProductionAssignment(id: String, splittingQuantity: Double): String
    suspend fun resumeProductionAssignment(id: String, splittingQuantity: Double): String
    suspend fun startRevision(id: String, splittingQuantity: Double): String
    suspend fun finishRevision(id: String, splittingQuantity: Double): String
}

class APIHelperImpl(app: App) : APIHelper {

    @Inject
    lateinit var sharedPrefHelper: SharedPrefHelper

    init{
        app.component.inject(this)
    }

    companion object {
        //TODO set 5, 3000
        const val LOG_TAG = "API_HELPER"
        const val RETRY_COUNT = 3
        const val RETRY_DELAY = 1000

        const val AUTH_ERROR = "Not authorized"
        const val TOKEN_ERROR = "Token"
    }

    private val authURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1337)
            .addPathSegment("oauth")
            .addPathSegment("token")
            .build()!!

    private val apiURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1337)
            .addPathSegment("graphql")
            .build()!!

    private val wsURL = "ws://11.11.11.11:1338/cable"

    private lateinit var graphQLAPI: GraphQLAPI

    init {
        Log.d(LOG_TAG, "New APIHelper object created")
    }

    /**
     * Special function to rebuild private [GraphQLAPI] class.
     */
    private fun rebuildClient() {
        var i = 0
        // Retrying only on ConnectException
        while (i < RETRY_COUNT) {
            try {
                val email = sharedPrefHelper.getLogin()
                val password = sharedPrefHelper.getSecret()
                graphQLAPI = GraphQLAPI(apiURL, authURL, wsURL, email, password)
                Log.d(LOG_TAG, "Client rebuild")
                return
            } catch (e: ConnectException) {
                Log.e(LOG_TAG, "API client build failed", e)
                if (++i == RETRY_COUNT) throw e
            }
        }
    }

    /**
     * Special generic function which makes [RETRY_COUNT] retries
     * if error takes place while connecting.
     */
    private suspend fun <T> proceedQuery(requestFun: () -> Deferred<T>): T {
        var exception = Throwable()
        for (i in 0..RETRY_COUNT) {
            try {
                if (!this::graphQLAPI.isInitialized) {
                    rebuildClient()
                }
                return requestFun().await()
            } catch (e: ConnectException) {
                exception = e
                Log.e(LOG_TAG, "Request to Auth server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            } catch (e: ApolloNetworkException) {
                // Handles every exception during API server call, and invalid token too
                exception = e
                Log.e(LOG_TAG, "Request to API server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            }
        }

        throw exception
    }

    private fun <R, D> response(response: Response<R>, data: D?): QResponse<D> {
        return if (response.errors().isEmpty())
            QResponse(data, null)
        else
            QResponse(null, when {
                (response.errors().find { it.message()!!.startsWith(AUTH_ERROR) } != null) -> QError.AUTH
                (response.errors().find { it.message()!!.startsWith(TOKEN_ERROR) } != null) -> QError.TOKEN
                else -> QError.SERVER
            })
    }

    /*##################################################################################*/
    /*######################## INTERFACE IMPLEMENTATION ################################*/
    /*##################################################################################*/

    /* Authentication */
    override fun connect(email: String, password: String) {
        sharedPrefHelper.setLogin(email)
        sharedPrefHelper.setSecret(password)
        rebuildClient()
    }
    /*##################################################################################*/


    /* Common */
    override suspend fun getCurrentUser() = proceedQuery { getCurrentUserDefer() }

    private fun getCurrentUserDefer(): Deferred<QResponse<User>> {
        val deferredUser = CompletableDeferred<QResponse<User>>()

        graphQLAPI.client.query(GetCurrentUserQuery())
                .enqueue(object : ApolloCall.Callback<GetCurrentUserQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get current_user object", e)
                        deferredUser.cancel(e)
                    }

                    override fun onResponse(response: Response<GetCurrentUserQuery.Data>) {
                        Log.i(LOG_TAG, "Current user query proceeded successfully")
                        Log.i(LOG_TAG, "Current user ID: ${response.data()?.current_user()?.id()}")
                        deferredUser.complete(response(response, if (!response.errors().isEmpty()) null else
                            User(response.data()!!.current_user()!!.id(),
                                response.data()!!.current_user()!!.full_name())))
                    }
                })

        return deferredUser
    }

    override suspend fun getAppPermission() = proceedQuery { getAppPermissionDef() }

    private fun getAppPermissionDef() : Deferred<Boolean> {
        val deferredPermission = CompletableDeferred<Boolean>()

        graphQLAPI.client.query(GetAppPermissionQuery())
                .enqueue(object : ApolloCall.Callback<GetAppPermissionQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get permission object", e)
                        deferredPermission.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAppPermissionQuery.Data>) {
                        Log.i(LOG_TAG, "Permission query proceeded successfully")
                        val permission = response.data()?.current_user()?.permissions()?.find{ it.action() == "access" && it.secured_object() == "production_app"}
                        if (permission == null) {
                            Log.i(LOG_TAG, "Permission to app was denied")
                        } else {
                            Log.i(LOG_TAG, "Permission to app was accepted")
                        }
                        deferredPermission.complete(permission != null)
                    }
                })

        return deferredPermission
    }

    override suspend fun getCurrentShift(workshopId: String) = proceedQuery { getCurrentShiftDefer(workshopId) }

    private fun getCurrentShiftDefer(workshopId: String) : Deferred<QResponse<Int>> {
        val deferredShift = CompletableDeferred<QResponse<Int>>()

        graphQLAPI.client.query(GetCurrentShiftQuery(workshopId))
                .enqueue(object : ApolloCall.Callback<GetCurrentShiftQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get current_shift object", e)
                        deferredShift.cancel(e)
                    }

                    override fun onResponse(response: Response<GetCurrentShiftQuery.Data>) {
                        Log.i(LOG_TAG, "Current shift query proceeded successfully")
                        val number = response.data()?.current_shift()?.number()?.toInt()
                        if (number == null) {
                            Log.i(LOG_TAG, "Current shift was null")
                        } else {
                            Log.i(LOG_TAG, "Current shift: $number")
                        }
                        deferredShift.complete(response(response, if (!response.errors().isEmpty()) null else
                            number ?: -1))
                    }
                })

        return deferredShift
    }

    override suspend fun getProductList()
            = proceedQuery(this::getProductListDefer)

    private fun getProductListDefer(): Deferred<QResponse<List<Product>>> {
        val deferredProductList = CompletableDeferred<QResponse<List<Product>>>()

        graphQLAPI.client.query(GetProductListQuery())
                .enqueue(object : ApolloCall.Callback<GetProductListQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Products list", e)
                        deferredProductList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetProductListQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()}")
                        deferredProductList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.products()!!.map {
                            Product(it.id(), it.name(), it.code())
                        }))
                    }
                })

        return deferredProductList
    }

    override suspend fun getWorkshopsList(): QResponse<List<Workshop>>
            = proceedQuery(this::getWorkshopsListDefer)

    private fun getWorkshopsListDefer(): Deferred<QResponse<List<Workshop>>> {
        val deferredWorkshopsList = CompletableDeferred<QResponse<List<Workshop>>>()

        graphQLAPI.client.query(GetWorkshopsListQuery())
                .enqueue(object : ApolloCall.Callback<GetWorkshopsListQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Workshops list", e)
                        deferredWorkshopsList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkshopsListQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.workshops()}")
                        deferredWorkshopsList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workshops()!!.map {
                            Workshop(it.id(), it.code(), it.name())
                        }))
                    }
                })

        return deferredWorkshopsList
    }

    override suspend fun getWorkplacesByWorkshopId(id: String)
            = proceedQuery { getWorkplacesByWorkshopIdDefer(id) }

    private fun getWorkplacesByWorkshopIdDefer(id: String): Deferred<QResponse<List<Workplace>>> {
        val deferredWorkplaces = CompletableDeferred<QResponse<List<Workplace>>>()

        graphQLAPI.client.query(GetWorkplacesByWorkshopIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetWorkplacesByWorkshopIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Workplaces list", e)
                        deferredWorkplaces.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkplacesByWorkshopIdQuery.Data>) {
                        Log.i(LOG_TAG, "Workplaces queried successfully")

                        deferredWorkplaces.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workplaces()!!.map {
                            Workplace(it.id(),
                                    it.code(),
                                    it.name(),
                                    it.current_technical_control_assignments_count()!!.toInt(),
                                    it.current_production_assignments_count()!!.toInt(),
                                    it.product_quantity()!!.toInt(),
                                    it.assigned_workers_count()!!.toInt(),
                                    it.assigned_workers()!!.map { it.id() })
                        }))
                    }
                })

        return deferredWorkplaces
    }


    override suspend fun getWorkplaceById(id: String)
            = proceedQuery { getWorkplaceByIdDefer(id) }

    private fun getWorkplaceByIdDefer(id: String): Deferred<QResponse<Workplace>> {
        val deferredWorkspace = CompletableDeferred<QResponse<Workplace>>()

        graphQLAPI.client.query(GetWorkplaceByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetWorkplaceByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Workplace", e)
                        deferredWorkspace.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkplaceByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Workplace queried successfully")
                        Log.i(LOG_TAG, "Queried Workspace ID: ${response.data()?.workplace()?.id()}")

                        deferredWorkspace.complete(response(response, if (!response.errors().isEmpty()) null else {
                            val workplace = response.data()!!.workplace()!!
                            val workersIds = workplace.assigned_workers()!!.map { it.id() }

                            val producedQuantity = workplace.current_technical_control_assignments_count()!!.toInt()
                            val currentJobsQuantity = workplace.current_production_assignments_count()!!.toInt()

                            Workplace(response.data()!!.workplace()!!.id(),
                                    response.data()!!.workplace()!!.code(),
                                    response.data()!!.workplace()!!.name(),
                                    producedQuantity,
                                    currentJobsQuantity,
                                    response.data()!!.workplace()!!.product_quantity()!!.toInt(),
                                    response.data()!!.workplace()!!.assigned_workers_count()!!.toInt(),
                                    workersIds)
                        }))
                    }
                })

        return deferredWorkspace
    }

    override suspend fun getWorkersByWorkplaceId(id: String)
            = proceedQuery { getWorkersByWorkplaceIdDefer(id) }

    private fun getWorkersByWorkplaceIdDefer(id: String): Deferred<QResponse<List<Worker>>> {
        val deferredWorkersList = CompletableDeferred<QResponse<List<Worker>>>()

        graphQLAPI.client.query(GetWorkersByWorkplaceIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetWorkersByWorkplaceIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Workers list by WorkplaceId", e)
                        deferredWorkersList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkersByWorkplaceIdQuery.Data>) {
                        Log.i(LOG_TAG, "Workers list by Workplace ID queried successfully")
                        deferredWorkersList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workplace()!!.assigned_workers()!!.map {
                            Worker(it.id(),
                                    it.full_name()?: "",
                                    "",
                                    "",
                                    emptyList())
                        }))
                    }
                })

        return deferredWorkersList
    }


    override suspend fun getUnassignedWorkersByWorkplaceId(id: String)
            = proceedQuery { getUnassignedWorkersByWorkplaceIdDefer(id) }

    private fun getUnassignedWorkersByWorkplaceIdDefer(id: String): Deferred<QResponse<List<Worker>>> {
        val deferredWorkersList = CompletableDeferred<QResponse<List<Worker>>>()

        graphQLAPI.client.query(GetUnassignedWorkersByWorkplaceIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetUnassignedWorkersByWorkplaceIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query unassigned Workers list by Workplace ID", e)
                        deferredWorkersList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetUnassignedWorkersByWorkplaceIdQuery.Data>) {
                        Log.i(LOG_TAG, "Unassigned Workers list by Workplace ID queried successfully")
                        deferredWorkersList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workplace()!!.unassigned_workers()!!.map {
                            Worker(it.id(),
                                    it.full_name()?: "",
                                    "",
                                    "",
                                    emptyList())
                        }))
                    }
                })

        return deferredWorkersList
    }

    override suspend fun getWorkerCountsByWorkplaceId(id: String): QResponse<WorkersCounts>
            = proceedQuery { getWorkerCountsByWorkplaceIdDef(id) }

    private fun getWorkerCountsByWorkplaceIdDef(id: String): Deferred<QResponse<WorkersCounts>> {
        val deferredCounts = CompletableDeferred<QResponse<WorkersCounts>>()

        graphQLAPI.client.query(GetWorkerCountsByWorkplaceIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetWorkerCountsByWorkplaceIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query workers counts by Workplace ID", e)
                        deferredCounts.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkerCountsByWorkplaceIdQuery.Data>) {
                        Log.i(LOG_TAG, "Workers counts by Workplace ID queried successfully")
                        deferredCounts.complete(
                                response(response, if (!response.errors().isEmpty()) null else
                                    WorkersCounts(
                                        response.data()!!.workplace()!!.worker_quantity()!!,
                                        response.data()!!.workplace()!!.assigned_workers_count()!!
                                )
                        ))
                    }
                })

        return deferredCounts
    }

    override suspend fun getWorkerById(id: String)
            = proceedQuery { getWorkerByIdDefer(id) }

    // TODO Decide if we use full_name, or name,surname,patronimic
    private fun getWorkerByIdDefer(id: String): Deferred<QResponse<Worker>> {
        val deferredWorker = CompletableDeferred<QResponse<Worker>>()

        graphQLAPI.client.query(GetWorkerByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetWorkerByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Worker", e)
                        deferredWorker.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkerByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Worker queried successfully")
                        Log.i(LOG_TAG, "Queried Worker ID: ${response.data()?.worker()?.id()}")
                        deferredWorker.complete(response(response, if (!response.errors().isEmpty()) null else
                            Worker(response.data()!!.worker()!!.id(),
                                response.data()?.worker()?.full_name()?: "",
                                "",
                                "",
                                response.data()!!.worker()!!.assigned_workplaces()!!.map { it.id() })))
                    }
                })

        return deferredWorker
    }


    override suspend fun getProductionAssignmentById(id: String)
            = proceedQuery { getProductionAssignmentByTaskIdDefer(id) }

    private fun getProductionAssignmentByTaskIdDefer(id: String): Deferred<QResponse<ProductionAssignment?>> {
        val deferredProductionAssignment = CompletableDeferred<QResponse<ProductionAssignment?>>()

        graphQLAPI.client.query(GetProductionAssignmentByIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetProductionAssignmentByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Production Assignmnet by ID", e)
                        deferredProductionAssignment.cancel(e)
                    }

                    override fun onResponse(response: Response<GetProductionAssignmentByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Product Assignment by ID queried successfully")
                        deferredProductionAssignment.complete(
                                response(response, if (!response.errors().isEmpty()) null else {
                                    val assignment = response.data()?.production_assignment()
                                    if (assignment == null) {
                                        null
                                    } else {
                                        val resourcesMap = assignment.current_operation()?.resources()
                                                ?.associate {
                                                    it.product().id() to it.quantity()
                                                }
                                        val sideProductsMap = assignment.current_operation()?.side_products()
                                                ?.associate {
                                                    it.product().id() to it.quantity()
                                                }

                                        ProductionAssignment(
                                                assignment.id(),
                                                assignment.code(),
                                                assignment.current_workplace()?.name() ?: "",
                                                assignment.current_operation()?.available_workplaces()?.map {
                                                    ProductionAssignment.ShortWorkplace(
                                                            it.id(),
                                                            it.name()
                                                    )
                                                } ?: emptyList(),
                                                assignment.route_map().product().id(),
                                                assignment.route_map().product().code(),
                                                assignment.quantity(),
                                                assignment.route_map().product().unit()!!.abbreviation(),
                                                assignment.factory_events()?.map {
                                                    HistoryUnit(it.message(), it.date())
                                                } ?: emptyList(),
                                                FactoryOperation("",
                                                        assignment.current_operation()?.name()
                                                                ?: "",
                                                        -1,
                                                        arrayListOf()),
                                                assignment.state(),
                                                assignment.deleted_at(),
                                                resourcesMap,
                                                sideProductsMap
                                        )
                                    }
                                }
                        ))
                    }
                })

        return deferredProductionAssignment
    }


    override suspend fun getProductionAssignmentsByWorkplaceId(id: String)
            = proceedQuery { getProductionAssignmentsByWorkplaceIdDefer(id) }

    private fun getProductionAssignmentsByWorkplaceIdDefer(id: String): Deferred<QResponse<List<ProductionAssignment>>> {
        val deferredProductionAssignmentsList = CompletableDeferred<QResponse<List<ProductionAssignment>>>()

        graphQLAPI.client.query(GetProductionAssignmentsByWorkplaceIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetProductionAssignmentsByWorkplaceIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Factory Operations by Workplace ID", e)
                        deferredProductionAssignmentsList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetProductionAssignmentsByWorkplaceIdQuery.Data>) {
                        Log.i(LOG_TAG, "Factory Operations by Workplace ID queried successfully")
                        deferredProductionAssignmentsList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workplace()!!.current_production_assignments()!!.map {
                            ProductionAssignment(
                                    it.id(),
                                    it.code(),
                                    "",
                                    emptyList(),
                                    it.route_map().product().id(),
                                    it.route_map().product().code(),
                                    it.quantity(),
                                    "",
                                    emptyList(),
                                    FactoryOperation("", it.current_operation()?.name()?: "", -1, emptyList()),
                                    it.state(),
                                    null)
                        }))

                    }
                })

        return deferredProductionAssignmentsList
    }


    override suspend fun getQCAssignmentsByWorkshopId(id: String): QResponse<List<QCProductionAssignment>> {
        return proceedQuery { getQCAssignmentsByWorkshopIdDefer(id) }
    }

    private fun getQCAssignmentsByWorkshopIdDefer(id: String): Deferred<QResponse<List<QCProductionAssignment>>> {
        val deferredAssignmentList = CompletableDeferred<QResponse<List<QCProductionAssignment>>>()

        graphQLAPI.client.query(GetQCAssignmentsByWorkshopIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetQCAssignmentsByWorkshopIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Query failed", e)
                        deferredAssignmentList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetQCAssignmentsByWorkshopIdQuery.Data>) {
                        deferredAssignmentList.complete(response(response, if (!response.errors().isEmpty()) null else {
                            val list = mutableListOf<QCProductionAssignment>()
                            response.data()!!.workshop()!!.workplaces()!!.forEach {
                                it.current_technical_control_assignments()!!.forEach {
                                    list.add(QCProductionAssignment(
                                            it.id(),
                                            it.code(),
                                            it.quantity(),
                                            it.route_map().product().id(),
                                            it.state(),
                                            "",
                                            "",
                                            "",
                                            it.route_map().product().unit()!!.abbreviation(),
                                            emptyList()))
                                }
                            }
                            list
                        }))
                    }
                })

        return deferredAssignmentList
    }


    override suspend fun getQCAssignmentById(id: String): QResponse<QCProductionAssignment> {
        return proceedQuery { getQCAssignmentByIdDefer(id) }
    }

    private fun getQCAssignmentByIdDefer(id: String): Deferred<QResponse<QCProductionAssignment>> {
        val deferredAssignmentList = CompletableDeferred<QResponse<QCProductionAssignment>>()

        graphQLAPI.client.query(GetQCAssignmentByIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetQCAssignmentByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Query failed", e)
                        deferredAssignmentList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetQCAssignmentByIdQuery.Data>) {
                        deferredAssignmentList.complete(response(response, if (!response.errors().isEmpty()) null else {
                            val assignment = response.data()!!.production_assignment()!!

                            QCProductionAssignment(
                                    assignment.id(),
                                    assignment.code(),
                                    assignment.quantity(),
                                    assignment.route_map().product().id(),
                                    assignment.state(),
                                    "",
                                    "",
                                    "",
                                    assignment.route_map().product().unit()!!.abbreviation(),
                                    assignment.factory_events()?.map {
                                        HistoryUnit(it.message(), it.date())
                                    } ?: emptyList()
                            )
                        }))
                    }
                })

        return deferredAssignmentList
    }

    override suspend fun getCurrentJobsQuantityByWorkplaceId(id: String)
            = proceedQuery { getCurrentJobsQuantityByWorkplaceIdDefer(id) }

    private fun getCurrentJobsQuantityByWorkplaceIdDefer(id: String): Deferred<QResponse<Int>> {
        val deferredQuantity = CompletableDeferred<QResponse<Int>>()

        graphQLAPI.client.query(GetCurrentJobsByWorkplaceIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetCurrentJobsByWorkplaceIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query current jobs quantity", e)
                        deferredQuantity.cancel(e)
                    }

                    override fun onResponse(response: Response<GetCurrentJobsByWorkplaceIdQuery.Data>) {
                        Log.i(LOG_TAG, "Current jobs quantity queried successfully")

                        deferredQuantity.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workplace()!!.current_production_assignments_count()!!.toInt()))
                    }
                })

        return deferredQuantity

    }

    override suspend fun getFinishedJobsQuantityByWorkplaceId(id: String)
            = proceedQuery { getFinishedJobsQuantityByWorkplaceIdDefer(id) }

    private fun getFinishedJobsQuantityByWorkplaceIdDefer(id: String): Deferred<QResponse<Int>> {
        val deferredQuantity = CompletableDeferred<QResponse<Int>>()

        graphQLAPI.client.query(GetFinishedJobsQuantityByWorkplaceIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetFinishedJobsQuantityByWorkplaceIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query finished jobs(sent to QC) quantity", e)
                        deferredQuantity.cancel(e)
                    }

                    override fun onResponse(response: Response<GetFinishedJobsQuantityByWorkplaceIdQuery.Data>) {
                        Log.i(LOG_TAG, "Finished jobs(sent to QC) quantity queried successfully")

                        deferredQuantity.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workplace()!!.current_technical_control_assignments_count()!!.toInt()))
                    }
                })

        return deferredQuantity
    }

    override suspend fun getAssignedWorkersQuantityByWorkplaceId(id: String)
            = proceedQuery { getAssignedWorkersQuantityByWorkplaceIdDefer(id) }

    private fun getAssignedWorkersQuantityByWorkplaceIdDefer(id: String): Deferred<QResponse<Int>> {
        val deferredQuantity = CompletableDeferred<QResponse<Int>>()

        graphQLAPI.client.query(GetAssignedWorkersQuantityByWorkplaceIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetAssignedWorkersQuantityByWorkplaceIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query assigned workers quantity", e)
                        deferredQuantity.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAssignedWorkersQuantityByWorkplaceIdQuery.Data>) {
                        Log.e(LOG_TAG, "Assigned workers quantity queried successfully")
                        deferredQuantity.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.workplace()!!.assigned_workers_count()!!.toInt()))
                    }
                })

        return deferredQuantity
    }

    /* Mutations */

    override suspend fun assignWorkerToWorkplace(workerId: String, workplaceId: String)
            = proceedQuery { assignWorkerToWorkplaceDefer(workerId, workplaceId) }

    private fun assignWorkerToWorkplaceDefer(workerId: String, workplaceId: String): Deferred<String> {
        val deferredWorkerId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(AssignWorkerToWorkplaceMutation(workerId, workplaceId))
                .enqueue(object : ApolloCall.Callback<AssignWorkerToWorkplaceMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to assign Worker to Workplace", e)
                        deferredWorkerId.cancel(e)
                    }

                    override fun onResponse(response: Response<AssignWorkerToWorkplaceMutation.Data>) {
                        Log.i(LOG_TAG, "Worker assigned to Workplace successfully")
                        Log.i(LOG_TAG, "Assigned Worker ID: ${response.data()?.assign_worker_to_workplace()?.worker()?.id()}")
                        deferredWorkerId.complete(response.data()!!.assign_worker_to_workplace().worker().id())
                    }
                })

        return deferredWorkerId
    }

    override suspend fun removeWorkerFromWorkplace(workerId: String, workplaceId: String)
            = proceedQuery { removeWorkerFromWorkplaceDefer(workerId, workplaceId) }

    private fun removeWorkerFromWorkplaceDefer(workerId: String, workplaceId: String): Deferred<String> {
        val deferredWorkerId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(RemoveWorkerFromWorkplacesMutation(workerId, workplaceId))
                .enqueue(object : ApolloCall.Callback<RemoveWorkerFromWorkplacesMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to remove the Worker", e)
                        deferredWorkerId.cancel(e)
                    }

                    override fun onResponse(response: Response<RemoveWorkerFromWorkplacesMutation.Data>) {
                        Log.i(LOG_TAG, "Worker removed successfully")
                        Log.i(LOG_TAG, "Removed Worker ID: ${response.data()?.remove_worker_from_workplace()?.worker()?.id()}")
                        deferredWorkerId.complete(response.data()!!.remove_worker_from_workplace().worker().id())
                    }
                })

        return deferredWorkerId
    }


    override suspend fun startProductionAssignment(id: String, workplaceId: String, splittingQuantity: Double)
            = proceedQuery { startProductionAssignmentDefer(id, workplaceId, splittingQuantity) }

    private fun startProductionAssignmentDefer(id: String, workplaceId: String, splittingQuantity: Double): Deferred<String> {

        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(StartProductionAssignmentByIdMutation(id, workplaceId, sQuantity))
                .enqueue(object: ApolloCall.Callback<StartProductionAssignmentByIdMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to start Production Assignment with ID: $id", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<StartProductionAssignmentByIdMutation.Data>) {
                        Log.i(LOG_TAG, "Production Assignment with ID: $id started successfully")
                        deferredStatus.complete("Success")
                    }
                })

        return deferredStatus
    }

    override suspend fun cancelProductionAssignment(id: String, splittingQuantity: Double)
            = proceedQuery { cancelProductionAssignmentDefer(id, splittingQuantity) }

    private fun cancelProductionAssignmentDefer(id: String, splittingQuantity: Double): Deferred<String> {
        val deferredCancelledAssignmentId = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(CancelProductionAssignmentByIdMutation(id, sQuantity))
                .enqueue(object: ApolloCall.Callback<CancelProductionAssignmentByIdMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to cancel Production Assignment with ID: $id", e)
                        deferredCancelledAssignmentId.cancel(e)
                    }

                    override fun onResponse(response: Response<CancelProductionAssignmentByIdMutation.Data>) {
                        Log.i(LOG_TAG, "Production Assignment with ID: $id cancelled successfully")
                        deferredCancelledAssignmentId.complete(response.data()!!.cancel_production_assignment().id())
                    }
                })

        return deferredCancelledAssignmentId
    }


    override suspend fun finishProductionAssignment(id: String, splittingQuantity: Double)
            = proceedQuery { finishProductionAssignmentDefer(id, splittingQuantity) }

    private fun finishProductionAssignmentDefer(id: String, splittingQuantity: Double): Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(FinishProductionAssignmentMutation(id, sQuantity))
                .enqueue(object: ApolloCall.Callback<FinishProductionAssignmentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to finish Production Assignment with ID: $id", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<FinishProductionAssignmentMutation.Data>) {
                        Log.i(LOG_TAG, "Production Assignment with ID: $id finished successfully")
                        deferredStatus.complete("Success")
                    }
                })

        return deferredStatus
    }



    override suspend fun suspendProductionAssignment(id: String, splittingQuantity: Double)
            = proceedQuery { suspendProductionAssignmentDefer(id, splittingQuantity) }

    private fun  suspendProductionAssignmentDefer(id: String, splittingQuantity: Double): Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(SuspendProductionAssignmentByIdMutation(id, sQuantity))
                .enqueue(object: ApolloCall.Callback<SuspendProductionAssignmentByIdMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to suspend Production Assignment with ID: $id", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<SuspendProductionAssignmentByIdMutation.Data>) {
                        Log.i(LOG_TAG, "Production Assignment with ID: $id suspended successfully")
                        deferredStatus.complete("Success")
                    }
                })

        return deferredStatus
    }

    override suspend fun resumeProductionAssignment(id: String, splittingQuantity: Double)
            = proceedQuery { resumeProductionAssignmentDefer(id, splittingQuantity) }

    private fun resumeProductionAssignmentDefer(id: String, splittingQuantity: Double): Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(ResumeProductionAssignmentByIdMutation(id, sQuantity))
                .enqueue(object: ApolloCall.Callback<ResumeProductionAssignmentByIdMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to resume Production Assignment", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<ResumeProductionAssignmentByIdMutation.Data>) {
                        Log.i(LOG_TAG, "Production Assignment with ID: $id resumed successfully")
                        deferredStatus.complete("Success")
                    }
                })

        return deferredStatus
    }

    override suspend fun startRevision(id: String, splittingQuantity: Double)
            = proceedQuery { startRevisionDefer(id, splittingQuantity) }

    private fun startRevisionDefer(id: String, splittingQuantity: Double): Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(StartRevisionForProductionAssignmentMutation(id, sQuantity))
                .enqueue(object: ApolloCall.Callback<StartRevisionForProductionAssignmentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to start Revision for Production Assignment with ID: $id", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<StartRevisionForProductionAssignmentMutation.Data>) {
                        Log.i(LOG_TAG, "Revision for Production Assignment with ID: $id started successfully")
                        deferredStatus.complete("Success")
                    }
                })

        return deferredStatus
    }


    override suspend fun finishRevision(id: String, splittingQuantity: Double)
            = proceedQuery { finishRevisionDefer(id, splittingQuantity) }

    private fun finishRevisionDefer(id: String, splittingQuantity: Double): Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(FinishRevisionForProductionAssignmentMutation(id, sQuantity))
                .enqueue(object: ApolloCall.Callback<FinishRevisionForProductionAssignmentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to finish Production Assignment with ID: $id", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<FinishRevisionForProductionAssignmentMutation.Data>) {
                        Log.i(LOG_TAG, "Production Assignment with ID: $id finished successfully")
                        deferredStatus.complete("Success")
                    }
                })

        return deferredStatus
    }

}
