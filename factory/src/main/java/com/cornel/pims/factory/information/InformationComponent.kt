package com.cornel.pims.factory.information

import dagger.Subcomponent
import javax.inject.Scope


@Scope
annotation class InformationScope

@InformationScope
@Subcomponent(modules = [InformationModule::class])
interface InformationComponent {
    fun inject(activity: InformationActivity)
    fun inject(presenter: InformationPresenterImpl)
}
