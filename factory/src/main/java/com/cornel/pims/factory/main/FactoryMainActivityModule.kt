package com.cornel.pims.factory.main

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 07.03.18.
 */


@Module
class FactoryMainActivityModule(private val activity: FactoryMainActivity) {

    @FactoryMainActivityScope
    @Provides
    fun providePresenter(): FactoryMainPresenter = FactoryMainPresenterImpl(activity)
}
