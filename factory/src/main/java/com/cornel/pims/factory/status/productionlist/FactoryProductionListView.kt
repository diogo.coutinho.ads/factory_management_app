package com.cornel.pims.factory.status.productionlist

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.ProductAssignmentListUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import com.cornel.pims.factory.status.productioninfo.FactoryProductInfoActivity
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import kotlinx.android.synthetic.main.common_production_list_item.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.w3c.dom.Text
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */

interface FactoryProductionListView {
    val component: FactoryProductionListComponent

    fun showItems(data: List<ProductAssignmentListUnit>)
    fun showError(e: Throwable)
    fun finish()
    fun setTitle(title: String)
    fun navigateToProductInfo(id: String, workplaceId: String)
}

class FactoryProductionListActivity: BaseView(), FactoryProductionListView {

    companion object {
        const val EXTRA_WORKPLACE_ID = "workplace_id"
    }

    @Inject
    override lateinit var presenter: FactoryProductionListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy {
        app.component.plus(FactoryProductionListModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        setSupportActionBar(toolbar)

        component.inject(this)

        val id = intent.getStringExtra(EXTRA_WORKPLACE_ID)

        presenter.onCreate(id)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackButtonPressed()
        }
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun showItems(data: List<ProductAssignmentListUnit>) {
        recycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick, this),
                false)
    }

    private fun onRecyclerItemClick(item: ProductAssignmentListUnit) = presenter.onItemSelected(item)

    override fun navigateToProductInfo(id: String, workplaceId: String) {
        startActivity(intentFor<FactoryProductInfoActivity>(
                FactoryProductInfoActivity.EXTRA_PRODUCT_ID to id,
                FactoryProductInfoActivity.EXTRA_WORKPLACE_ID to workplaceId
        ))
    }

    class RecyclerAdapter(private val data: List<ProductAssignmentListUnit>,
                          private val listener: (ProductAssignmentListUnit) -> Unit,
                          private val context: Context)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init { setHasStableIds(true) }

        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view               = v
            val itemId: TextView = v.itemCode
            val itemName: TextView = v.itemName
            val itemSKU: TextView = v.itemSKU
            val itemOperation: TextView = v.itemOperation
            val itemStatus: TextView   = v.itemStatus
            val itemShipDate: TextView = v.itemShipDate
            val itemStateIcon: CardView = v.stateIcon

            fun bind(item: ProductAssignmentListUnit, listener: (ProductAssignmentListUnit) -> Unit) {
                itemId.text     = item.code
                itemName.text   = item.name
                itemSKU.text    = context.getString(R.string.factory_status_production_list_sku, item.sku)
                itemOperation.text = item.operation
                itemStatus.text = context.getString(item.stateResId)
                itemShipDate.text = item.shipmentDate
                itemStateIcon.setCardBackgroundColor(
                        ContextCompat.getColor(context, item.colorResId)
                )
                view.setOnClickListener { listener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_production_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}