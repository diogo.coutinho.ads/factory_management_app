package com.cornel.pims.factory.workers.list

import android.os.Bundle
import android.support.v7.widget.DialogTitle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.Barcode
import com.cornel.pims.core.BarcodeType
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkerListUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import com.cornel.pims.factory.workers.add.FactoryWorkerAddActivity
import com.cornel.pims.factory.workers.info.FactoryWorkerInfoActivity
import kotlinx.android.synthetic.main.common_workers_workplace_list_item.view.*
import kotlinx.android.synthetic.main.factory_worker_list.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */

interface FactoryWorkerListView {
    val component: FactoryWorkerListComponent

    fun showItems(data: List<WorkerListUnit>)
    fun showError(e: Throwable)
    fun finish()
    fun navigateToWorkerInfo(id: String)
    fun navigateToAddWorkerView(workplaceId: String)
    fun setTitle(title: String)
}

class FactoryWorkerListActivity: BaseView(), FactoryWorkerListView {

    companion object {
        const val EXTRA_WORKPLACE_ID = "workplace_id"
        const val EXTRA_WORKPLACE_NAME = "workplace_name"
    }

    @Inject
    override lateinit var presenter: FactoryWorkerListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy {
        app.component.plus(FactoryWorkerListModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.factory_worker_list)
        setSupportActionBar(toolbar)

        component.inject(this)

        val name = intent.getStringExtra(EXTRA_WORKPLACE_NAME) ?: "Регистрация работников"
        val id = intent.getStringExtra(EXTRA_WORKPLACE_ID)

        supportActionBar?.title = name

        presenter.onCreate(id)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        addListeners()
    }

    override fun onBarcodeScan(barcode: Barcode) {
        if (barcode.type == BarcodeType.WORKER) {
            presenter.registerWorker(barcode.id)
        } else {
            super.onBarcodeScan(barcode)
        }
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackButtonPressed()
        }
        addWorkerBtn.onClick {
            presenter.onAddWorkerBtnPressed()
        }
    }

    override fun showItems(data: List<WorkerListUnit>) {
        recycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick, this::onItemRemoveBtnClick),
                false)
    }

    private fun onRecyclerItemClick(item: WorkerListUnit) = presenter.onItemSelected(item)
    private fun onItemRemoveBtnClick(item: WorkerListUnit) = presenter.onItemRemove(item)

    override fun navigateToWorkerInfo(id: String) {
        startActivity(intentFor<FactoryWorkerInfoActivity>(
                FactoryWorkerInfoActivity.EXTRA_WORKER_ID to id
        ))
    }

    override fun navigateToAddWorkerView(workplaceId: String) {
        startActivity(intentFor<FactoryWorkerAddActivity>(
                FactoryWorkerAddActivity.EXTRA_WORKPLACE_ID to workplaceId
        ))
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    class RecyclerAdapter(private val data: List<WorkerListUnit>,
                          private val listener: (WorkerListUnit) -> Unit,
                          private val itemRemoveListener: (WorkerListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init { setHasStableIds(true) }

        class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view               = v
            val itemName: TextView = v.itemName

            fun bind(item: WorkerListUnit, listener: (WorkerListUnit) -> Unit,
                     itemRemoveListener: (WorkerListUnit) -> Unit) {
                itemName.text   = item.name
                view.setOnClickListener { listener(item) }
                view.removeBtn.setOnClickListener { itemRemoveListener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener, itemRemoveListener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_worker_list_item_with_remove, parent, false)
            return ViewHolder(v)
        }
    }
}