package com.cornel.pims.factory.core

import android.util.Log
import com.cornel.pims.factory.core.app.App
import com.cornel.pims.factory.core.helper.APIHelper
import com.cornel.pims.factory.core.helper.SharedPrefHelper
import kotlinx.coroutines.experimental.sync.Mutex
import kotlinx.coroutines.experimental.sync.withLock
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

interface DataManager {

    /* Init */
    suspend fun connect(login: String, password: String)

    suspend fun getUser(): QResponse<User> // Common
    suspend fun getAppPermission(): Boolean
    suspend fun getWorkingShift(workshopId: String): QResponse<Int> // Common
    suspend fun getProductList(): QResponse<List<Product>>
    suspend fun getProductById(id: String): QResponse<Product>

    suspend fun getWorkshopList(): QResponse<List<Workshop>> // Factory, QC
    suspend fun getWorkshopById(id: String): QResponse<Workshop> // Factory, QC
    suspend fun getCurrentWorkshop(): QResponse<Workshop> // Factory, QC


    suspend fun getWorkerById(id: String): QResponse<Worker> // Factory
    suspend fun getWorkersByWorkplaceId(id: String): QResponse<List<Worker>> // Factory
    suspend fun getUnassignedWorkerListByWorkplaceId(id: String): QResponse<List<Worker>> //Factory
    suspend fun getWorkerCountsByWorkplaceId(id: String): QResponse<WorkersCounts>

    suspend fun getWorkplacesByWorkshopId(id: String): QResponse<List<Workplace>> // Factory, QC
    suspend fun getWorkplaceById(id: String): QResponse<Workplace>

    suspend fun getQCAssignmentsByWorkshopId(id: String): QResponse<List<QCProductionAssignment>> // Factory, QC
    suspend fun getQCAssignmentById(id: String): QResponse<QCProductionAssignment>

    suspend fun getProductionAssignmentById(id: String): QResponse<ProductionAssignment?>
    suspend fun getProductionAssignmentsByWorkplaceId(id: String): QResponse<List<ProductionAssignment>>

    /* Shared Preferences */
    fun getAuthUrl(): String // Common
    fun getApiUrl(): String // Common
    fun getCurrentWorkshopId(): String // Factory, QC
    fun getLogin(): String // Common

    fun setAuthUrl(url: String) // Common
    fun setApiUrl(url: String) // Common
    fun setCurrentWorkshopId(id: String) // Factory, QC
    fun setLogin(login: String) // Common

    /* Mutations */

    suspend fun registerWorker(workerId: String, workplaceId: String)
    suspend fun unregisterWorker(workerId: String, workplaceId: String)
    suspend fun startProductionAssignment(id: String, workplaceId: String, splittingQuantity: Double)
    suspend fun cancelProductionAssignment(id: String, splittingQuantity: Double)
    suspend fun finishProductionAssignment(id: String, splittingQuantity: Double)
    suspend fun suspendProductionAssignment(id: String, splittingQuantity: Double)
    suspend fun resumeProductionAssignment(id: String, splittingQuantity: Double)
    suspend fun startRevision(id: String, splittingQuantity: Double)
    suspend fun finishRevision(id: String, splittingQuantity: Double)
}

/*
 * TODO
 * Проработать логику если DM не возвращает корректных значений, определить поведение
 * в этом случае или показывать подробную ошибку на русском
*/
class DataManagerImpl(app: App) : DataManager {

    @Inject
    lateinit var sharedPrefHelper: SharedPrefHelper

    @Inject
    lateinit var apiHelper: APIHelper


    init {
        app.component.inject(this)
    }

    private fun info(message: String) {
        Log.i(this::class.simpleName, message)
    }

    override suspend fun connect(login: String, password: String) {
        apiHelper.connect(login, password)
    }

    override suspend fun getWorkshopList(): QResponse<List<Workshop>> {
        return apiHelper.getWorkshopsList()
    }

    override suspend fun getWorkshopById(id: String): QResponse<Workshop> {
        val list = getWorkshopList()
        return QResponse(list.data?.find { it.id == id }, list.error)
    }

    override suspend fun getCurrentWorkshop(): QResponse<Workshop> = getWorkshopById(getCurrentWorkshopId())

    override suspend fun getProductList(): QResponse<List<Product>> {
        return apiHelper.getProductList()
    }

    override suspend fun getProductById(id: String): QResponse<Product> {
        val list = getProductList()
        val product = QResponse(list.data?.find { it.id == id }
                ?: throw RuntimeException("Cannot find product with code=$id"), list.error)
        info("Got product: ${product.data}")
        return product
    }

    override suspend fun getUser(): QResponse<User> {
        return apiHelper.getCurrentUser()
    }

    override suspend fun getAppPermission(): Boolean {
        return apiHelper.getAppPermission()
    }

    override fun getCurrentWorkshopId(): String {
        val id = sharedPrefHelper.getWorkshopId()
        info("Got current workshop ID: $id")
        return id
    }

    override fun setCurrentWorkshopId(id: String) {
        sharedPrefHelper.setWorkshopId(id)
        info("Have set current workshop ID: $id")
    }

    override suspend fun getWorkplacesByWorkshopId(id: String): QResponse<List<Workplace>> {
        val workplaces = apiHelper.getWorkplacesByWorkshopId(id)
        info("Got workplaces: $workplaces")

//        return workplaces
        return workplaces
    }

    override suspend fun getWorkersByWorkplaceId(id: String): QResponse<List<Worker>> {
        val worker = apiHelper.getWorkersByWorkplaceId(id)
        info("Got worker: $worker")
        return worker
    }

    override suspend fun getUnassignedWorkerListByWorkplaceId(id: String): QResponse<List<Worker>> {
        val workers = apiHelper.getUnassignedWorkersByWorkplaceId(id)
        info("Fetched unassigned workers: $workers")
        return workers
    }

    override suspend fun getWorkerCountsByWorkplaceId(id: String): QResponse<WorkersCounts> {
        val counts = apiHelper.getWorkerCountsByWorkplaceId(id)
        info("Fetched workers counts: $counts")
        return counts
    }

    override suspend fun getWorkerById(id: String): QResponse<Worker> {
        val worker = apiHelper.getWorkerById(id)
        info("Fetched worker: $worker")
        return worker
    }

    override suspend fun getWorkplaceById(id: String): QResponse<Workplace> {
        val workplace = apiHelper.getWorkplaceById(id)
        info("Fetched workplace: $workplace")
        return workplace
    }

    override suspend fun getWorkingShift(workshopId: String): QResponse<Int> {
        return apiHelper.getCurrentShift(workshopId)
    }

    override suspend fun getQCAssignmentsByWorkshopId(id: String): QResponse<List<QCProductionAssignment>> {
        val assignments = apiHelper.getQCAssignmentsByWorkshopId(id)
        info("Fetched assingments: $assignments")
        return assignments
    }

    override suspend fun getQCAssignmentById(id: String): QResponse<QCProductionAssignment> {
        val assignment = apiHelper.getQCAssignmentById(id)
        info("Fetched assingment: $assignment")
        return assignment
    }

    override suspend fun registerWorker(workerId: String, workplaceId: String) {
        apiHelper.assignWorkerToWorkplace(workerId, workplaceId)
        info("Worker registered: $workerId")
    }

    override suspend fun unregisterWorker(workerId: String, workplaceId: String) {
        apiHelper.removeWorkerFromWorkplace(workerId, workplaceId)
        info("Worker unregistered: $workerId")
    }

    override fun getLogin(): String {
        val login = sharedPrefHelper.getLogin()
        info("Got login: $login")
        return login
    }

    override fun setAuthUrl(url: String) {
        sharedPrefHelper.setAuthUrl(url)
        info("Auth url set: $url")
    }

    override fun setApiUrl(url: String) {
        sharedPrefHelper.setApiUrl(url)
        info("API url set: $url")
    }

    override fun setLogin(login: String) {
        sharedPrefHelper.setLogin(login)
        info("Login set: $login")
    }

    override fun getAuthUrl(): String {
        val url = sharedPrefHelper.getAuthUrl()
        info("Got auth url : $url")
        return url
    }

    override fun getApiUrl(): String {
        val url = sharedPrefHelper.getApiUrl()
        info("Got API url: $url")
        return url
    }

    override suspend fun getProductionAssignmentById(id: String): QResponse<ProductionAssignment?> {
        val assignment = apiHelper.getProductionAssignmentById(id)
        info("Fetched assignment: $assignment")
        return assignment
    }

    override suspend fun startProductionAssignment(id: String, workplaceId: String, splittingQuantity: Double) {
        apiHelper.startProductionAssignment(id, workplaceId, splittingQuantity)
        info("Assignment started: $id")
    }

    override suspend fun cancelProductionAssignment(id: String, splittingQuantity: Double) {
        apiHelper.cancelProductionAssignment(id, splittingQuantity)
        info("Assignment canceled: $id")
    }

    override suspend fun suspendProductionAssignment(id: String, splittingQuantity: Double) {
        apiHelper.suspendProductionAssignment(id, splittingQuantity)
        info("Assignment suspended: $id")
    }

    override suspend fun resumeProductionAssignment(id: String, splittingQuantity: Double) {
        apiHelper.resumeProductionAssignment(id, splittingQuantity)
        info("Assignment resumed: $id")
    }

    override suspend fun finishProductionAssignment(id: String, splittingQuantity: Double) {
        apiHelper.finishProductionAssignment(id, splittingQuantity)
        info("Assignment finished: $id")
    }

    override suspend fun getProductionAssignmentsByWorkplaceId(id: String): QResponse<List<ProductionAssignment>> {
        val operation = apiHelper.getProductionAssignmentsByWorkplaceId(id)
        info("Got operation: $operation")
        return operation
    }

    override suspend fun startRevision(id: String, splittingQuantity: Double) {
        apiHelper.startRevision(id, splittingQuantity)
        info("Revision started: $id")
    }

    override suspend fun finishRevision(id: String, splittingQuantity: Double) {
        apiHelper.finishRevision(id, splittingQuantity)
        info("Revision started: $id")
    }
}

