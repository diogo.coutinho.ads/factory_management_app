package com.cornel.pims.factory.workers.workplace

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkplaceListUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import com.cornel.pims.factory.workers.list.FactoryWorkerListActivity
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import kotlinx.android.synthetic.main.common_workplace_list_item_workers.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */

interface FactoryWorkersWorkplaceListView {
    val component: FactoryWorkersWorkplaceListComponent

    fun showItems(data: List<WorkplaceListUnit>)
    fun showError(e: Throwable)
    fun finish()
    fun navigateToWorkerList(id: String, name: String)
}

class FactoryWorkersWorkplaceListActivity: BaseView(), FactoryWorkersWorkplaceListView {

    @Inject
    override lateinit var presenter: FactoryWorkersWorkplaceListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy {
        app.component.plus(FactoryWorkersWorkplaceListModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        setSupportActionBar(toolbar)

        component.inject(this)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        presenter.onCreate()
        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackButtonPressed()
        }
    }

    override fun showItems(data: List<WorkplaceListUnit>) {
        recycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick),
                false)
    }

    private fun onRecyclerItemClick(item: WorkplaceListUnit) = presenter.onItemSelected(item)

    override fun navigateToWorkerList(id: String, name: String) {
        startActivity(intentFor<FactoryWorkerListActivity>(
                FactoryWorkerListActivity.EXTRA_WORKPLACE_ID to id,
                FactoryWorkerListActivity.EXTRA_WORKPLACE_NAME to name
        ))
    }

    class RecyclerAdapter(private val data: List<WorkplaceListUnit>,
                          private val listener: (WorkplaceListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init { setHasStableIds(true) }

        class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view                = v
            val itemName: TextView = v.itemName
            val itemCount: TextView = v.itemCount

            fun bind(item: WorkplaceListUnit, listener: (WorkplaceListUnit) -> Unit) {
                itemName.text   = item.name
                itemCount.text  = item.count.toString()
                view.setOnClickListener { listener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_workplace_list_item_workers, parent, false)
            return ViewHolder(v)
        }
    }
}