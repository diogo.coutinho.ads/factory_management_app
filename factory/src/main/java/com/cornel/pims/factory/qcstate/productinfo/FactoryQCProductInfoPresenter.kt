package com.cornel.pims.factory.qcstate.productinfo

import android.content.Context
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.HistoryUnit
import com.cornel.pims.factory.core.AssignmentState
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.Product
import com.cornel.pims.factory.core.QCProductionAssignment
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by AlexGator on 13.03.18.
 */

interface FactoryQCProductInfoPresenter : BasePresenter {
    fun onCreate(id: String)
    fun onRefresh()
    fun onBackBtnClick()
}

class FactoryQCProductInfoPresenterImpl(private val view: FactoryQCProductInfoView)
    : BasePresenterImpl(view as BaseView), FactoryQCProductInfoPresenter {

    companion object {
        val stateTextMap = mapOf (
                AssignmentState.ARRIVED to R.string.factory_qc_status_state_arrived,
                AssignmentState.IN_PROGRESS to R.string.factory_qc_status_state_in_progress,
                AssignmentState.PAUSED to R.string.factory_qc_status_state_paused,
                AssignmentState.WAIT_REV to R.string.factory_qc_status_state_wait_revision,
                AssignmentState.REV_IN_PROGRESS to R.string.factory_qc_status_state_revision_in_progress,
                AssignmentState.WAIT_QC to R.string.factory_qc_status_state_wait_qc,
                AssignmentState.REJECTED to R.string.factory_qc_status_state_rejected,
                AssignmentState.WAIT_ACCEPT to R.string.factory_qc_status_state_wait_accept,
                AssignmentState.ACCEPT_STORAGE to R.string.factory_qc_status_state_accept_storage,
                AssignmentState.UNKNOWN to R.string.factory_qc_status_state_unknown
        )
    }

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var assignmentId: String
    private lateinit var assignment: QCProductionAssignment
    private lateinit var product: Product

    override fun onCreate(id: String) {
        view.component.inject(this)
        assignmentId = id
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            assignment = get(dataManager.getQCAssignmentById(assignmentId))!!
            product = get(dataManager.getProductById(assignment.productId))!!
            val stateResId = stateTextMap[assignment.state]
            val state = if (stateResId != null)
                appContext.getString(stateResId) else assignment.state?.value ?: AssignmentState.UNKNOWN.value
            launch(UI) { view.apply {
                setProductName(product.name)
                setProductCode(product.code)
                setAssignmentCode(assignment.code)
                showHistory(assignment.history.map{
                    HistoryUnit(it.message, it.date)
                })
                setState(state)
                setQuantity(assignment.quantity.toString())
            }}
        }
    }

    override fun onBackBtnClick() {
        (view as BaseView).finish()
    }
}