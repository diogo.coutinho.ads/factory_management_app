package com.cornel.pims.factory.qcstate.productinfo

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 13.03.18.
 */

@Module
class FactoryQCProductInfoModule(private val activity: FactoryQCProductInfoView) {
    @FactoryQCProductInfoScope
    @Provides
    fun providePresenter(): FactoryQCProductInfoPresenter = FactoryQCProductInfoPresenterImpl(activity)
}