package com.cornel.pims.factory.auth

import android.os.Bundle
import android.os.Handler
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import com.cornel.pims.factory.main.FactoryMainActivity
import kotlinx.android.synthetic.main.factory_auth_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import javax.inject.Inject


interface FactoryAuthView {

    val component: FactoryAuthComponent

    fun setLogin(text: String)
    fun showError(e: Throwable, finishActivity: Boolean)
    fun finish()
    fun navigateToMainActivity()
}

class FactoryAuthActivity : BaseView(), FactoryAuthView {

    companion object {
        const val EXTRA_TOKEN_FLAG = "Token flag"
        const val TOKEN_TOAST_DELAY = 250L
    }

    override val component by lazy { app.component.plus(FactoryAuthModule(this))}

    @Inject
    override lateinit var presenter: FactoryAuthPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.factory_auth_activity)

        component.inject(this)

        if (intent.getBooleanExtra(EXTRA_TOKEN_FLAG, false)) {
            Handler().postDelayed({
                toast("Время сеанса закончено. Необходима авторизация.")
            }, TOKEN_TOAST_DELAY)
        }

        presenter.onCreate()

        addListeners()

        //TODO Remove autologin
       // presenter.onLogin("admin@example.com", "password")
    }


    private fun addListeners() {
        enter.onClick {
            val userLogin = login.text.toString()
            val userPass = password.text.toString()
            presenter.onLogin(userLogin, userPass)
        }
    }

    override fun navigateToMainActivity() {
        startActivity(intentFor<FactoryMainActivity>())
    }

    override fun setLogin(text: String) = login.setText(text)
}

