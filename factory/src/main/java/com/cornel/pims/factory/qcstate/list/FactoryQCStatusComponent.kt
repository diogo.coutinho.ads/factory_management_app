package com.cornel.pims.factory.qcstate.list

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryQCStatusActivityScope

@FactoryQCStatusActivityScope
@Subcomponent(modules = [FactoryQCStatusModule::class])
interface FactoryQCStatusComponent {
    fun inject(activity: FactoryQCStatusActivity)
    fun inject(presenter: FactoryQCStatusPresenterImpl)
    fun inject(fragment: FactoryQCStatusListFragment)
}

