package com.cornel.pims.factory.core.proto

/**
 * Created by AlexGator on 02.04.18.
 */

interface HasComponent<out Component> {
    val component: Component
}