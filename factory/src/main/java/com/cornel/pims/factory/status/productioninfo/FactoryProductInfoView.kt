package com.cornel.pims.factory.status.productioninfo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ArrayAdapter
import com.cornel.pims.core.Formatter
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.HistoryUnit
import com.cornel.pims.factory.common.ItemListUnit
import com.cornel.pims.factory.common.WorkplaceUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.android.synthetic.main.common_history_list_item.view.*
import kotlinx.android.synthetic.main.common_item_list_item.view.*
import kotlinx.android.synthetic.main.factory_product_info_activity.*
import kotlinx.android.synthetic.main.factory_production_cancel_dialog.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 14.02.18.
 */

interface FactoryProductInfoView {
    val component: FactoryProductInfoComponent

    enum class ResourceCancelAction {
        RETURN, WRITE_OFF, BLOCK
    }

    fun setProductName(text: String)
    fun setProductCode(text: String)
    fun showWorkplaces(units: List<WorkplaceUnit>)
    fun setWorkplace(text: String)
    fun hideWorkplace()
    fun setAssignmentCode(text: String)
    fun getSelectedWorkplace() : WorkplaceUnit
//    fun setPartner(text: String)
//    fun setShippingDate(text: String)
//    fun setManager(text: String)
//    fun setAdditionalInfo(text: String)
    fun setActionBtnText(text: String)
    fun setMaxQuantity(quantity: Double)
    fun setQuantity(quantity: Double)
    fun setOperationType(type: String)
    fun getQuantity(): Double?
    fun setState(state: String)
    fun showOrHideDeleteDate(text: String)
    fun showOrHidePauseBtn(isVisible: Boolean)
    fun showOrHideRejectBtn(isVisible: Boolean)
    fun showError(e: Throwable)
    fun showCancelDialog(callback: (FactoryProductInfoView.ResourceCancelAction, String) -> Unit)
    fun finish()
    fun showResources(items: List<ItemListUnit>)
    fun showSideProducts(items: List<ItemListUnit>)
    fun showHistory(items: List<HistoryUnit>)
}

class FactoryProductInfoActivity : BaseView(), FactoryProductInfoView {

    companion object {
        const val EXTRA_PRODUCT_ID = "product_id"
        const val EXTRA_WORKPLACE_ID = "workplace_id"
    }

    @Inject
    override lateinit var presenter: FactoryProductInfoPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    private var maxQuantity = 0.0
    private lateinit var workplaceId: String

    override val component by lazy { app.component.plus(FactoryProductInfoModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.factory_product_info_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        supportActionBar?.title = ""

        val extras = intent.extras
        val id = extras.getString(EXTRA_PRODUCT_ID)
        workplaceId = extras.getString(EXTRA_WORKPLACE_ID, "")

        presenter.onCreate(id)

        addListeners()
    }

    override fun onResume() {
        setActionBtnText("")
        showOrHidePauseBtn(false)
        showOrHideRejectBtn(false)
        hideWorkplace()
        super.onResume()
    }

    override fun onNewIntent(intent: Intent?) {
        val extras = intent!!.extras
        val id = extras.getString(EXTRA_PRODUCT_ID)
        workplaceId = extras.getString(EXTRA_WORKPLACE_ID, "")

        presenter.onCreate(id)
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackButtonPressed()
        }
        actionBtn.onClick {
            presenter.onActionBtnClick()
        }
        rejectBtn.onClick {
            presenter.onCancelBtnClick()
        }
        pauseBtn.onClick {
            presenter.onPauseBtnClick()
        }
    }

    override fun setProductName(text: String) {
        supportActionBar?.title = text
    }

    override fun setProductCode(text: String) {
        itemCode.setText(text)
    }

//    override fun setPartner(text: String) = partner.setText(text)
//
//    override fun setShippingDate(text: String) = shippingDate.setText(text)
//
//    override fun setManager(text: String) = manager.setText(text)
//
//    override fun setAdditionalInfo(text: String) = additionalInfo.setText(text)

    override fun setOperationType(type: String) {
        operationType.setText(type)
    }

    override fun setAssignmentCode(text: String) {
        assignmentCode.setText(text)
    }

    override fun setActionBtnText(text: String) {
        if (text.isEmpty()) {
            actionBtn.visibility = View.GONE
        } else {
            actionBtn.visibility = View.VISIBLE
            actionBtn.setText(text)
        }
    }

    override fun setState(state: String) = itemState.setText(state)

    override fun showOrHideDeleteDate(text: String) = setOrHide(text, itemDeleteDate, itemDeleteDateContainer)

    override fun showWorkplaces(units: List<WorkplaceUnit>) {
        workplaceContainer.visibility = View.GONE
        workplaceLayout.visibility = View.VISIBLE
        workplaceSpinner.visibility = View.VISIBLE
        workplaceSpinner.adapter = WorkplaceAdapter(this,
                R.layout.common_spinner_list_item,
                R.id.itemName,
                units)

        if (!workplaceId.isEmpty())
            for (i in 0 until units.size) {
                if (units[i].id == workplaceId) {
                    workplaceSpinner.setSelection(i)
                    break
                }
            }
    }

    override fun setWorkplace(text: String) {
        workplaceLayout.visibility = View.GONE
        workplaceSpinner.visibility = View.GONE
        workplaceContainer.visibility = View.VISIBLE
        workplace.setText(text)
    }

    override fun hideWorkplace() {
        workplaceLayout.visibility = View.GONE
        workplaceSpinner.visibility = View.GONE
        workplaceContainer.visibility = View.GONE
    }

    override fun getSelectedWorkplace(): WorkplaceUnit = workplaceSpinner.selectedItem as WorkplaceUnit

    override fun setMaxQuantity(quantity: Double) {
        maxQuantity = quantity
    }

    override fun setQuantity(quantity: Double) = itemQuantity.setText(Formatter.format(quantity))

    override fun getQuantity(): Double? {
        val illegalQuantityErr = getString(R.string.factory_status_production_info_illegal_quantity)
        val mustBeGreaterZeroErr = getString(R.string.factory_status_production_info_must_be_greater_zero)
        val tooMuchErr = getString(R.string.factory_status_production_info_too_much_quantity)

        val quantityValue = try {
            itemQuantity.text.toString().toDouble()
        } catch (e: Throwable) {
            itemQuantity.error = illegalQuantityErr
            return null
        }

        return when {
            quantityValue <= 0.0 -> {
                itemQuantity.error = mustBeGreaterZeroErr
                null
            }
            quantityValue > maxQuantity -> {
                itemQuantity.error = tooMuchErr
                null
            }
            else -> quantityValue
        }
    }

    override fun showOrHidePauseBtn(isVisible: Boolean) {
        pauseBtn.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    override fun showOrHideRejectBtn(isVisible: Boolean) {
        rejectBtn.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    override fun showResources(items: List<ItemListUnit>) {
        resourcesContainer.removeAllViews()
        if (items.isEmpty()) {
            resourcesTitle.visibility = View.GONE
        } else {
            resourcesTitle.visibility = View.VISIBLE
            items.forEach {
                val view = layoutInflater
                        .inflate(R.layout.common_item_list_item, resourcesContainer, false)
                val code = getString(R.string.factory_status_production_info_code, it.code)
                view.itemName.text = it.name
                view.itemCount.text = it.quantity.toString()
                view.itemCode.text = code
                resourcesContainer.addView(view)
            }
        }
    }

    override fun showSideProducts(items: List<ItemListUnit>) {
        sideProductsContainer.removeAllViews()
        if (items.isEmpty()) {
            sideProductsTitle.visibility = View.GONE
        } else {
            sideProductsTitle.visibility = View.VISIBLE
            items.forEach {
                val view = layoutInflater
                        .inflate(R.layout.common_item_list_item, sideProductsContainer, false)
                val code = getString(R.string.factory_status_production_info_code, it.code)
                view.itemName.text = it.name
                view.itemCount.text = it.quantity.toString()
                view.itemCode.text = code
                sideProductsContainer.addView(view)
            }
        }
    }

    override fun showHistory(items: List<HistoryUnit>) {
        historyContainer.removeAllViews()
        if (items.isEmpty()) {
            historyTitle.visibility = View.GONE
        } else {
            historyTitle.visibility = View.VISIBLE
            items.forEach {
                val view = layoutInflater
                        .inflate(R.layout.common_history_list_item, historyContainer, false)
                view.itemMessage.text = it.name
                view.itemDate.text = it.date
                historyContainer.addView(view)
            }
        }
    }

    override fun showCancelDialog(
            callback: (FactoryProductInfoView.ResourceCancelAction, String) -> Unit) {
        val dialog = CancelDialog()
        dialog.callback = callback
        dialog.show(supportFragmentManager, "cancel_dialog")
    }

    class CancelDialog: DialogFragment() {
        lateinit var callback: (FactoryProductInfoView.ResourceCancelAction, String) -> Unit
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(
                    R.layout.factory_production_cancel_dialog, container, false)
            dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
            isCancelable = true

            view.submitBtn.onClick {
                val cause = view.cause.text.toString()
                val selected = when(view.radioGroup.checkedRadioButtonId) {
                    R.id.radioReturn -> FactoryProductInfoView.ResourceCancelAction.RETURN
                    R.id.radioWriteOff -> FactoryProductInfoView.ResourceCancelAction.WRITE_OFF
                    R.id.radioBlock -> FactoryProductInfoView.ResourceCancelAction.BLOCK
                    else -> return@onClick
                }
                callback(selected, cause)
                dialog.dismiss()
            }

            view.closeBtn.onClick {
                dialog.dismiss()
            }

            return view
        }
    }


    class WorkplaceAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<WorkplaceUnit>
    ) : ArrayAdapter<WorkplaceUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item, parent, false)
            view.itemName.text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_spinner_list_item_dropdown, parent, false)
            view.itemName.text = values[position].name
            return view
        }
    }
}