package com.cornel.pims.factory.status.workplacelist

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 08.03.18.
 */

@Module
class FactoryProdWorkplaceListModule(
        private val activity: FactoryProdWorkplaceListActivity) {
    @FactoryProdWorkplaceListScope
    @Provides
    fun providePresenter(): FactoryProdWorkplaceListPresenter =
            FactoryProdWorkplaceListPresenterImpl(activity)
}
