package com.cornel.pims.factory.main

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.auth.FactoryAuthActivity
import com.cornel.pims.factory.common.WorkshopListUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import com.cornel.pims.factory.information.InformationActivity
import com.cornel.pims.factory.qcstate.list.FactoryQCStatusActivity
import com.cornel.pims.factory.status.workplacelist.FactoryProdWorkplaceListActivity
import com.cornel.pims.factory.workers.workplace.FactoryWorkersWorkplaceListActivity
import kotlinx.android.synthetic.main.common_main_spinner_list_item.view.*
import kotlinx.android.synthetic.main.factory_main_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 07.03.18.
 */

interface FactoryMainView {
    // The action buttons which may be clicked by user in the current view
    enum class Button { WORKERS, PRODUCTION, QC }
    enum class Route { WORKERS, PRODUCTION, QC, SETTINGS, AUTH }

    val component: FactoryMainActivityComponent

    fun showError(e: Throwable)
    fun finish()
    fun hidePreloader()
    fun setWorkingShift(text: String)
    fun setDate(text: String)
    fun setUserName(name: String)
    fun setAwaitCount(text: String)
    fun setQCRejectedCount(text: String)
    fun setSelectedWorkshop(id: String)
    fun setWorkshopSpinner(workshopList: List<WorkshopListUnit>)
    fun navigateTo(route: Route)
    fun finishAndRemoveTask()
    fun showLogoutDialog()
}

class FactoryMainActivity : BaseView(), FactoryMainView {

    companion object {
        const val EXTRA_WORKSHOP_ID = "workshop_id"
    }

    override val component by lazy { app.component.plus(FactoryMainActivityModule(this))}

    @Inject
    override lateinit var presenter: FactoryMainPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    private lateinit var spinnerValues: List<WorkshopListUnit>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.factory_main_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        barcodeHelper.register(this)

        val workshopId = intent.getStringExtra(EXTRA_WORKSHOP_ID)

        presenter.onCreate(workshopId)

        addListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_activity_appbar_menu, menu)
        return true
    }

    private fun addListeners() {

        btnWorkers.onClick {
            presenter.onActionBtnClick(FactoryMainView.Button.WORKERS)
        }

        btnProduction.onClick {
            presenter.onActionBtnClick(FactoryMainView.Button.PRODUCTION)
        }

        btnQC.onClick {
            presenter.onActionBtnClick(FactoryMainView.Button.QC)
        }

        workshopSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?,
                                        position: Int, rowId: Long) {
                val selected = workshopSpinner.selectedItem as WorkshopListUnit
                presenter.onWorkshopChange(selected)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_settings -> presenter.onSettingsClick()
            R.id.action_logout -> presenter.onLogoutClick()
        }
        return true
    }

    override fun setWorkingShift(text: String) { textWorkingShift.text = text }

    override fun setDate(text: String) { textDate.text = text }

    override fun setUserName(name: String) { textUserName.text  = name }

    override fun setAwaitCount(text: String) { textAwaitCount.text = text }

    override fun setQCRejectedCount(text: String) { textQCDenied.text = text }
    
    override fun setSelectedWorkshop(id: String) {
        val selected = spinnerValues.find { it.id == id }
        val position = spinnerValues.indexOf(selected)
        if (position != -1) {
            workshopSpinner.setSelection(position)
        }
    }

    override fun setWorkshopSpinner(workshopList: List<WorkshopListUnit>) {
        workshopSpinner.visibility = View.VISIBLE
        spinnerValues = workshopList
        workshopSpinner.adapter = WorkshopSpinnerAdapter(this,
                R.layout.common_main_spinner_list_item,
                R.id.itemName,
                workshopList)
    }

    override fun navigateTo(route: FactoryMainView.Route) {
        val intent = when (route) {
            FactoryMainView.Route.WORKERS -> intentFor<FactoryWorkersWorkplaceListActivity>()
            FactoryMainView.Route.QC -> intentFor<FactoryQCStatusActivity>()
            FactoryMainView.Route.PRODUCTION -> intentFor<FactoryProdWorkplaceListActivity>()
            FactoryMainView.Route.SETTINGS -> intentFor<InformationActivity>()
            FactoryMainView.Route.AUTH -> intentFor<FactoryAuthActivity>().newTask()
        }
        startActivity(intent)
    }

    override fun onBackPressed() = showLogoutDialog()

    override fun onDestroy() {
        barcodeHelper.unregister(this)
        super.onDestroy()
    }

    class WorkshopSpinnerAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<WorkshopListUnit>
            ): ArrayAdapter<WorkshopListUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_main_spinner_list_item, parent, false)
            view.itemName.text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_main_spinner_list_item_dropdown, parent, false)
            view.itemName.text = values[position].name
            return view
        }
    }
}

