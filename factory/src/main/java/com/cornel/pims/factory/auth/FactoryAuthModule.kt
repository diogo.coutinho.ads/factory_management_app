package com.cornel.pims.factory.auth

import dagger.Module
import dagger.Provides



@Module
class FactoryAuthModule(private val activity: FactoryAuthView) {

    @FactoryAuthScope
    @Provides
    fun providePresenter(): FactoryAuthPresenter
            = FactoryAuthPresenterImpl(activity)

    @FactoryAuthScope
    @Provides
    fun provideActivity(): FactoryAuthView = activity
}
