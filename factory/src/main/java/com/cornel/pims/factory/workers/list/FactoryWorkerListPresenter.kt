package com.cornel.pims.factory.workers.list

import android.content.Context
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkerListUnit
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.Worker
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */
interface FactoryWorkerListPresenter : BasePresenter {
    fun onCreate(id: String)
    fun onRefresh()
    fun onItemSelected(item: WorkerListUnit)
    fun onBackButtonPressed()
    fun onItemRemove(item: WorkerListUnit)
    fun onAddWorkerBtnPressed()
    fun registerWorker(id: String)
}

class FactoryWorkerListPresenterImpl(
        private val view: FactoryWorkerListView)
    : BasePresenterImpl(view as BaseView), FactoryWorkerListPresenter {
    @Inject
    lateinit var dataManager: DataManager

    private lateinit var item: WorkerListUnit

    @Inject
    lateinit var appContext: Context

    private lateinit var workplaceId: String

    private lateinit var workers: List<Worker>

    override fun onCreate(id: String) {
        view.component.inject(this)
        workplaceId = id
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val worklpace = get(dataManager.getWorkplaceById(workplaceId))!!
            launch(UI) { view.setTitle(worklpace.name) }
            workers = get(dataManager.getWorkersByWorkplaceId(workplaceId))!!

            val workersList = workers.map {
                WorkerListUnit(it.id, it.fullName)
            }

            launch(UI) {
                view.showItems(workersList)
            }
        }
    }

    override fun onItemSelected(item: WorkerListUnit) = view.navigateToWorkerInfo(item.id)

    override fun onBackButtonPressed() = view.finish()

    override fun onItemRemove(item: WorkerListUnit) {

        this.item = item

        smartLaunch {
            runBlocking {
                // TODO check if ok
                val area = get(dataManager.getWorkplaceById(workplaceId))!!

//                val text = appContext.getString(R.string.factory_workers_unregister_confirm, item.name, area.name)
                val text = "Вы уверены, что хотите удалить работника ${item.name} с рабочей зоны ${area.name}?"

//                val btnText = appContext.getString(R.string.factory_workers_register_button)
                val btnText = "Удалить"

                launch(UI) {
                    (view as BaseView).showConfirmation(text,
                            btnText,
                            {unregisterWorker()})

//                    doOnResumeAsync()
                }

            }
        }
    }

    private fun unregisterWorker() {

        smartLaunch(onException = {
            (view as BaseView).isOk(false)
            super.onException(it)
        }, finally = {
            super.finally()
            view.finish()
        }
        ) {
            runBlocking {
                dataManager.unregisterWorker(item.id, workplaceId)
                doOnResumeAsync()
            }
        }
    }

    override fun registerWorker(id: String) {
        if (workers.find { it.id == id } != null) {
            smartLaunch(onException = {
                (view as BaseView).showMessage(appContext.getString(
                        R.string.factory_workers_list_cant_unregister))
            }, finally = {
                super.finally()
                doOnResumeAsync()
            }) {
                runBlocking {
                    dataManager.unregisterWorker(id, workplaceId)
                }
            }
        } else {
            runBlocking {
                if (get(dataManager.getUnassignedWorkerListByWorkplaceId(workplaceId))!!.find { it.id == id } == null) {
                    (view as BaseView).showMessage(appContext.getString(R.string.factory_workers_list_not_found))
                } else {
                    val counts = get(dataManager.getWorkerCountsByWorkplaceId(workplaceId))!!
                    if (counts.max == counts.current) {
                        (view as BaseView).showError(RuntimeException("Не удалось зарегистрировать сотрудника. Зарегистрировано максимально возможное количество сотрудников. Обратитесь к начальнику производства."), false)
                    } else {
                        smartLaunch(onException = {
                            (view as BaseView).showMessage(appContext.getString(
                                    R.string.factory_workers_list_cant_register_here))
                        }, finally = {
                            super.finally()
                            doOnResumeAsync()
                        }) {
                            runBlocking {
                                dataManager.registerWorker(id, workplaceId)
                                doOnResumeAsync()
                            }
                        }
                    }
                }
            }
        }



    }

    override fun onAddWorkerBtnPressed() = view.navigateToAddWorkerView(workplaceId)
}