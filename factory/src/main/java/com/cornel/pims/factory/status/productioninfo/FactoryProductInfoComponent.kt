package com.cornel.pims.factory.status.productioninfo

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryProductInfoActivityScope

@FactoryProductInfoActivityScope
@Subcomponent(modules = [FactoryProductInfoModule::class])
interface FactoryProductInfoComponent {
    fun inject(activity: FactoryProductInfoActivity)
    fun inject(presenter: FactoryProductInfoPresenterImpl)
}

