package com.cornel.pims.qc.productinfo

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.qc.R
import com.cornel.pims.qc.core.AssignmentState
import com.cornel.pims.qc.core.DataManager
import com.cornel.pims.qc.core.Product
import com.cornel.pims.qc.core.QCProductionAssignment
import com.cornel.pims.qc.core.proto.BasePresenter
import com.cornel.pims.qc.core.proto.BasePresenterImpl
import com.cornel.pims.qc.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by AlexGator on 13.03.18.
 */

interface QCProductInfoPresenter : BasePresenter {
    fun onCreate(id: String)
    fun onRefresh()
    fun onBackBtnClick()
    fun onAcceptBtnClick()
    fun onRejectBtnClick()
    fun onReject(cause: String, defect: Boolean)
}

class QCProductInfoPresenterImpl(private val view: QCProductInfoView)
    : BasePresenterImpl(view as BaseView), QCProductInfoPresenter {
    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var assignmentId: String
    private lateinit var assignment: QCProductionAssignment
    private lateinit var product: Product

    override fun onCreate(id: String) {
        view.component.inject(this)
        assignmentId = id
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val assignmentNull = get(dataManager.getProductionAssignmentById(assignmentId))
            if (assignmentNull == null) {
                launch(UI) {
                    view.showError(RuntimeException("Данное производственное задание было удалено."))
                }
            } else {
                assignment = assignmentNull
                product = get(dataManager.getProductById(assignment.productId))
                val stateStr: String
                val deletedStr: String
                val deleted = assignment.deletedAt
                if (deleted != null) {
                    stateStr = "Отменено"
                    deletedStr = deleted
                } else {
                    val stateRes = stateTextMap[assignment.state]
                    stateStr = if (stateRes != null) appContext.getString(stateRes) else assignment.state
                    deletedStr = ""
                }
                launch(UI) {
                    view.apply {
                        showOrHideResponseBtn((deleted == null) && assignment.state == AssignmentState.WAIT_QC.value)
                        setName(product.name)
                        setProductCode(product.code)
                        setCode(assignment.code)
                        setOperationName(assignment.operationName)
                        setLastWorkplace(assignment.lastWorkplaceName)
                        setWorkers(assignment.workers)
                        setState(stateStr)
                        showOrHideDeletedAt(deletedStr)
                        setQuantity(Formatter.format(assignment.quantity))
                    }
                }
            }
        }
    }

    override fun onAcceptBtnClick() {
        val confirmText = appContext.getString(
                R.string.product_info_confirm_accept, product.name, Formatter.format(assignment.quantity)
        )
        val confirmBtnText = appContext.getString(R.string.product_info_confirm_accept_btn)
        (view as BaseView).showConfirmation(confirmText, confirmBtnText, this::onAccept)
    }

    override fun onRejectBtnClick() {
        view.showRejectDialog()
    }

    override fun onReject(cause: String, defect: Boolean) {
        smartLaunch(finally = {
            finally()
            view.finish()
        }) {
            runBlocking {
                if (defect) {
                    // TODO set up splitting_quantity usage
                    dataManager.rejectAssignment(assignment.id, cause, assignment.quantity)
                } else {
                    // TODO set up splitting_quantity usage
                    dataManager.sendAssignmentToRevision(assignment.id, cause, assignment.quantity)
                }
            }
        }
    }

    private fun onAccept() {
        smartLaunch {
            runBlocking {
                // TODO set up splitting_quantity usage
                dataManager.acceptAssignment(assignment.id, assignment.quantity)
                view.finish()
            }
        }
    }

    override fun onBackBtnClick() {
        view.finish()
    }



    companion object {
        val stateTextMap = mapOf(
                AssignmentState.ARRIVED.value to R.string.product_info_state_arrived,
                AssignmentState.IN_PROGRESS.value to R.string.product_info_state_in_progress,
                AssignmentState.PAUSED.value to R.string.product_info_state_paused,
                AssignmentState.WAIT_QC.value to R.string.product_info_state_wait_qc,
                AssignmentState.QC_IN_PROGRESS to R.string.product_info_state_tc_in_progress,
                AssignmentState.REJECTED to R.string.product_info_state_rejected,
                AssignmentState.WAIT_REV.value to R.string.product_info_state_wait_revision,
                AssignmentState.REV_IN_PROGRESS.value to R.string.product_info_state_revision_in_progress,
                AssignmentState.WAIT_ACCEPT.value to R.string.product_info_state_wait_accept,
                AssignmentState.ACCEPT_STORAGE.value to R.string.product_info_accept_storage,
                AssignmentState.UNKNOWN.value to R.string.product_info_state_unknown
        )
    }
}