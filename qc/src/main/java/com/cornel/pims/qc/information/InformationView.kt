package com.cornel.pims.qc.information

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.qc.R
import com.cornel.pims.qc.core.app.app
import com.cornel.pims.qc.core.proto.BaseView
import kotlinx.android.synthetic.main.information_activity.*
import org.jetbrains.anko.toast
import javax.inject.Inject


interface InformationView {
    val component: InformationComponent

    enum class Button { VERSION, UPDATE, TEAM }

    fun showSecret(authUrl: String, apiUrl: String)
    fun showError(e: Throwable)
    fun finish()
}

class InformationActivity : BaseView(), InformationView {

    @Inject
    override lateinit var presenter: InformationPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(InformationModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.information_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        presenter.onCreate()

        addListeners()
    }

    private fun addListeners() {
        btnVersion.setOnClickListener {
            presenter.onButtonPressed(InformationView.Button.VERSION)
        }
        btnUpdate.setOnClickListener {
            presenter.onButtonPressed(InformationView.Button.UPDATE)
        }
        btnTeam.setOnClickListener {
            presenter.onButtonPressed(InformationView.Button.TEAM)
        }
        goBackBtn.setOnClickListener {
            presenter.onBackButton()
        }

        editTextAuthUrl.onFocusChangeListener = FocusListener(presenter::onDoneEditingAuthUrl)
        editTextApiUrl.onFocusChangeListener = FocusListener(presenter::onDoneEditingApiUrl)
    }

    class FocusListener(private val handler: (String) -> Unit) : View.OnFocusChangeListener {

        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            if (!hasFocus)
                handler((v as TextView).text.toString())
        }
    }

    override fun showSecret(authUrl: String, apiUrl: String) {
        editTextAuthUrl.setText(authUrl)
        editTextApiUrl.setText(apiUrl)
        btnUrlAuth.visibility = View.VISIBLE
        btnUrlApi.visibility = View.VISIBLE
        toast("Параметры разработчика активированы")
    }
}
