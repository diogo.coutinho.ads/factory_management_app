package com.cornel.pims.qc.auth

import com.cornel.pims.qc.core.DataManager
import com.cornel.pims.qc.core.proto.BasePresenter
import com.cornel.pims.qc.core.proto.BasePresenterImpl
import com.cornel.pims.qc.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by AlexGator on 14.03.18.
 */

interface QCAuthPresenter : BasePresenter {
    fun onCreate()
    fun onLogin(userLogin: String, userPass: String)

}

class QCAuthPresenterImpl(private val view: QCAuthView)
    : BasePresenterImpl(view as BaseView), QCAuthPresenter {
    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        view.component.inject(this)

        smartLaunch {
            val login = dataManager.getLogin()
            launch(UI) {
                view.setLogin(login)
            }
        }
    }

    override fun doOnResumeAsync() {}

    override fun onLogin(userLogin: String, userPass: String) {
        smartLaunch {
            runBlocking {
                dataManager.setLogin(userLogin)
                dataManager.connect(userLogin, userPass)
                dataManager.getUser()
                launch(UI) {
                    val check = dataManager.getAppPermission()
                    launch(UI) {
                        if (check) {
                            view.navigateToMainActivity()
                        } else {
                            view.showError(RuntimeException("Вы не имеете права работать в данном приложении. Обратитесь к начальнику производства"), false)
                        }
                    }
                }
            }
        }
    }

}