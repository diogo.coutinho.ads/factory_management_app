package com.cornel.pims.qc.main

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 13.03.18.
 */

@Module
class QCMainModule(private val activity: QCMainActivity) {
    @QCMainScope
    @Provides
    fun providePresenter(): QCMainPresenter = QCMainPresenterImpl(activity)
}