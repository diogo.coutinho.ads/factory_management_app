package com.cornel.pims.qc.auth

import android.os.Bundle
import android.os.Handler
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.qc.R
import com.cornel.pims.qc.core.app.app
import com.cornel.pims.qc.core.proto.BaseView
import com.cornel.pims.qc.main.QCMainActivity
import kotlinx.android.synthetic.main.qc_auth_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import javax.inject.Inject

interface QCAuthView {

    val component: QCAuthComponent

    fun setLogin(text: String)
    fun showError(e: Throwable, finishActivity: Boolean)
    fun finish()
    fun navigateToMainActivity()
}

class QCAuthActivity : BaseView(), QCAuthView {

    companion object {
        const val EXTRA_TOKEN_FLAG = "Token flag"
        const val TOKEN_TOAST_DELAY = 250L
    }

    override val component by lazy { app.component.plus(QCAuthModule(this))}

    @Inject
    override lateinit var presenter: QCAuthPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.qc_auth_activity)
        component.inject(this)

        if (intent.getBooleanExtra(EXTRA_TOKEN_FLAG, false)) {
            Handler().postDelayed({
                toast("Время сеанса закончено. Необходима авторизация.")
            }, TOKEN_TOAST_DELAY)
        }

        presenter.onCreate()

        addListeners()
//        presenter.onLogin("admin@example.com", "password")
    }


    private fun addListeners() {
        enter.onClick {
            val userLogin = login.text.toString()
            val userPass = password.text.toString()
            presenter.onLogin(userLogin, userPass)
        }
    }

    override fun navigateToMainActivity() {
        startActivity(intentFor<QCMainActivity>())
    }

    override fun setLogin(text: String) = login.setText(text)
}

