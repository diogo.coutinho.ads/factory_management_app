package com.cornel.pims.qc.information

import android.content.Context
import android.widget.Toast
import com.cornel.pims.qc.core.DataManager
import com.cornel.pims.qc.core.proto.BasePresenter
import com.cornel.pims.qc.core.proto.BasePresenterImpl
import com.cornel.pims.qc.core.proto.BaseView
import javax.inject.Inject


interface InformationPresenter : BasePresenter {
    fun onCreate()
    fun onButtonPressed(btn: InformationView.Button)
    fun onDoneEditingAuthUrl(url: String)
    fun onDoneEditingApiUrl(url: String)
    fun onBackButton()
}

class InformationPresenterImpl(val view: InformationView, private val context: Context)
    : BasePresenterImpl(view as BaseView), InformationPresenter {

    companion object {
        private const val SECRET_TOUCH_COUNT = 7
        private const val SECRET_TOUCH_DELAY = 500L
    }

    private var count = 0
    private var lastTime = System.currentTimeMillis()
    private var secretShown = false

    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        view.component.inject(this)
    }

    override fun doOnResumeAsync() {}

    override fun onButtonPressed(btn: InformationView.Button) {
        info("Clicked: $btn")
        when (btn) {
            InformationView.Button.VERSION -> onSecretMenuClick()
            InformationView.Button.UPDATE -> context.showToast("Ваши данные являются актуальными, обновление не требуется.")
            InformationView.Button.TEAM -> context.showToast("Сделано командой TOCTEAM.")
        }
    }

    fun Context.showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    private fun onSecretMenuClick() {
        val newTime = System.currentTimeMillis()
        val delay = newTime - lastTime
        lastTime = newTime

        if (delay > SECRET_TOUCH_DELAY) {
            count = 1
        } else {
            count++
            if (count > SECRET_TOUCH_COUNT) {
                secretShown = true
                view.showSecret(dataManager.getAuthUrl(), dataManager.getApiUrl())
            }
        }
    }

    override fun onDoneEditingAuthUrl(url: String) = dataManager.setAuthUrl(url)

    override fun onDoneEditingApiUrl(url: String) = dataManager.setApiUrl(url)

    override fun onBackButton() {
        view.finish()
    }

}

