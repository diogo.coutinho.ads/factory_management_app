package com.cornel.pims.qc.main

import android.content.Context
import com.cornel.pims.qc.R
import com.cornel.pims.qc.common.QCListUnit
import com.cornel.pims.qc.common.WorkshopListUnit
import com.cornel.pims.qc.core.DataManager
import com.cornel.pims.qc.core.QError
import com.cornel.pims.qc.core.QResponse
import com.cornel.pims.qc.core.proto.BasePresenter
import com.cornel.pims.qc.core.proto.BasePresenterImpl
import com.cornel.pims.qc.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 13.03.18.
 */
interface QCMainPresenter : BasePresenter {
    fun onCreate()
    fun onSettingsClick()
    fun onLogoutClick()
    fun onWorkshopChange(selected: WorkshopListUnit)
    fun onRefresh()
    fun onQCItemSelected(id: String)
}

class QCMainPresenterImpl(private val view: QCMainView)
    : BasePresenterImpl(view as BaseView), QCMainPresenter {
    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    override fun onCreate() {
        view.component.inject(this)
    }

    override fun doOnResumeAsync() {
        runBlocking {
            val userName = get(dataManager.getUser()).name
            val workshopList = get(dataManager.getWorkshopList()).map {
                WorkshopListUnit(it.id, it.name)
            }

            val currentWorkshop = try {
                get(dataManager.getWorkshopById(dataManager.getCurrentWorkshopId())).id
            } catch (e: RuntimeException) {
                // Workshop hasn't been selected yet, or didn't found. Ignore it.
                workshopList[0].id
            }

            val workingShift = get(dataManager.getWorkingShift(currentWorkshop))
            val workingShiftText = if (workingShift != -1)
                appContext.getString(R.string.working_shift, workingShift)
            else
                appContext.getString(R.string.working_shift_off)

            val date = SimpleDateFormat("d MMM", Locale("ru")).format(Date())
            launch(UI) {
                view.apply {
                    setWorkingShift(workingShiftText)
                    setDate(date)
                    setWorkshopSpinner(workshopList)
                    setUserName(userName)
                    setSelectedWorkshop(currentWorkshop)
                }
            }
            loadItemsAwaitQC()
        }
    }

    private fun loadItemsAwaitQC() {
        smartLaunch {
            runBlocking {
                val workshop = try {
                    get(dataManager.getWorkshopById(dataManager.getCurrentWorkshopId()))
                } catch (e: RuntimeException) {
                    // Workshop hasn't been selected yet, or didn't found. Ignore it.
                    return@runBlocking
                }
                val assignments = get(dataManager.getProductionAwaitQCByWorkshopId(workshop.id))

                val awaitQCText = appContext.getString(R.string.main_await_qc)

                val qcItems = assignments.map {
                                val product = get(dataManager.getProductById(it.productId))
                                val workplaceText = appContext
                                        .getString(R.string.main_workplace, it.lastWorkplaceName)
                                QCListUnit(
                                        it.id,
                                        it.code,
                                        product.name,
                                        workplaceText,
                                        awaitQCText,
                                        //TODO real shipping date
                                        "")
                            }
                launch(UI) {
                    view.showQCItems(qcItems)
                }
            }

        }
    }

    override fun onRefresh() {
        loadItemsAwaitQC()
    }

    override fun onQCItemSelected(id: String) {
        view.navigateToProductInfo(id)
    }

    override fun onWorkshopChange(selected: WorkshopListUnit) {
        dataManager.setCurrentWorkshopId(selected.id)
        runBlocking {
            val workingShift = get(dataManager.getWorkingShift(selected.id))
            val workingShiftText = if (workingShift != -1)
                appContext.getString(R.string.working_shift, workingShift)
            else
                appContext.getString(R.string.working_shift_off)
            launch(UI) {
                view.setWorkingShift(workingShiftText)
            }
        }
        loadItemsAwaitQC()
    }

    override fun onSettingsClick() {
        view.navigateTo(QCMainView.Route.SETTINGS)
    }

    override fun onLogoutClick() {
        view.finishAndRemoveTask()
    }
}