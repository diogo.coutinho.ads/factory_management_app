package com.cornel.pims.qc.core.helper

import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.exception.ApolloNetworkException
import com.cornel.pims.core.api.GraphQLAPI
import com.cornel.pims.qc.core.*
import com.cornel.pims.qc.core.app.App
import com.cornel.pims.qc.mutation.AcceptTechnicalControlMutation
import com.cornel.pims.qc.mutation.RejectTechnicalControlMutation
import com.cornel.pims.qc.mutation.SendToRevisionMutation
import com.cornel.pims.qc.query.*
import kotlinx.coroutines.experimental.CompletableDeferred
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.delay
import okhttp3.HttpUrl
import java.net.ConnectException
import javax.inject.Inject

interface APIHelper {
    fun connect(email: String, password: String)

    /* Queries */
    suspend fun getCurrentUser(): QResponse<User>
    suspend fun getAppPermission(): Boolean
    suspend fun getCurrentShift(workshopId: String): QResponse<Int>

    suspend fun getProductList(): QResponse<List<Product>>
    suspend fun getWorkshopsList(): QResponse<List<Workshop>>
    suspend fun getWorkplacesByWorkshopId(id: String): QResponse<List<Workplace>>
    suspend fun getWorkplaceById(id: String): QResponse<Workplace>

    suspend fun getProductionAwaitQCByWorkshopId(id: String): QResponse<List<QCProductionAssignment>>
    suspend fun getProductionAssignmentById(id: String): QResponse<QCProductionAssignment?>

    /* Mutations */
    suspend fun rejectTechnicalControl(id: String, cause: String, splittingQuantity: Double)
    suspend fun sendToRevision(id: String, cause: String, splittingQuantity: Double)
    suspend fun acceptTechnicalControl(id: String, splittingQuantity: Double)

}

class APIHelperImpl(app: App) : APIHelper {

    @Inject
    lateinit var sharedPrefHelper: SharedPrefHelper

    init{
        app.component.inject(this)
    }

    companion object {
        const val LOG_TAG = "API_HELPER"
        const val RETRY_COUNT = 5
        const val RETRY_DELAY = 3000

        const val AUTH_ERROR = "Not authorized"
        const val TOKEN_ERROR = "Token"
    }

    private val authURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1337)
            .addPathSegment("oauth")
            .addPathSegment("token")
            .build()!!

    private val apiURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1338)
            .addPathSegment("graphql")
            .build()!!

    private val wsURL = "ws://11.11.11.11:1338/cable"

    private lateinit var graphQLAPI: GraphQLAPI

    init {
        Log.d(LOG_TAG, "New APIHelper object created")
    }

    /**
     * Special function to rebuild private [GraphQLAPI] class.
     */
    private fun rebuildClient() {
        var i = 0
        // Retrying only on ConnectException
        while (i < RETRY_COUNT) {
            try {
                val email = sharedPrefHelper.getLogin()
                val password = sharedPrefHelper.getSecret()
                graphQLAPI = GraphQLAPI(apiURL, authURL, wsURL, email, password)
                Log.d(LOG_TAG, "Client rebuild")
                return
            } catch (e: ConnectException) {
                Log.e(LOG_TAG, "API client build failed", e)
                if (++i == RETRY_COUNT) throw e
            }
        }
    }

    /**
     * Special generic function which makes [RETRY_COUNT] retries
     * if error takes place while connecting.
     */
    private suspend fun <T> proceedQuery(requestFun: () -> Deferred<T>): T {
        var exception = Throwable()
        for (i in 0..RETRY_COUNT) {
            try {
                if (!this::graphQLAPI.isInitialized) {
                    rebuildClient()
                }
                return requestFun().await()
            } catch (e: ConnectException) {
                exception = e
                Log.e(LOG_TAG, "Request to Auth server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            } catch (e: ApolloNetworkException) {
                // Handles every exception during API server call, and invalid token too
                exception = e
                Log.e(LOG_TAG, "Request to API server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            }
        }

        throw exception
    }

    private fun <R, D> response(response: Response<R>, data: D?): QResponse<D> {
        return if (response.errors().isEmpty())
            QResponse(data, null)
        else
            QResponse(null, when {
                (response.errors().find { it.message()!!.startsWith(AUTH_ERROR) } != null) -> QError.AUTH
                (response.errors().find { it.message()!!.startsWith(TOKEN_ERROR) } != null) -> QError.TOKEN
                else -> QError.SERVER
            })
    }

    /*##################################################################################*/
    /*######################## INTERFACE IMPLEMENTATION ################################*/
    /*##################################################################################*/

    /* Authentication */
    override fun connect(email: String, password: String) {
        sharedPrefHelper.setLogin(email)
        sharedPrefHelper.setSecret(password)
        rebuildClient()
    }


    /* Common */
    override suspend fun getCurrentUser() = proceedQuery { getCurrentUserDefer() }

    private fun getCurrentUserDefer(): Deferred<QResponse<User>> {
        val deferredUser = CompletableDeferred<QResponse<User>>()

        graphQLAPI.client.query(GetCurrentUserQuery())
                .enqueue(object : ApolloCall.Callback<GetCurrentUserQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get current_user object", e)
                        deferredUser.cancel(e)
                    }

                    override fun onResponse(response: Response<GetCurrentUserQuery.Data>) {
                        Log.i(LOG_TAG, "Current user query proceeded successfully")
                        Log.i(LOG_TAG, "Current user ID: ${response.data()?.current_user()?.id()}")
                        deferredUser.complete(
                                response(response, if (!response.errors().isEmpty()) null else
                                    User(response.data()!!.current_user()!!.id(),
                                        response.data()!!.current_user()!!.full_name()))
                        )
                    }
                })

        return deferredUser
    }

    override suspend fun getAppPermission() = proceedQuery { getAppPermissionDef() }

    private fun getAppPermissionDef() : Deferred<Boolean> {
        val deferredPermission = CompletableDeferred<Boolean>()

        graphQLAPI.client.query(GetAppPermissionQuery())
                .enqueue(object : ApolloCall.Callback<GetAppPermissionQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get permission object", e)
                        deferredPermission.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAppPermissionQuery.Data>) {
                        Log.i(LOG_TAG, "Permission query proceeded successfully")
                        val permission = response.data()?.current_user()?.permissions()?.find{ it.action() == "access" && it.secured_object() == "technical_control_app"}
                        if (permission == null) {
                            Log.i(LOG_TAG, "Permission to app was denied")
                        } else {
                            Log.i(LOG_TAG, "Permission to app was accepted")
                        }
                        deferredPermission.complete(permission != null)
                    }
                })

        return deferredPermission
    }

    override suspend fun getCurrentShift(workshopId: String) = proceedQuery { getCurrentShiftDefer(workshopId) }

    private fun getCurrentShiftDefer(workshopId: String) : Deferred<QResponse<Int>> {
        val deferredShift = CompletableDeferred<QResponse<Int>>()

        graphQLAPI.client.query(GetCurrentShiftQuery(workshopId))
                .enqueue(object : ApolloCall.Callback<GetCurrentShiftQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get current_shift object", e)
                        deferredShift.cancel(e)
                    }

                    override fun onResponse(response: Response<GetCurrentShiftQuery.Data>) {
                        Log.i(LOG_TAG, "Current shift query proceeded successfully")
                        val number = response.data()?.current_shift()?.number()?.toInt()
                        if (number == null) {
                            Log.i(LOG_TAG, "Current shift was null")
                        } else {
                            Log.i(LOG_TAG, "Current shift: ${number}")
                        }
                        deferredShift.complete(
                                response(response, if (!response.errors().isEmpty()) null else
                                    number ?: 1))
                    }
                })

        return deferredShift
    }

    override suspend fun getProductList()
            = proceedQuery(this::getProductListDefer)

    private fun getProductListDefer(): Deferred<QResponse<List<Product>>> {
        val deferredProductList = CompletableDeferred<QResponse<List<Product>>>()

        graphQLAPI.client.query(GetProductListQuery())
                .enqueue(object : ApolloCall.Callback<GetProductListQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Products list", e)
                        deferredProductList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetProductListQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()}")
                        deferredProductList.complete(
                                response(response, if (!response.errors().isEmpty()) null else
                                    response.data()!!.products()!!.map {
                            Product(it.id(), it.name(), it.code(), it.unit()!!.abbreviation())
                        }))
                    }
                })

        return deferredProductList
    }


    override suspend fun getWorkshopsList(): QResponse<List<Workshop>>
            = proceedQuery(this::getWorkshopsListDefer)

    private fun getWorkshopsListDefer(): Deferred<QResponse<List<Workshop>>> {
        val deferredWorkshopsList = CompletableDeferred<QResponse<List<Workshop>>>()

        graphQLAPI.client.query(GetWorkshopsListQuery())
                .enqueue(object : ApolloCall.Callback<GetWorkshopsListQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Workshops list", e)
                        deferredWorkshopsList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkshopsListQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.workshops()}")
                        deferredWorkshopsList.complete(
                                response(response, if (!response.errors().isEmpty()) null else
                                    response.data()!!.workshops()!!.map {
                            Workshop(it.id(), it.code(), it.name())
                        }))
                    }
                })

        return deferredWorkshopsList
    }

    override suspend fun getWorkplacesByWorkshopId(id: String)
            = proceedQuery { getWorkplacesByWorkshopIdDefer(id) }

    private fun getWorkplacesByWorkshopIdDefer(id: String): Deferred<QResponse<List<Workplace>>> {
        val deferredWorkplaces = CompletableDeferred<QResponse<List<Workplace>>>()

        graphQLAPI.client.query(GetWorkplacesByWorkshopIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetWorkplacesByWorkshopIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Workplaces list", e)
                        deferredWorkplaces.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkplacesByWorkshopIdQuery.Data>) {
                        Log.i(LOG_TAG, "Workplaces queried successfully")

                        deferredWorkplaces.complete(
                                response(response, if (!response.errors().isEmpty()) null else
                                    response.data()!!.workplaces()!!.map {
                            Workplace(it.id(), it.code(), it.name(), it.current_technical_control_assignments_count()!!.toInt())
                        }))
                    }
                })

        return deferredWorkplaces
    }


    override suspend fun getWorkplaceById(id: String)
            = proceedQuery { getWorkplaceByIdDefer(id) }

    private fun getWorkplaceByIdDefer(id: String): Deferred<QResponse<Workplace>> {
        val deferredWorkspace = CompletableDeferred<QResponse<Workplace>>()

        graphQLAPI.client.query(GetWorkplaceByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetWorkplaceByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Workplace", e)
                        deferredWorkspace.cancel(e)
                    }

                    override fun onResponse(response: Response<GetWorkplaceByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Workplace queried successfully")
                        Log.i(LOG_TAG, "Queried Workspace ID: ${response.data()?.workplace()?.id()}")

                        deferredWorkspace.complete(
                                response(response,
                                        if (!response.errors().isEmpty()) null else {
                                            val workplace = response.data()!!.workplace()!!
                                            val producedQuantity = response.data()!!.workplace()!!.current_technical_control_assignments_count()!!.toInt()

                                            Workplace(workplace.id(), workplace.code(), workplace.name(), producedQuantity)
                                        }
                        ))
                    }
                })

        return deferredWorkspace
    }


    override suspend fun getProductionAwaitQCByWorkshopId(id: String): QResponse<List<QCProductionAssignment>> {
        return proceedQuery { getProductionAwaitQcByWorkshopIdDefer(id) }
    }

    private fun getProductionAwaitQcByWorkshopIdDefer(id: String)
            : Deferred<QResponse<List<QCProductionAssignment>>> {
        val deferredProductionAwaitQC = CompletableDeferred<QResponse<List<QCProductionAssignment>>>()

        graphQLAPI.client.query(GetProductionAwaitQCByWorkshopIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetProductionAwaitQCByWorkshopIdQuery.Data>() {

                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Factory Operations await QC by Workshop ID", e)
                        deferredProductionAwaitQC.cancel(e)
                    }
                    override fun onResponse(response: Response<GetProductionAwaitQCByWorkshopIdQuery.Data>) {
                        Log.i(LOG_TAG, "Factory Operations await QC by Workshop ID queried successfully")
                        deferredProductionAwaitQC.complete(
                                response(response, if (!response.errors().isEmpty()) null else {

                                    val listProductionAssignments
                                        = mutableListOf<QCProductionAssignment>()

                                    val workplaces = response.data()!!.workshop()!!.workplaces()!!

                                    workplaces.forEach {
                                        val workplace = it;
                                        workplace.current_technical_control_assignments()!!.forEach{
                                            listProductionAssignments.add(
                                                    QCProductionAssignment(
                                                            it.id(),
                                                            it.code(),
                                                            "",
                                                            null,
                                                            it.quantity(),
                                                            it.route_map().product().id(),
                                                            "",
                                                            workplace.name(),
                                                            emptyList(),
                                                            "",
                                                            "",
                                                            "",
                                                            it.route_map().product().unit()!!.abbreviation()))
                                        }
                                    }

                                    listProductionAssignments
                                }))
                    }
                })


        return deferredProductionAwaitQC
    }


    override suspend fun getProductionAssignmentById(id: String)
            = proceedQuery { getProductionAssignmentByTaskIdDefer(id) }

    private fun getProductionAssignmentByTaskIdDefer(id: String): Deferred<QResponse<QCProductionAssignment?>> {
        val deferredProductionAssignment = CompletableDeferred<QResponse<QCProductionAssignment?>>()

        graphQLAPI.client.query(GetProductionAssignmentByIdQuery(id))
                .enqueue(object: ApolloCall.Callback<GetProductionAssignmentByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Production Assignment by ID", e)
                        deferredProductionAssignment.cancel(e)
                    }

                    override fun onResponse(response: Response<GetProductionAssignmentByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Product Assignment by ID queried successfully")
                        val assignment = response.data()?.production_assignment()
                        deferredProductionAssignment.complete(
                                response(response, if (!response.errors().isEmpty()) null else
                                    if (assignment == null)
                                        null
                                    else
                                        QCProductionAssignment(
                                                assignment.id(),
                                                assignment.code(),
                                                assignment.state(),
                                                assignment.deleted_at(),
                                                assignment.quantity(),
                                                assignment.route_map().product().id(),
                                                assignment.current_operation()?.name() ?: "",
                                                assignment.current_workplace()?.name() ?: "",
                                                emptyList(),
                                                "",
                                                "",
                                                "",
                                                assignment.route_map().product().unit()!!.abbreviation()
                                        )))
                    }
                })

        return deferredProductionAssignment
    }


    override suspend fun acceptTechnicalControl(id: String, splittingQuantity: Double) {
        proceedQuery { acceptTechnicalControlDefer(id, splittingQuantity) }
    }

    private fun acceptTechnicalControlDefer(id: String, splittingQuantity: Double) : Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(AcceptTechnicalControlMutation(id, sQuantity))
                .enqueue(object: ApolloCall.Callback<AcceptTechnicalControlMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to accept Production Assignment with ID: $id", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<AcceptTechnicalControlMutation.Data>) {
                        Log.e(LOG_TAG, "Production Assignment with ID: $id accepted successfully")
                        deferredStatus.complete("Success")
                    }

                })

        return deferredStatus
    }

    override suspend fun rejectTechnicalControl(id: String, cause: String, splittingQuantity: Double) {
        proceedQuery { rejectTechnicalControlDefer(id, cause, splittingQuantity) }
    }

    private fun rejectTechnicalControlDefer(id: String, reason: String, splittingQuantity: Double): Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val reasonInput = Input.fromNullable(reason)

        val sQuantity = Input.fromNullable(splittingQuantity)

        graphQLAPI.client.mutate(RejectTechnicalControlMutation(id, reasonInput, sQuantity))
                .enqueue(object: ApolloCall.Callback<RejectTechnicalControlMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to reject Production Assignment with ID: $id", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<RejectTechnicalControlMutation.Data>) {
                        Log.i(LOG_TAG, "Production Assignment with ID: $id rejected successfully")
                        deferredStatus.complete("Success")
                    }

                })

        return deferredStatus
    }


    override suspend fun sendToRevision(id: String, cause: String, splittingQuantity: Double) {
        proceedQuery { sendToRevisionDefer(id, cause, splittingQuantity) }
    }

    private fun sendToRevisionDefer(id: String, reason: String, splittingQuantity: Double): Deferred<String> {
        val deferredStatus = CompletableDeferred<String>()

        val sQuantity = Input.fromNullable(splittingQuantity)

        val reasonInput = Input.fromNullable(reason)
        graphQLAPI.client.mutate(SendToRevisionMutation(id, reasonInput, sQuantity))
                .enqueue(object: ApolloCall.Callback<SendToRevisionMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to send Production Assignment with ID: $id to revision.", e)
                        deferredStatus.cancel(e)
                    }

                    override fun onResponse(response: Response<SendToRevisionMutation.Data>) {
                        Log.e(LOG_TAG, "Production Assignment with ID: $id sent to revision successfully")
                        deferredStatus.complete("Success")
                    }
                })

        return deferredStatus
    }

}
