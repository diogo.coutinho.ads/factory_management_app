package com.cornel.pims.qc.main

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.qc.R
import com.cornel.pims.qc.common.QCListUnit
import com.cornel.pims.qc.common.WorkshopListUnit
import com.cornel.pims.qc.core.app.app
import com.cornel.pims.qc.core.proto.BaseView
import com.cornel.pims.qc.information.InformationActivity
import com.cornel.pims.qc.productinfo.QCProductInfoActivity
import kotlinx.android.synthetic.main.common_main_spinner_list_item_dropdown.view.*
import kotlinx.android.synthetic.main.common_qc_status_list_item.view.*
import kotlinx.android.synthetic.main.qc_main_activity.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

/**
 * Created by AlexGator on 07.03.18.
 */

interface QCMainView {
    // The action buttons which may be clicked by user in the current view
    enum class Route { SETTINGS }

    val component: QCMainComponent

    fun showError(e: Throwable)
    fun finish()
    fun hidePreloader()
    fun setWorkingShift(text: String)
    fun setDate(text: String)
    fun setUserName(name: String)
    fun setWorkshopSpinner(workshopList: List<WorkshopListUnit>)
    fun setSelectedWorkshop(id: String)
    fun navigateTo(route: Route)
    fun finishAndRemoveTask()
    fun showQCItems(items: List<QCListUnit>)
    fun navigateToProductInfo(id: String)
}

class QCMainActivity : BaseView(), QCMainView {

    companion object {
        const val EXTRA_WORKSHOP_ID = "workshop_id"
    }

    override val component by lazy { app.component.plus(QCMainModule(this))}

    @Inject
    override lateinit var presenter: QCMainPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    private lateinit var spinnerValues: List<WorkshopListUnit>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.qc_main_activity)
        setSupportActionBar(toolbar)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        component.inject(this)

        barcodeHelper.register(this)

        val workShopId = intent.getStringExtra(EXTRA_WORKSHOP_ID)
        workShopId?.let {
            setSelectedWorkshop(workShopId)
        }
        presenter.onCreate()

        addListeners()
    }

    override fun onDestroy() {
        barcodeHelper.unregister(this)
        super.onDestroy()
    }

    private fun addListeners() {
        recyclerContainer.setOnRefreshListener {
            recyclerContainer.isRefreshing = false;
            presenter.onRefresh()
        }
        workshopSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?,
                                        position: Int, rowId: Long) {
                val selected = workshopSpinner.selectedItem as WorkshopListUnit
                presenter.onWorkshopChange(selected)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_activity_appbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_settings -> presenter.onSettingsClick()
            R.id.action_logout -> presenter.onLogoutClick()
        }
        return true
    }

    override fun setSelectedWorkshop(id: String) {
        val selected = spinnerValues.find { it.id == id }
        val position = spinnerValues.indexOf(selected)
        if (position != -1) {
            workshopSpinner.setSelection(position)
        }
    }

    override fun setWorkshopSpinner(workshopList: List<WorkshopListUnit>) {
        workshopSpinner.visibility = View.VISIBLE
        spinnerValues = workshopList
        workshopSpinner.adapter = WorkshopSpinnerAdapter(this,
                R.layout.common_main_spinner_list_item,
                R.id.itemName,
                workshopList)
    }

    override fun setWorkingShift(text: String) { textWorkingShift.text = text }

    override fun setDate(text: String) { textDate.text = text }

    override fun setUserName(name: String) {
        iconUser.visibility = View.VISIBLE
        textUserName.text  = name
    }

    override fun onBackPressed() = showLogoutDialog()

    override fun navigateTo(route: QCMainView.Route) {
        val intent = when (route) {
            QCMainView.Route.SETTINGS -> intentFor<InformationActivity>()
        }
        startActivity(intent)
    }

    override fun navigateToProductInfo(id: String) {
        startActivity(intentFor<QCProductInfoActivity>(
                QCProductInfoActivity.EXTRA_PRODUCT_ID to id
        ))
    }

    override fun showQCItems(items: List<QCListUnit>) {
        divider.visibility = View.VISIBLE
        recycler.swapAdapter(RecyclerAdapter(items, this::onRecyclerItemSelected), false)
    }

    private fun onRecyclerItemSelected(item: QCListUnit) {
        presenter.onQCItemSelected(item.id)
    }

    class WorkshopSpinnerAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<WorkshopListUnit>
    ): ArrayAdapter<WorkshopListUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_main_spinner_list_item, parent, false)
            view.itemName.text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_main_spinner_list_item_dropdown, parent, false)
            view.itemName.text = values[position].name
            return view
        }
    }

    class RecyclerAdapter(private val data: List<QCListUnit>,
                          private val listener: (QCListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init { setHasStableIds(true) }

        class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view                    = v
            val code: TextView          = v.qcStatusItemCode
            val name: TextView          = v.qcStatusItemName
            val status: TextView        = v.qcStatusItemStatus
            val workplace: TextView     = v.qcStatusItemWorkplace
            val shippingDate: TextView  = v.qcStatusItemShippingDate

            fun bind(item: QCListUnit, listener: (QCListUnit) -> Unit) {
                code.text           = item.code
                name.text           = item.name
                workplace.text      = item.workplace
                shippingDate.text   = item.shippingDate
                status.text         = item.status
                view.setOnClickListener { listener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_qc_status_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}