package com.cornel.pims.qc.auth

import dagger.Subcomponent
import javax.inject.Scope

@Scope annotation class QCAuthScope
@QCAuthScope
@Subcomponent(modules = [QCAuthModule::class])
interface QCAuthComponent {
    fun inject(activity: QCAuthActivity)
    fun inject(presenter: QCAuthPresenterImpl)
}