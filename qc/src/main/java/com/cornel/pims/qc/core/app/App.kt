package com.cornel.pims.qc.core.app

import android.app.Activity
import android.app.Application
import com.cornel.pims.qc.core.DataManager
import javax.inject.Inject


/**
 * Created by AlexGator on 03.02.18.
 */

val Activity.app: App
    get() = application as App

inline fun <reified T> cast(from: Any): T? = from as? T

class App : Application() {

    /*
     * The code won't be executed until
     * [component.inject(this)]
     * is done, so that by that time [this] already exist.
     * */
    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }

}
