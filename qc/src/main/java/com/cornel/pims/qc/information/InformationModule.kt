package com.cornel.pims.qc.information

import android.content.Context
import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 04.02.18.
 */

@Module
class InformationModule(private val view: InformationView) {

    @InformationScope
    @Provides
    fun providePresenter(context: Context): InformationPresenter = InformationPresenterImpl(view, context)

    @InformationScope
    @Provides
    fun provideActivity(): InformationView = view
}
