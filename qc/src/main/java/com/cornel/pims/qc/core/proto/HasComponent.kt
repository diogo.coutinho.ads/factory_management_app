package com.cornel.pims.qc.core.proto

/**
 * Created by AlexGator on 02.04.18.
 */

interface HasComponent<out Component> {
    val component: Component
}