package com.cornel.pims.qc.core.app

import android.app.Application
import com.cornel.pims.qc.auth.QCAuthComponent
import com.cornel.pims.qc.auth.QCAuthModule
import com.cornel.pims.qc.core.DataManagerImpl
import com.cornel.pims.qc.core.helper.APIHelperImpl
import com.cornel.pims.qc.core.helper.SharedPrefHelperImpl
import com.cornel.pims.qc.main.QCMainComponent
import com.cornel.pims.qc.main.QCMainModule
import com.cornel.pims.qc.information.InformationComponent
import com.cornel.pims.qc.information.InformationModule
import com.cornel.pims.qc.productinfo.QCProductInfoComponent
import com.cornel.pims.qc.productinfo.QCProductInfoModule
import dagger.Component
import javax.inject.Singleton


/**
 * Created by AlexGator on 03.02.18.
 */

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: Application)
    fun inject(dataManager: DataManagerImpl)
    fun inject(sharedPrefHelper: SharedPrefHelperImpl)
    fun inject(apiHelper: APIHelperImpl)

    fun plus(module: InformationModule): InformationComponent
    fun plus(module: QCAuthModule): QCAuthComponent
    fun plus(module: QCMainModule): QCMainComponent
    fun plus(module: QCProductInfoModule): QCProductInfoComponent
}
