package com.cornel.pims.qc.productinfo

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.qc.R
import com.cornel.pims.qc.core.app.app
import com.cornel.pims.qc.core.proto.BaseView
import kotlinx.android.synthetic.main.common_worker_list_item.view.*
import kotlinx.android.synthetic.main.qc_product_info_activity.*
import kotlinx.android.synthetic.main.qc_product_reject_dialog.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by AlexGator on 14.02.18.
 */

interface QCProductInfoView {
    val component: QCProductInfoComponent

    fun showError(e: Throwable)
    fun finish()
    fun setName(text: String)
    fun setCode(text: String)
//    fun setPartner(text: String)
//    fun setShippingDate(text: String)
    fun setQuantity(text: String)
//    fun setManager(text: String)
//    fun setAdditionalInfo(text: String)
//    fun setComment(text: String)
    fun setState(text: String)
    fun showOrHideDeletedAt(text: String)
    fun setProductCode(text: String)
    fun setOperationName(text: String)
    fun setLastWorkplace(text: String)
    fun setWorkers(list: List<String>)
    fun showOrHideResponseBtn(isVisible: Boolean)
    fun showRejectDialog()
}

class QCProductInfoActivity : BaseView(), QCProductInfoView {

    companion object {
        const val EXTRA_PRODUCT_ID = "product_id"
    }

    @Inject
    override lateinit var presenter: QCProductInfoPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper


    override val component by lazy {app.component.plus(QCProductInfoModule(this))}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.qc_product_info_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        component.inject(this)

        val id = intent.getStringExtra(EXTRA_PRODUCT_ID)

        presenter.onCreate(id)

        addListeners()
    }

    override fun onResume() {
        showOrHideResponseBtn(false)
        super.onResume()
    }

    override fun showRejectDialog() {
        val dialog = RejectDialog()
        dialog.callback = presenter::onReject
        dialog.show(supportFragmentManager, "rejectDialog")
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        product_info_btn_accept.onClick {
            presenter.onAcceptBtnClick()
        }

        product_info_btn_reject.onClick {
            presenter.onRejectBtnClick()
        }

        product_info_btn_back.onClick {
            presenter.onBackBtnClick()
        }
    }

    override fun setName(text: String) {
        supportActionBar?.setTitle(text)
    }

    override fun setQuantity(text: String) = product_info_quantity.setText(text)

    override fun setCode(text: String) = product_info_code.setText(text)

    override fun setOperationName(text: String) = product_info_operation.setText(text)

    override fun setLastWorkplace(text: String) = setOrHide(text, product_info_last_workplace, product_info_last_workplace_container)

    override fun setProductCode(text: String) = product_info_sku.setText(text)

    override fun setState(text: String) = product_info_state.setText(text)

    override fun showOrHideDeletedAt(text: String) = setOrHide(text, product_info_deleted_at, product_info_deleted_at_container)

    override fun setWorkers(list: List<String>) {
        workersContainer.removeAllViews()
        list.forEach{
            val view = layoutInflater
                    .inflate(R.layout.common_worker_list_item, workersContainer, false)
            view.itemName.text = it
            workersContainer.addView(view)
        }
    }

    override fun showOrHideResponseBtn(isVisible: Boolean) {
        val visibility = if (isVisible) View.VISIBLE else View.GONE
        product_info_btn_accept.visibility = visibility
        product_info_btn_reject.visibility = visibility
    }

//    override fun setPartner(text: String) = product_info_partner.setText(text)
//
//    override fun setShippingDate(text: String) = product_info_shipping_date.setText(text)
//
//    override fun setManager(text: String) = product_info_manager.setText(text)
//
//    override fun setAdditionalInfo(text: String) = product_info_additional_info.setText(text)

//    override fun setComment(text: String) = product_info_comment.setText(text)

    class RejectDialog: DialogFragment() {

        lateinit var callback: (String, Boolean) -> Unit

        override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater?.inflate(R.layout.qc_product_reject_dialog, container, false)
                ?: return null

            view.submitBtn.onClick {
                val cause = view.cause.text.toString()
                val defect = view.checkboxDefect.isChecked
                if (cause.trim().isEmpty()) {
                    view.cause.error = getString(R.string.product_info_error_empty_cause)
                } else {
                    callback(cause, defect)
                    dismiss()
                }
            }

            view.closeBtn.onClick {
                dialog.dismiss()
            }

            dialog.window.requestFeature(Window.FEATURE_NO_TITLE)

            return view
        }
    }
}