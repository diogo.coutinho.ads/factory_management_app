package com.cornel.pims.qc.common

data class WorkshopListUnit(val id: String, val name: String)

data class WorkplaceListUnit(val id: String, val name: String, val awaitCount: Int)

data class QCListUnit(
        val id: String,
        val code: String,
        val name: String,
        val workplace: String,
        val status: String,
        val shippingDate: String
)
