package com.cornel.pims.qc.core

import com.cornel.pims.qc.core.app.App
import com.cornel.pims.qc.core.helper.APIHelper
import com.cornel.pims.qc.core.helper.SharedPrefHelper
import javax.inject.Inject

interface DataManager {

    /* Init */
    suspend fun connect(login: String, password: String)

    suspend fun getUser(): QResponse<User>
    suspend fun getAppPermission(): Boolean
    suspend fun getWorkingShift(workshopId: String): QResponse<Int>
    suspend fun getProductList(): QResponse<List<Product>>
    suspend fun getProductById(id: String): QResponse<Product>

    suspend fun getWorkshopList(): QResponse<List<Workshop>>
    suspend fun getWorkshopById(id: String): QResponse<Workshop>

    suspend fun getWorkplacesByWorkshopId(id: String): QResponse<List<Workplace>>
    suspend fun getWorkplaceById(id: String): QResponse<Workplace>

    suspend fun getProductionAwaitQCByWorkshopId(id: String): QResponse<List<QCProductionAssignment>>
    suspend fun getProductionAssignmentById(id: String): QResponse<QCProductionAssignment?>

    /* Mutations */

    suspend fun rejectAssignment(id: String, cause: String, splittingQuantity: Double)
    suspend fun sendAssignmentToRevision(id: String, cause: String, splittingQuantity: Double)
    suspend fun acceptAssignment(id: String, splittingQuantity: Double)

    /* Shared Preferences */
    fun getAuthUrl(): String
    fun getApiUrl(): String
    fun getCurrentWorkshopId(): String
    fun getLogin(): String

    fun setAuthUrl(ip: String)
    fun setApiUrl(port: String)
    fun setCurrentWorkshopId(id: String)
    fun setLogin(login: String)
}

/*
 * TODO
 * Проработать логику если DM не возвращает корректных значений, определить поведение
 * в этом случае или показывать подробную ошибку на русском
*/

class DataManagerImpl(app: App) : DataManager {

    @Inject
    lateinit var sharedPrefHelper: SharedPrefHelper

    @Inject
    lateinit var apiHelper: APIHelper

    init {
        app.component.inject(this)
    }

    override suspend fun connect(login: String, password: String) {
        apiHelper.connect(login, password)
    }

    override suspend fun getWorkshopList(): QResponse<List<Workshop>> {
        return apiHelper.getWorkshopsList()
    }

    override suspend fun getWorkshopById(id: String): QResponse<Workshop> {
        val list = getWorkshopList()
        return QResponse(list.data!!.find { it.id == id }
                ?: throw RuntimeException("Cannot find workshop with code: $id"), list.error)
    }

    override suspend fun getProductList(): QResponse<List<Product>> {
        return apiHelper.getProductList()
    }

    override suspend fun getProductById(id: String): QResponse<Product> {
        val list = getProductList()
        return QResponse(list.data!!.filter { it.id == id }.getOrNull(0)
                ?: throw RuntimeException("Cannot find product with code=$id"), list.error)
    }


    override suspend fun getUser(): QResponse<User> {
        return apiHelper.getCurrentUser()
    }

    override suspend fun getAppPermission(): Boolean {
        return apiHelper.getAppPermission()
    }

    override suspend fun getWorkingShift(workshopId: String): QResponse<Int> {
        return apiHelper.getCurrentShift(workshopId)
    }

    override suspend fun getProductionAssignmentById(id: String): QResponse<QCProductionAssignment?> {
        return apiHelper.getProductionAssignmentById(id)
    }

    override fun getCurrentWorkshopId(): String = sharedPrefHelper.getWorkshopId()

    override fun setCurrentWorkshopId(id: String) = sharedPrefHelper.setWorkshopId(id)

    override fun getLogin(): String = sharedPrefHelper.getLogin()

    override fun setAuthUrl(ip: String) = sharedPrefHelper.setAuthUrl(ip)

    override fun setApiUrl(port: String) = sharedPrefHelper.setApiUrl(port)

    override fun setLogin(login: String) = sharedPrefHelper.setLogin(login)

    override fun getAuthUrl() = sharedPrefHelper.getAuthUrl()

    override fun getApiUrl() = sharedPrefHelper.getApiUrl()

    override suspend fun getWorkplaceById(id: String): QResponse<Workplace> {
        return apiHelper.getWorkplaceById(id)
    }

    override suspend fun getWorkplacesByWorkshopId(id: String): QResponse<List<Workplace>> {
        return apiHelper.getWorkplacesByWorkshopId(id)
    }

    override suspend fun getProductionAwaitQCByWorkshopId(id: String): QResponse<List<QCProductionAssignment>> {
        return apiHelper.getProductionAwaitQCByWorkshopId(id)
    }

    override suspend fun rejectAssignment(id: String, cause: String, splittingQuantity: Double) {
        apiHelper.rejectTechnicalControl(id, cause, splittingQuantity)
    }

    override suspend fun sendAssignmentToRevision(id: String, cause: String, splittingQuantity: Double) {
        apiHelper.sendToRevision(id, cause, splittingQuantity)
    }

    override suspend fun acceptAssignment(id: String, splittingQuantity: Double) {
        apiHelper.acceptTechnicalControl(id, splittingQuantity)
    }
}
